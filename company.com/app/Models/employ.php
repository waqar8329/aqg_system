<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class employ extends Model
{
    use HasFactory;
    protected $table = 'employee';
    protected $fillable = [
        'employeeno',
        'name',
        'jobtitleid',
        'projectid',
        'gender',
        'nationality',
        'dob',
        'bsalary',
        'allownce',
        'jdate',
        'idno',
        'idexpiry',
        'passportno',
        'passportexpiry',
        'hcardexpiry',
        'contractsdate',
        'contractexdate',
        'last_set_rec_date',
        'insurancestartdate',
        'insuranceexpirydate',
        'uploadfile',
        'note',
        'status1',
        'status2',
        'status3',
        'status4',
        'status5',
        'status6',
        'islocked',
        'send'
    ];
}
