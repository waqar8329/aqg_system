<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    use HasFactory;
    protected $fillable = [
        'referenceno',
        'contact',
        'customerid',
        'categoryid',
        'projectid',
        'supervisor',
        'date',
        'serviceid',
        'locationid',
        'fees',
        'file',
        'note',
        'islocked'
    ];
    protected $table = 'payments';
}
