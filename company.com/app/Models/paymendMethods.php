<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class paymendMethods extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
    ];
    protected $table = 'payment_methods';
}
