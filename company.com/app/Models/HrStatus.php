<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrStatus extends Model
{
    use HasFactory;
    protected $fillable = [
        'status_1',
        'status_2',
        'status_3',
        'status_4',
        'status_5',
        'status_6',
    ];
    protected $table = 'hrstatus';
}
