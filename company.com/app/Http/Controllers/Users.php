<?php

namespace App\Http\Controllers;

use App\Models\JobTitles;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class Users extends Controller
{
    public function getUsers(Request $request){
        $users = User::select('users.*', 'jobtitles.name as jobtitle')
            ->join('jobtitles', 'jobtitles.id','users.title')
            ->orderBy('users.created_at', 'desc')
            ->get()->toArray();
        $jobTitles = JobTitles::all()->toArray();
        $titlesAdded = array_unique(array_column($users, 'title'));

        $data = [];
        foreach ($jobTitles as $titles){
            if($titles['id'] == 3 || !in_array($titles['id'], $titlesAdded)){
                $data[] = $titles;
            }
        }
        $jobTitles = $data;
        return view('pages.users.users',compact('users', 'jobTitles'));
    }

    public function addUsers(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users',
            'name' => 'required',
            'password' => 'required',
        ],[
            'email.unique' => 'Username has already been taken.',
        ]);

        if ($validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $showError = $err;
                    break 2;
                }
            }
            return $showError;
        }else{
            $user = [
                'name' => $request->name,
                'email' => $request->email,
                'title' => (int) $request->job_title,
                'password' => Hash::make($request->password),
            ];
            if(User::create($user)){
                return 1;
            }
        }
    }

    function resetPassword(){
        return view('pages.users.resetpassword');
    }

    function updatepassword(Request $request){
        $user = [
            'password' => Hash::make($request->password),
        ];
        if(User::where('id', $request->id)->update($user)){
            return 1;
        }
    }

    function backupDB(){
        /*$filename = "backup-" . \Carbon\Carbon::now()->format('Y-m-d') . ".sql";
        $command = "".env('DUMP_PATH')." --user=" . env('DB_USERNAME') . " --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . "  > " . storage_path() . "/app/backup/" . $filename;
        $returnVar = NULL;
        $output = NULL;
        echo $command;
        exec($command, $output, $returnVar);*/
        $backup_file = 'db-backup-'.time().'.sql';

// get backup
        $mybackup = $this->backup_tables("127.0.0.1","root","","company","*");

// save to file
        $handle = fopen($backup_file,'w+');
        fwrite($handle,$mybackup);
        fclose($handle);
    }
    function backup_tables($host, $user, $pass, $name, $tables = '*'){
        $data = "\n/*---------------------------------------------------------------".
            "\n  SQL DB BACKUP ".date("d.m.Y H:i")." ".
            "\n  HOST: {$host}".
            "\n  DATABASE: {$name}".
            "\n  TABLES: {$tables}".
            "\n  ---------------------------------------------------------------*/\n";
        $link = mysqli_connect($host,$user,$pass,$name);
        $mysqli = new mysqli($host,$user,$pass,$name);
        //mysqli_select_db($name,$link);
       // mysqli_query( "SET NAMES `utf8` COLLATE `utf8_general_ci`" , $link ); // Unicode

        if($tables == '*'){ //get all of the tables
            $tables = array();
            $result =  $mysqli->query("SHOW TABLES");//mysqli_query("SHOW TABLES");
            while($row = $result -> fetch_row()){
                $tables[] = $row[0];
            }
        }else{
            $tables = is_array($tables) ? $tables : explode(',',$tables);
        }

        foreach($tables as $table){
            $data.= "\n/*---------------------------------------------------------------".
                "\n  TABLE: `{$table}`".
                "\n  ---------------------------------------------------------------*/\n";
            $data.= "DROP TABLE IF EXISTS `{$table}`;\n";
            $res = mysql_query("SHOW CREATE TABLE `{$table}`", $link);
            $row = mysql_fetch_row($res);
            $data.= $row[1].";\n";

            $result = mysql_query("SELECT * FROM `{$table}`", $link);
            $num_rows = mysql_num_rows($result);

            if($num_rows>0){
                $vals = Array(); $z=0;
                for($i=0; $i<$num_rows; $i++){
                    $items = mysql_fetch_row($result);
                    $vals[$z]="(";
                    for($j=0; $j<count($items); $j++){
                        if (isset($items[$j])) { $vals[$z].= "'".mysql_real_escape_string( $items[$j], $link )."'"; } else { $vals[$z].= "NULL"; }
                        if ($j<(count($items)-1)){ $vals[$z].= ","; }
                    }
                    $vals[$z].= ")"; $z++;
                }
                $data.= "INSERT INTO `{$table}` VALUES ";
                $data .= "  ".implode(";\nINSERT INTO `{$table}` VALUES ", $vals).";\n";
            }
        }
        mysql_close( $link );
        return $data;
    }
}
