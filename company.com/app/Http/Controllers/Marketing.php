<?php

namespace App\Http\Controllers;

use App\Models\Amounts;
use App\Models\Category;
use App\Models\Customers;
use App\Models\JobTitles;
use App\Models\locations;
use App\Models\paymendMethods;
use App\Models\Payments;
use App\Models\projects;
use App\Models\services;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class Marketing extends Controller
{
    public function customers(){
        $customers = Customers::orderBy('created_at', 'desc')->get()->toArray();
        return view('pages.marketing.customers',compact('customers'));
    }

    public function addCustomer(Request $request){
        $validator = Validator::make($request->all(), [
            'contact' => 'required|unique:customers',
            'name' => 'required',
        ]);
        if (Customers::where('contact', $request->contact)->where('id', '!=', $request->id)->exists()) {
            return 'Contact has already been taken.';
        }
        if ( (!isset($request->id)) && $validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $showError = $err;
                    break 2;
                }
            }
            return $showError;
        }else{
            $data = [
                'name' => $request->name,
                'contact' => $request->contact,
            ];
            if(isset($request->id)) {
                return Customers::find($request->id)->update($data);
            }else{
                if (Customers::create($data)) {
                    return 1;
                }
            }
        }
    }



    public function category(){
        $datas = Category::orderBy('created_at', 'desc')->get()->toArray();
        return view('pages.marketing.category',compact('datas'));
    }

    public function addCategory(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:category',
        ]);
        if (Category::where('name', $request->name)->where('id', '!=', $request->id)->exists()) {
            return 'Name has already been taken.';
        }
        if ( (!isset($request->id)) && $validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $showError = $err;
                    break 2;
                }
            }
            return $showError;
        }else{
            $category = [
                'name' => $request->name,
            ];
            if(isset($request->id)) {
                return Category::find($request->id)->update($category);
            }else{
                if (Category::create($category)) {
                    return 1;
                }
            }
        }
    }

    function getCustomerIdbyConctact(Request $request){
        $customer = Customers::where('contact', $request->contact)->first();
        $customerId = '';
        if(!empty($customer)){
            $customer = $customer->toArray();
            $customerId = $customer['id'] ?? '';
        }
        return $customerId;
    }


    public function projects(){
        $datas = projects::orderBy('created_at', 'desc')->get()->toArray();
        return view('pages.marketing.projects',compact('datas'));
    }

    public function addProject(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:projects',
        ]);
        if (projects::where('name', $request->name)->where('id', '!=', $request->id)->exists()) {
            return 'Name has already been taken.';
        }
        if ((!isset($request->id)) && $validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $showError = $err;
                    break 2;
                }
            }
            return $showError;
        }else{
            $data = [
                'name' => $request->name,
            ];
            if(isset($request->id)) {
                return projects::find($request->id)->update($data);
            }else{
                if (projects::create($data)) {
                    return 1;
                }
            }

        }
    }

    public function services(){
        $datas = services::orderBy('created_at', 'desc')->get()->toArray();
        return view('pages.marketing.services',compact('datas'));
    }

    public function addServices(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:services',
        ]);
        if (services::where('name', $request->name)->where('id', '!=', $request->id)->exists()) {
            return 'Name has already been taken.';
        }
        if ((!isset($request->id)) && $validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $showError = $err;
                    break 2;
                }
            }
            return $showError;
        }else{
            $data = [
                'name' => $request->name,
            ];
            if(isset($request->id)) {
                return services::find($request->id)->update($data);
            }else{
                if (services::create($data)) {
                    return 1;
                }
            }
        }
    }

    public function paymentMethods(){
        $datas = paymendMethods::orderBy('created_at', 'desc')->get()->toArray();
        return view('pages.marketing.paymentmethods',compact('datas'));
    }

    public function addPaymentMethod(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:payment_methods',
        ]);
        if (paymendMethods::where('name', $request->name)->where('id', '!=', $request->id)->exists()) {
            return 'Name has already been taken.';
        }
        if ((!isset($request->id)) && $validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $showError = $err;
                    break 2;
                }
            }
            return $showError;
        }else{
            $data = [
                'name' => $request->name,
            ];
            if(isset($request->id)) {
                return paymendMethods::find($request->id)->update($data);
            }else{
                if (paymendMethods::create($data)) {
                    return 1;
                }
            }
        }
    }

    public function locations(){
        $datas = locations::orderBy('created_at', 'desc')->get()->toArray();
        return view('pages.marketing.locations',compact('datas'));
    }

    public function addLocations(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:locations',
        ]);
        if (locations::where('name', $request->name)->where('id', '!=', $request->id)->exists()) {
            return 'Name has already been taken.';
        }
        if ((!isset($request->id)) && $validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $showError = $err;
                    break 2;
                }
            }
            return $showError;
        }else{
            $data = [
                'name' => $request->name,
            ];
            if(isset($request->id)) {
                return locations::find($request->id)->update($data);
            }else{
                if (locations::create($data)) {
                    return 1;
                }
            }
        }
    }

    public function jobTitles(){
        $datas = JobTitles::orderBy('created_at', 'desc')->get()->toArray();
        return view('pages.marketing.jobtitles',compact('datas'));
    }

    public function addJobTitles(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:jobtitles',
        ]);
        if (JobTitles::where('name', $request->name)->where('id', '!=', $request->id)->exists()) {
            return 'Name has already been taken.';
        }
        if ((!isset($request->id)) && $validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $showError = $err;
                    break 2;
                }
            }
            return $showError;
        }else{
            $data = [
                'name' => $request->name,
            ];
            if(isset($request->id)) {
                return JobTitles::find($request->id)->update($data);
            }else{
                if (JobTitles::create($data)) {
                    return 1;
                }
            }
        }
    }

    function delete(Request $request){
        $action = $request->action;
        $id = $request->id;

        switch ($action){
            case 'user':
                User::where('id', $id)->delete();
                break;
            case 'category':
                category::where('id', $id)->delete();
                break;
            case 'location':
                locations::where('id', $id)->delete();
                break;
            case 'service':
                services::where('id', $id)->delete();
                break;
            case 'customer':
                Customers::where('id', $id)->delete();
                break;
            case 'project':
                projects::where('id', $id)->delete();
                break;
            case 'paymentmethods':
                paymendMethods::where('id', $id)->delete();
                break;
            case 'jobtitle':
                JobTitles::where('id', $id)->delete();
                break;
            case 'payment':
                Payments::where('id', $id)->delete();
                Amounts::where('paymentid', $id)->delete();
                break;
        }
    }

    function edit(Request $request){
        $action = $request->action;
        $id = $request->id;

        switch ($action){
            case 'user':
                $data = User::where('id', $id)->get()->toArray();
                break;
            case 'category':
                $data = category::where('id', $id)->get()->toArray();
                break;
            case 'location':
                $data = locations::where('id', $id)->get()->toArray();
                break;
            case 'service':
                $data = services::where('id', $id)->get()->toArray();
                break;
            case 'customer':
                $data = Customers::where('id', $id)->get()->toArray();
                break;
            case 'project':
                $data = projects::where('id', $id)->get()->toArray();
                break;
            case 'paymentmethods':
                $data = paymendMethods::where('id', $id)->get()->toArray();
                break;
            case 'jobtitle':
                $data = JobTitles::where('id', $id)->get()->toArray();
                break;
            case 'payment':
                Payments::where('id', $id)->delete();
                Amounts::where('paymentid', $id)->delete();
                break;
        }
        return $data[0] ?? [];
    }

    function updateUser(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'name' => 'required',
            'password' => 'required',
        ],[
            'email.unique' => 'Username has already been taken.',
        ]);
        if (User::where('email', $request->email)->where('id', '!=', $request->id)->exists()) {

            return 'Username has already been taken.';
        }
        if ((!isset($request->id)) && $validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $showError = $err;
                    break 2;
                }
            }
            return $showError;
        }else{
            $user = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ];
            if(isset($request->id)) {
                return User::find($request->id)->update($user);
            }else{
                if (User::create($user)) {
                    return 1;
                }
            }
        }
    }

    function update(Request $request){
        $action = $request->action;
        switch ($action){
            case 'updateuser':
                return $this->updateUser($request);
                break;
            case 'user':
                return $this->addCustomer($request);
                break;
            case 'category':
                return $this->addCategory($request);
                break;
            case 'location':
                return $this->addLocations($request);
                break;
            case 'service':
                return $this->addServices($request);
                break;
            case 'customer':
                return $this->addCustomer($request);
                break;
            case 'project':
                return $this->addProject($request);
                break;
            case 'paymentmethods':
                return $this->addPaymentMethod($request);
                break;
            case 'jobtitle':
                return $this->addJobTitles($request);
                break;
            case 'payment':
                Payments::where('id', $id)->delete();
                Amounts::where('paymentid', $id)->delete();
                break;
        }
        return $data[0] ?? [];
    }


    function payments(Request $request){
        $data = Payments::select('payments.*','customers.name as cname', 'category.name as catname', 'projects.name as pname','users.name as supervisor','services.name as servicename', 'locations.name as locname')
            ->join('customers','customers.id','payments.customerid')
            ->join('category','category.id','payments.categoryid')
            ->leftjoin('projects','projects.id','payments.projectid')
            ->join('users','users.id','payments.supervisor')
            ->join('services','services.id','payments.serviceid')
            ->join('locations','locations.id','payments.locationid')
            ->orderBy('payments.created_at', 'DESC')
            ->whereMonth('payments.date', date('m'))
            ->whereYear('payments.date', date('Y'))
            ->get()->toArray();
        //echo "<pre>"; print_r($data); exit;
        $totalFees = $uncollected = $collected = 0;
        foreach ($data as $key => $dt){
            $data[$key]['amount'] = Amounts::select('amount.*', 'payment_methods.name as paymentmethod')
                ->where('paymentid',$dt['id'])
                ->join('payment_methods','payment_methods.id', 'amount.paymentmethodid')
                ->get()->toArray();
            $collected += (int) Amounts::where('paymentid',$dt['id'])->sum('amount');
            $totalFees += (int) $dt['fees'];
        }
        $uncollected = (int) $totalFees-$collected;
        $datas['paymentDetail'] = [
            'totalFees' => $totalFees,
            'collected' => $collected,
            'uncollected' => $uncollected
        ];
        $datas['data'] = $data;
        $datas['customers'] = Customers::all()->toArray();
        $datas['category'] = Category::all()->toArray();
        $datas['project'] = projects::all()->toArray();
        $datas['supervisors'] = User::where('title', 3)->get()->toArray();
        $datas['service'] = services::all()->toArray();
        $datas['locations'] = locations::all()->toArray();
        $datas['paymentmethods'] = paymendMethods::all()->toArray();
        //echo "<pre>"; print_r($datas); exit;
        return view('pages.marketing.payments', compact('datas'));
    }

    function create(Request $request){
        $datas['customers'] = Customers::all()->toArray();
        $datas['category'] = Category::all()->toArray();
        $datas['project'] = projects::all()->toArray();
        $datas['supervisors'] = User::where('title', 3)->get()->toArray();
        $datas['service'] = services::all()->toArray();
        $datas['locations'] = locations::all()->toArray();
        $datas['paymentmethods'] = paymendMethods::all()->toArray();
        $datas['refnos'] = Payments::select('referenceno')->get()->toArray();
        //echo "<pre>"; print_r($datas); exit;
        return view('pages.marketing.addpayments', compact('datas'));
    }

    function paymentsEdit(int $id){
        $datas['customers'] = Customers::all()->toArray();
        $datas['category'] = Category::all()->toArray();
        $datas['project'] = projects::all()->toArray();
        $datas['supervisors'] = User::where('title', 3)->get()->toArray();
        $datas['service'] = services::all()->toArray();
        $datas['locations'] = locations::all()->toArray();
        $datas['paymentmethods'] = paymendMethods::all()->toArray();
        $payment = Payments::where('id', $id)->get()->toArray()[0] ?? [];
        $amount = Amounts::where('paymentid',$id)->get()->toArray();
        //dd($amount);
        return view('pages.marketing.editpayments', compact('datas', 'payment', 'amount'));
    }

    function store(Request $request){
        $validator = Validator::make($request->all(), [
            //'referenceno' => 'required|unique:payments',
            'contact' => 'required',
            'customerid' => 'required',
            'categoryid' => 'required',
            //'projectid' => 'required',
            'supervisor' => 'required',
            'date' => 'required',
            'serviceid' => 'required',
            'locationid' => 'required',
            'fees' => 'required'
        ],[
            //'referenceno.unique' => 'Reference no has already been taken..',
            //'referenceno.required' => 'Reference no is required.',
            'contact.required' => 'Contact is required.',
            'customerid.required' => 'Customer is required.',
            'categoryid.required' => 'Category is required.',
            //'projectid.required' => 'Project is required.',
            'supervisor.required' => 'Supervisor is required.',
            'date.required' => 'Date is required.',
            'serviceid.required' => 'Service is required.',
            'locationid.required' => 'Location is required.',
            'fees.required' => 'Fees is required.',
        ]);
        if ($validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $message = $err[0] ?? '';
                    break 2;
                }
            }
            $request->session()->flash('error',$message);
            return redirect()->back()->withInput();
        }else{
            $newFilename = 0;
            if($request->file) {
                if($request->file('file')->getSize() > 1048576){
                    $request->session()->flash('error','File size allowed 2MB');
                    return redirect()->back()->withInput();
                }
                $file = $request->file('file');
                $hashedName = hash_file('md5', $file->path());
                $newFilename = $hashedName .  '.' . $file->getClientOriginalExtension();
               // echo $newFilename; exit;
               // Storage::disk('public')->put($newFilename, $request->file);
                Storage::disk('public')->put($newFilename, file_get_contents($file));
            }
            $data = [
                'referenceno' => $request->referenceno ??'',
                'contact' => $request->contact,
                'customerid' => $request->customerid,
                'categoryid' => $request->categoryid,
                'projectid' => $request->projectid ?? '',
                'supervisor' => $request->supervisor,
                'date' => $request->date,
                'serviceid' => $request->serviceid,
                'locationid' => $request->locationid,
                'fees' => $request->fees,
                'file' => $newFilename ?? '',
                'note' => $request->note
            ];
            $record = Payments::create($data);
            $id = $record->id;
            $fees = $request->fees;
            if($request->amount_1 && $request->date_1 && $request->method_1) {
                $amount = [
                    'amount' => $request->amount_1,
                    'date' => $request->date_1,
                    'paymentmethodid' => $request->method_1,
                    'paymentid' => $id
                ];
                $fees = $fees-$request->amount_1;
                Amounts::create($amount);
            }

            if($request->amount_2 && $request->date_2 && $request->method_2) {
                $amount = [
                    'amount' => $request->amount_2,
                    'date' => $request->date_2,
                    'paymentmethodid' => $request->method_2,
                    'paymentid' => $id
                ];
                $fees = $fees-$request->amount_2;
                Amounts::create($amount);
            }
            if($request->amount_3 && $request->date_3 && $request->method_3) {
                $amount = [
                    'amount' => $request->amount_3,
                    'date' => $request->date_3,
                    'paymentmethodid' => $request->method_3,
                    'paymentid' => $id
                ];
                $fees = $fees-$request->amount_3;
                Amounts::create($amount);
            }
            if($request->amount_4 && $request->date_4 && $request->method_4) {
                $amount = [
                    'amount' => $request->amount_4,
                    'date' => $request->date_4,
                    'paymentmethodid' => $request->method_4,
                    'paymentid' => $id
                ];
                $fees = $fees-$request->amount_4;
                Amounts::create($amount);
            }

            if($fees === 0){
                Payments::where('id', $id)->update(['paymentstatus' => 1]);
            }

            if(isset($request->addbyreference)){
                $message = "Payment added successfully.";
                $request->session()->flash('success',$message);
                return redirect()->back();
                //return redirect()->route('marketing.payments');
            }else {
                $message = "Payment added successfully.";
                $request->session()->flash('success',$message);
                return redirect()->back();
            }
        }

    }

    function paymentsUpdate(Request $request){
        $validator = Validator::make($request->all(), [
            //'referenceno' => 'required|unique:payments',
            'contact' => 'required',
            'customerid' => 'required',
            'categoryid' => 'required',
            //'projectid' => 'required',
            'supervisor' => 'required',
            'date' => 'required',
            'serviceid' => 'required',
            'locationid' => 'required',
            'fees' => 'required'
        ],[
            //'referenceno.unique' => 'Reference no has already been taken..',
            //'referenceno.required' => 'Reference no is required.',
            'contact.required' => 'Contact is required.',
            'customerid.required' => 'Customer is required.',
            'categoryid.required' => 'Category is required.',
            //'projectid.required' => 'Project is required.',
            'supervisor.required' => 'Supervisor is required.',
            'date.required' => 'Date is required.',
            'serviceid.required' => 'Service is required.',
            'locationid.required' => 'Location is required.',
            'fees.required' => 'Fees is required.',
        ]);
        $id = $request->id;
        /*if(Payments::where('id', '!=', $id)->where('referenceno', $request->referenceno )->exists()){
            $request->session()->flash('error', 'Reference no has already been taken.');
            return redirect()->back()->withInput();
        }*/
        if ($validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $message = $err[0] ?? '';
                    break 2;
                }
            }
            $request->session()->flash('error',$message);
            return redirect()->back()->withInput();
        }else {


            if($request->file) {
                if($request->file('file')->getSize() > 1048576){
                    $request->session()->flash('error','File size allowed 2MB');
                    return redirect()->back()->withInput();
                }
                $file = $request->file('file');
                $hashedName = hash_file('md5', $file->path());
                $newFilename = $hashedName .  '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put($newFilename, file_get_contents($file));
            }
            $data = [
                'referenceno' => $request->referenceno ?? '',
                'contact' => $request->contact,
                'customerid' => $request->customerid,
                'categoryid' => $request->categoryid,
                'projectid' => $request->projectid ?? '',
                'supervisor' => $request->supervisor,
                'date' => $request->date,
                'serviceid' => $request->serviceid,
                'locationid' => $request->locationid,
                'fees' => $request->fees,
                'file' => $newFilename ?? '',
                'note' => $request->note
            ];
            Payments::where('id', $id)->update($data);
            Amounts::where('paymentid', $id)->delete();
            $fees = 0;
            $amount = [];
            if ($request->amount_1 && $request->date_1 && $request->method_1) {
                $amount = [
                    'amount' => $request->amount_1,
                    'date' => $request->date_1,
                    'paymentmethodid' => $request->method_1,
                    'paymentid' => $id
                ];
                $fees = $fees-$request->amount_1;
                Amounts::create($amount);
            }
            if ($request->amount_2 && $request->date_2 && $request->method_2) {
                $amount = [
                    'amount' => $request->amount_2,
                    'date' => $request->date_2,
                    'paymentmethodid' => $request->method_2,
                    'paymentid' => $id
                ];
                $fees = $fees-$request->amount_2;
                Amounts::create($amount);
            }
            if ($request->amount_3 && $request->date_3 && $request->method_3) {
                $amount = [
                    'amount' => $request->amount_3,
                    'date' => $request->date_3,
                    'paymentmethodid' => $request->method_3,
                    'paymentid' => $id
                ];
                $fees = $fees-$request->amount_3;
                Amounts::create($amount);
            }
            if ($request->amount_4 && $request->date_4 && $request->method_4) {
                $amount = [
                    'amount' => $request->amount_4,
                    'date' => $request->date_4,
                    'paymentmethodid' => $request->method_4,
                    'paymentid' => $id
                ];
                $fees = $fees-$request->amount_4;
                Amounts::create($amount);
            }
            //echo "<pre>"; print_r($amount);exit;
            if(!empty($amount)) {
                //Amounts::create($amount);
            }
            if($fees === 0){
                Payments::where('id', $id)->update(['paymentstatus' => 1]);
            }
            $message = "Payment updated successfully.";
            $request->session()->flash('success', $message);
            return redirect()->back();
        }
    }

    function lock(Request $request){
        $id = $request->id;
        $action = $request->action == 'true' ? 1:0;
        return Payments::where('id', $id)->update(['islocked'=>$action]);
    }

    function lockAll(Request $request){
        $action = (bool) $request->action ? 1:0;
        return DB::table('payments')->update(['islocked'=>$action]);
    }

    function lockFromTo(Request $request){

        $action = $request->action ? 1:0;
        $start_date = date('Y-m-d 00:00:00', strtotime($request->from)-24*3600);
        $end_date = date('Y-m-d 23:59:59', strtotime($request->to)+24*3600);

        return DB::table('payments')
            ->where('date', '>=', $start_date)
            ->where('date', '<=', $end_date)
            ->update(['islocked'=>$action]);

    }

    function paymentByRefNo(Request $request, $refNo){
        //$refNo = $request->refno;
        $datas['customers'] = Customers::all()->toArray();
        $datas['category'] = Category::all()->toArray();
        $datas['project'] = projects::all()->toArray();
        $datas['supervisors'] = User::where('title', 3)->get()->toArray();
        $datas['service'] = services::all()->toArray();
        $datas['locations'] = locations::all()->toArray();
        $datas['paymentmethods'] = paymendMethods::all()->toArray();
        $payment = Payments::where('id', $refNo)->get()->toArray()[0] ?? [];
        $amount = Amounts::where('paymentid',$payment['id'])->get()->toArray();

        $totalFees = $payment['fees'] ?? 0;
        $collected = Amounts::where('paymentid',$payment['id'])->sum('amount');
        $uncollected = $totalFees-$collected;
        $datas['paymentDetail'] = [
            'totalFees' => $totalFees,
            'collected' => $collected,
            'uncollected' => $uncollected
        ];
        return view('pages.marketing.paymentsbyrefno', compact('datas', 'payment', 'amount'));
    }

    function searchPayments(Request $request){
        $data = $request->all();
        $payment = DB::table('payments');
        if(isset($data['reference_number']) && $data['reference_number'] != ''){
            $payment->where('referenceno' ,$data['reference_number']); //
        }
        if(isset($data['contact_number']) && $data['contact_number'] != ''){
            $payment->where('payments.contact', $data['contact_number']); //
        }
        if(isset($data['customerid']) && $data['customerid'] != ''){
            $payment->where('customerid', $data['customerid']); //
        }
        if(isset($data['categoryid']) && $data['categoryid'] != ''){
            $payment->where('categoryid', $data['categoryid']); //
        }
        if(isset($data['projectid']) && $data['projectid'] != ''){
            $payment->where('projectid', $data['projectid']); //
        }
        if(isset($data['supervisor']) && $data['supervisor'] != ''){
            $payment->where('supervisor', $data['supervisor']); //
        }
        if(isset($data['serviceid']) && $data['serviceid'] != ''){
            $payment->where('serviceid', $data['serviceid']); //
        }
        if(isset($data['locationid']) && $data['locationid'] != ''){
            $payment->where('locationid', $data['locationid']); //
        }
        if(isset($data['from']) && $data['from'] != ''){
            $start_date = date('Y-m-d 00:00:00', strtotime($data['from'])-24*3600);
            $payment->where('payments.date', '>=', $start_date);
        }
        if(isset($data['to']) && $data['to'] != ''){
            $end_date = date('Y-m-d 23:59:59', strtotime($data['to']));
            //dd($end_date);
            $payment->where('payments.date', '<=', $end_date);
        }
        if(isset($data['income_fees']) && $data['income_fees'] != ''){
            $payment->where('fees', $data['income_fees']); //
        }
        if(isset($data['status']) && $data['status'] != ''){
            $payment->where('paymentstatus', $data['status']); //
        }
        if(isset($data['lock']) && $data['lock'] != ''){
            $payment->where('islocked', $data['lock']); //done
        }
        $payment->select('payments.*','customers.name as cname', 'category.name as catname', 'projects.name as pname','users.name as supervisor','services.name as servicename', 'locations.name as locname')
            ->join('customers','customers.id','payments.customerid')
            ->join('category','category.id','payments.categoryid')
            ->leftjoin('projects','projects.id','payments.projectid')
            ->join('users','users.id','payments.supervisor')
            ->join('services','services.id','payments.serviceid')
            ->join('locations','locations.id','payments.locationid')
            ->orderBy('payments.created_at', 'DESC');

        $data =  json_decode(json_encode($payment->get()->toArray()), true );
        $totalFees = $uncollected = $collected = 0;
        foreach ($data as $key => $dt){
            $data[$key]['amount'] = Amounts::select('amount.*', 'payment_methods.name as paymentmethod')
                ->where('paymentid',$dt['id'])
                ->join('payment_methods','payment_methods.id', 'amount.paymentmethodid')
                ->get()->toArray();
            $collected += (int) Amounts::where('paymentid',$dt['id'])->sum('amount');
            $totalFees += (int) $dt['fees'];
        }

        $uncollected = (int) $totalFees-$collected;
        $datas['paymentDetail'] = [
            'totalFees' => $totalFees,
            'collected' => $collected,
            'uncollected' => $uncollected
        ];


        $datas['data'] = $data;

        $datas['customers'] = Customers::all()->toArray();
        $datas['category'] = Category::all()->toArray();
        $datas['project'] = projects::all()->toArray();
        $datas['supervisors'] = User::where('title', 3)->get()->toArray();
        $datas['service'] = services::all()->toArray();
        $datas['locations'] = locations::all()->toArray();
        $datas['paymentmethods'] = paymendMethods::all()->toArray();
        $datas['searchedData'] = $request->all();
        return view('pages.marketing.payments', compact('datas'));
    }
}
