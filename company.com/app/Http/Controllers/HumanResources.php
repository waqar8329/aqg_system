<?php

namespace App\Http\Controllers;

use App\Models\attendance;
use App\Models\employ;
use App\Models\HrJobTitles;
use App\Models\HrProjects;
use App\Models\HrStatus;
use App\Models\projects;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class HumanResources extends Controller
{
    public function projects(){
        $datas = HrProjects::select('hrprojects.*','users.name as supervisorname')
            ->join('users','users.id', 'hrprojects.supervisorid')
            ->orderBy('hrprojects.created_at', 'desc')->get()->toArray();
        $supervisors = User::where('title', 3)->get()->toArray();
        return view('pages.hr.projects',compact('datas', 'supervisors'));
    }

    public function addProjects(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => "required|unique:hrprojects,name,NULL,id,deleted_at,NULL",
            //'name' => [Rule::unique('hrprojects')->ignore($request->id)],
            'supervisor' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $showError = $err;
                    break 2;
                }
            }
            return $showError;
        }else{
            $category = [
                'name' => $request->name,
                'supervisorid' => $request->supervisor
            ];
            if(isset($request->id)) {
                return HrProjects::find($request->id)->update($category);
            }else{
                if (HrProjects::create($category)) {
                    return 1;
                }
            }
        }
    }

    public function jobTitles(){
        $datas = HrJobTitles::orderBy('created_at', 'desc')->get()->toArray();
        return view('pages.hr.jobtitles',compact('datas'));
    }

    public function addJobTitles(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => ['required', Rule::unique('hrjobtitles')->ignore($request->id)]
        ]);

        if ($validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $showError = $err;
                    break 2;
                }
            }
            return $showError;
        }else{
            $category = [
                'name' => $request->name,
            ];
            if(isset($request->id)) {
                return HrJobTitles::find($request->id)->update($category);
            }else{
                if (HrJobTitles::create($category)) {
                    return 1;
                }
            }
        }
    }

    public function status(){
        $datas = HrStatus::orderBy('created_at', 'desc')->get()->toArray();
        return view('pages.hr.status',compact('datas'));
    }

    public function addStatus(Request $request){
        $data = [
            'status_1' => $request->status_1,
            'status_2' => $request->status_2,
            'status_3' => $request->status_3,
            'status_4' => $request->status_4,
            'status_5' => $request->status_5,
            'status_6' => $request->status_6,
        ];
        if(isset($request->id)) {
            return HrStatus::find($request->id)->update($data);
        }else{
            if (HrStatus::create($data)) {
                return 1;
            }
        }
    }

    function delete(Request $request){
        $action = $request->action;
        $id = $request->id;

        switch ($action){
            case 'project':
                HrProjects::where('id', $id)->delete();
                break;
            case 'jobtitle':
                HrJobTitles::where('id', $id)->delete();
                break;
            case 'status':
                HrStatus::where('id', $id)->delete();
                break;
            case 'employee':
                employ::where('id', $id)->delete();
                break;

        }
    }

    function edit(Request $request){
        $action = $request->action;
        $id = $request->id;

        switch ($action){
            case 'project':
                $data = HrProjects::where('id', $id)->get()->toArray();
                break;
            case 'jobtitle':
                $data = HrJobTitles::where('id', $id)->get()->toArray();
                break;
            case 'status':
                $data = HrStatus::where('id', $id)->get()->toArray();
                break;
        }
        return $data[0] ?? [];
    }

    function update(Request $request){
        $action = $request->action;

        switch ($action){
            case 'jobtitle':
                return $this->addJobTitles($request);
                break;
            case 'project':
                return $this->addProjects($request);
                break;
            case 'status':
                return $this->addStatus($request);
                break;
        }
    }


    public function addEmploy(){
        $data = HrStatus::orderBy('created_at', 'desc')->get()->toArray();

        $datas['status_1'] = array_filter(array_column($data, 'status_1'));
        $datas['status_2'] = array_filter(array_column($data, 'status_2'));
        $datas['status_3'] = array_filter(array_column($data, 'status_3'));
        $datas['status_4'] = array_filter(array_column($data, 'status_4'));
        $datas['status_5'] = array_filter(array_column($data, 'status_5'));
        $datas['status_6'] = array_filter(array_column($data, 'status_6'));
        $datas['data'] = $data;
        $datas['jobtitles'] = HrJobTitles::orderBy('created_at', 'desc')->get()->toArray();
        $datas['projects'] = HrProjects::orderBy('created_at', 'desc')->get()->toArray();
        return view('pages.hr.addemploy',compact('datas'));
    }

    public function employ(){
        $data = HrStatus::orderBy('created_at', 'desc')->get()->toArray();
        $datas['status_1'] = array_filter(array_column($data, 'status_1'));
        $datas['status_2'] = array_filter(array_column($data, 'status_2'));
        $datas['status_3'] = array_filter(array_column($data, 'status_3'));
        $datas['status_4'] = array_filter(array_column($data, 'status_4'));
        $datas['status_5'] = array_filter(array_column($data, 'status_5'));
        $datas['status_6'] = array_filter(array_column($data, 'status_6'));
        $datas['jobtitles'] = HrJobTitles::orderBy('created_at', 'desc')->get()->toArray();
        $datas['projects'] = HrProjects::orderBy('created_at', 'desc')->get()->toArray();
        $datas['employs'] = employ::select('employee.*','hrjobtitles.name as title_name', 'hrprojects.name as projectname', 'users.name as supervisor')
            ->join('hrjobtitles','hrjobtitles.id','employee.jobtitleid')
            ->join('hrprojects','hrprojects.id','employee.projectid')
            ->join('users','hrprojects.supervisorid','users.id')
            ->orderBy('employee.created_at', 'DESC')->get()
            ->toArray();
//echo "<pre>"; print_r($datas['employs']); exit;

        $locked = $unlocked = 0;
        foreach ($datas['employs'] as $employ){
            if(isset($employ['islocked']) && $employ['islocked'] == 1){
                ++$locked;
            }else{
                ++$unlocked;
            }
        }
        $locData['totalUnlockRecords'] = $unlocked;
        $locData['totalLockRecords'] = $locked;
        $locData['totalRecords'] = $unlocked+$locked;

        return view('pages.hr.employ',compact('datas', 'locData'));
    }

    public function employEdit($id){
        $data = HrStatus::orderBy('created_at', 'desc')->get()->toArray();

        $datas['status_1'] = array_filter(array_column($data, 'status_1'));
        $datas['status_2'] = array_filter(array_column($data, 'status_2'));
        $datas['status_3'] = array_filter(array_column($data, 'status_3'));
        $datas['status_4'] = array_filter(array_column($data, 'status_4'));
        $datas['status_5'] = array_filter(array_column($data, 'status_5'));
        $datas['status_6'] = array_filter(array_column($data, 'status_6'));
        $datas['data'] = $data;
        $datas['jobtitles'] = HrJobTitles::orderBy('created_at', 'desc')->get()->toArray();
        $datas['projects'] = HrProjects::orderBy('created_at', 'desc')->get()->toArray();
        $employ = employ::where('id', $id)->get()->toArray()[0] ?? [];

        $supervisor = HrProjects::where('hrprojects.id', $employ['projectid'])
            ->Join('users', 'users.id', 'hrprojects.supervisorid')
            ->pluck('users.name')->toArray()[0] ?? '';

        return view('pages.hr.employedit',compact('datas', 'employ', 'supervisor'));
    }

    public function employeeSave(Request $request){
        $validator = Validator::make($request->all(), [
            'employeeno' => 'required|unique:employee',
            'idno' => 'required|unique:employee',
        ],[
            'employeeno.unique' => 'The Employee No. has already been taken',
            'idno.unique' => 'The ID No. has already been taken',
        ]);

        if ($validator->fails()) {
            $errors = (array) $validator->errors();
            foreach ($errors as $error){
                foreach ($error as $err){
                    $message = $err[0] ?? '';
                    break 2;
                }
            }
            $request->session()->flash('error',$message);
            return redirect()->back()->withInput();
        }else {

            if($request->file) {
                if($request->file('file')->getSize() > 1048576){
                    $request->session()->flash('error','File size allowed 2MB');
                    return redirect()->back()->withInput();
                }
                $file = $request->file('file');
                $hashedName = hash_file('md5', $file->path());
                $newFilename = $hashedName .  '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put($newFilename, file_get_contents($file));
            }
            $insert = [
                'employeeno' => $request->employeeno ?? '',
                'name' => $request->emp_name ?? '',
                'jobtitleid' => $request->job_title ?? 0,
                'projectid' => $request->project ?? 0,
                'gender' => $request->gender ?? '',
                'nationality' => $request->nationality ?? '',
                'dob' => $request->dob ?? '',
                'bsalary' => $request->basic_salary ?? '',
                'allownce' => $request->main_allowance ?? '',
                'jdate' => $request->jdate ?? '',
                'idno' => $request->idno ?? '',
                'idexpiry' => $request->idepirydate ?? '',
                'passportno' => $request->p_number ?? '',
                'passportexpiry' => $request->pexpirydate ?? '',
                'hcardexpiry' => $request->hcardexpirydate ?? '',
                'contractsdate' => $request->contractstartdate ?? '',
                'contractexdate' => $request->contactexpirydate ?? '',
                'last_set_rec_date' => $request->lastsetrecdate ?? '',
                'insurancestartdate' => $request->insurancestartdate ?? '',
                'insuranceexpirydate' => $request->insuranceexpirydate ?? '',
                'uploadfile' => $newFilename ?? '',
                'note' => $request->note ?? '',
                'status1' => $request->status_1 ?? '',
                'status2' => $request->status_2 ?? '',
                'status3' => $request->status_3 ?? '',
                'status4' => $request->status_4 ?? '',
                'status5' => $request->status_5 ?? '',
                'status6' => $request->status_6 ?? ''
            ];
            employ::create($insert);
            $message = "Employee added successfully.";
            $request->session()->flash('success',$message);
            return redirect()->back();
        }
    }

    public function employeeUpdate(Request $request){
        if (employ::where('id', '!=', $request->id)->where('employeeno', $request->employeeno)->exists()){
            $request->session()->flash('error',"Employee number already has been taken.");
            return redirect()->back()->withInput();
        }else if(employ::where('id', '!=', $request->id)->where('employeeno', $request->idno)->exists()){
            $request->session()->flash('error',"ID number already has been taken.");
            return redirect()->back()->withInput();
        }else {
            if($request->file) {
                if($request->file('file')->getSize() > 1048576){
                    $request->session()->flash('error','File size allowed 2MB');
                    return redirect()->back()->withInput();
                }
                $file = $request->file('file');
                $hashedName = hash_file('md5', $file->path());
                $newFilename = $hashedName .  '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put($newFilename, file_get_contents($file));
            }else{
                $newFilename = employ::where('id', $request->id)->first()->toArray();
                $newFilename = $newFilename['uploadfile'] ?? '';
            }
            $insert = [
                'employeeno' => $request->employeeno ?? '',
                'name' => $request->emp_name ?? '',
                'jobtitleid' => $request->job_title ?? 0,
                'projectid' => $request->project ?? 0,
                'gender' => $request->gender ?? '',
                'nationality' => $request->nationality ?? '',
                'dob' => $request->dob ?? '',
                'bsalary' => $request->basic_salary ?? '',
                'allownce' => $request->main_allowance ?? '',
                'jdate' => $request->jdate ?? '',
                'idno' => $request->idno ?? '',
                'idexpiry' => $request->idepirydate ?? '',
                'passportno' => $request->p_number ?? '',
                'passportexpiry' => $request->pexpirydate ?? '',
                'hcardexpiry' => $request->hcardexpirydate ?? '',
                'contractsdate' => $request->contractstartdate ?? '',
                'contractexdate' => $request->contactexpirydate ?? '',
                'last_set_rec_date' => $request->lastsetrecdate ?? '',
                'insurancestartdate' => $request->insurancestartdate ?? '',
                'insuranceexpirydate' => $request->insuranceexpirydate ?? '',
                'uploadfile' => $newFilename ?? '',
                'note' => $request->note ?? '',
                'status1' => $request->status_1 ?? '',
                'status2' => $request->status_2 ?? '',
                'status3' => $request->status_3 ?? '',
                'status4' => $request->status_4 ?? '',
                'status5' => $request->status_5 ?? '',
                'status6' => $request->status_6 ?? ''
            ];
            employ::where('id', $request->id)->update($insert);
            $message = "Employee updated successfully.";
            $request->session()->flash('success',$message);
            return redirect()->back();
        }
    }

    function lock(Request $request){
        $id = $request->id;
        $action = $request->action;
        /*if($action == 0){
            employ::where('id', $id)->update(['send'=>$action]);
        }*/
        return employ::where('id', $id)->update(['islocked'=>$action]);
    }

    function lockAll(Request $request){
        $action = (bool) $request->action ? 1:0;
        /*if($action == 0){
            DB::table('employee')->update(['send'=>$action]);
        }*/
        return DB::table('employee')->update(['islocked'=>$action]);
    }

    function sendEmploy(Request $request){
        $id = $request->id;
        $action = $request->action;
        return employ::where('id', $id)->update(['send'=>$action]);
    }
    function undoSend(Request $request){
        $projectid = $request->prjectid;
        return DB::table('employee')->where('projectid', $projectid)->update(['send'=>0]);
    }


    function sendAllEmploy(Request $request){
        $id = json_decode($request->id);
        $action = 1;// $request->action;
        return DB::table('employee')
            ->whereIn('id', $id)
            ->update(['send'=>$action]);
    }

    function searchEmploy(Request $request){
        $data = $request->all();
        //echo "<pre>"; print_r($data); exit;
        $payment = DB::table('employee');
        if(isset($data['employee_number']) && $data['employee_number'] != ''){
            $payment->where('employeeno' ,$data['employee_number']); //
        }
        if(isset($data['emp_name']) && $data['emp_name'] != ''){
            $payment->where('empoyee.name', $data['emp_name']); //
        }
        if(isset($data['job_title']) && $data['job_title'] != ''){
            $payment->where('jobtitleid', $data['job_title']); //
        }
        if(isset($data['project']) && $data['project'] != ''){
            $payment->where('projectid', $data['project']); //
        }
        if(isset($data['gender']) && $data['gender'] != ''){
            $payment->where('gender', $data['gender']); //
        }
        if(isset($data['nationality']) && $data['nationality'] != ''){
            $payment->where('nationality', $data['nationality']); //
        }
        if(isset($data['dob_from']) && $data['dob_from'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['dob_from']));
            $payment->where('dob', '>=', $date);
        }
        if(isset($data['dob_to']) && $data['dob_to'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['dob_to']));
            $payment->where('dob', '<=', $date);
        }
        if(isset($data['basic_salary']) && $data['basic_salary'] != ''){
            $payment->where('bsalary', $data['basic_salary']); //
        }
        if(isset($data['main_allowance']) && $data['main_allowance'] != ''){
            $payment->where('allownce', $data['main_allowance']); //
        }
        if(isset($data['joining_from']) && $data['joining_from'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['joining_from']));
            $payment->where('jdate', '>=', $date);
        }

        if(isset($data['joining_to']) && $data['joining_to'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['joining_to']));
            $payment->where('jdate', '<=', $date);
        }
        if(isset($data['id_no']) && $data['id_no'] != ''){
            $payment->where('idno', $data['id_no']); //done
        }
        if(isset($data['id_expired_from']) && $data['id_expired_from'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['id_expired_from']));
            $payment->where('idexpiry', '>=', $date);
        }
        if(isset($data['id_expired_to']) && $data['id_expired_to'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['id_expired_to']));
            $payment->where('idexpiry', '<=', $date);
        }
        if(isset($data['p_number']) && $data['p_number'] != ''){
            $payment->where('passportno', $data['p_number']); //done
        }
        if(isset($data['passport_expired_from']) && $data['passport_expired_from'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['passport_expired_from']));
            $payment->where('passportexpiry', '>=', $date);
        }
        if(isset($data['passport_expired_to']) && $data['passport_expired_to'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['passport_expired_to']));
            $payment->where('passportexpiry', '<=', $date);
        }

        if(isset($data['hcart_expired_from']) && $data['hcart_expired_from'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['hcart_expired_from']));
            $payment->where('hcardexpiry', '>=', $date);
        }
        if(isset($data['hcart_expired_to']) && $data['hcart_expired_to'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['hcart_expired_to']));
            $payment->where('hcardexpiry', '<=', $date);
        }
        if(isset($data['contract_start_from']) && $data['contract_start_from'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['contract_start_from']));
            $payment->where('contractsdate', '>=', $date);
        }
        if(isset($data['contract_start_to']) && $data['contract_start_to'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['contract_start_to']));
            $payment->where('contractsdate', '<=', $date);
        }

        if(isset($data['contract_expiry_from']) && $data['contract_expiry_from'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['contract_expiry_from']));
            $payment->where('contractexdate', '>=', $date);
        }
        if(isset($data['contract_expiry_to']) && $data['contract_expiry_to'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['contract_expiry_to']));
            $payment->where('contractexdate', '<=', $date);
        }

        if(isset($data['set_from']) && $data['set_from'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['set_from']));
            $payment->where('last_set_rec_date', '>=', $date);
        }
        if(isset($data['set_to']) && $data['set_to'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['set_to']));
            $payment->where('last_set_rec_date', '<=', $date);
        }


        if(isset($data['insurance_start_from']) && $data['insurance_start_from'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['insurance_start_from']));
            $payment->where('insurancestartdate', '>=', $date);
        }
        if(isset($data['insurance_start_to']) && $data['insurance_start_to'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['insurance_start_to']));
            $payment->where('insurancestartdate', '<=', $date);
        }
        if(isset($data['insurance_expiry_from']) && $data['insurance_expiry_from'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['insurance_expiry_from']));
            $payment->where('insuranceexpirydate', '>=', $date);
        }
        if(isset($data['insurance_expiry_to']) && $data['insurance_expiry_to'] != ''){
            $date = date('Y-m-d 00:00:00', strtotime($data['insurance_expiry_to']));
            $payment->where('insuranceexpirydate', '<=', $date);
        }


        if(isset($data['status_1']) && $data['status_1'] != ''){
            $payment->where('status1', $data['status_1']); //done
        }
        if(isset($data['status_2']) && $data['status_2'] != ''){
            $payment->where('status2', $data['status_2']); //done
        }
        if(isset($data['status_3']) && $data['status_3'] != ''){
            $payment->where('status3', $data['status_3']); //done
        }
        if(isset($data['status_4']) && $data['status_4'] != ''){
            $payment->where('status4', $data['status_4']); //done
        }
        if(isset($data['status_5']) && $data['status_5'] != ''){
            $payment->where('status5', $data['status_5']); //done
        }
        if(isset($data['status_6']) && $data['status_6'] != ''){
            $payment->where('status6', $data['status_6']); //done
        }
        if(isset($data['locked']) && $data['locked'] != ''){
            if($data['locked'] == 1){
                $payment->where('islocked', 1); //done
            }else{
                $payment->where('islocked', 0); //done
            }
        }

        $payment->select('employee.*','hrjobtitles.name as title_name', 'hrprojects.name as projectname', 'users.name as supervisor')
            ->join('hrjobtitles','hrjobtitles.id','employee.jobtitleid')
            ->join('hrprojects','hrprojects.id','employee.projectid')
            ->join('users','hrprojects.supervisorid','users.id')
            ->orderBy('employee.created_at', 'DESC');

        $data =  json_decode(json_encode($payment->get()->toArray()), true );


        $status = HrStatus::orderBy('created_at', 'desc')->get()->toArray();

        $datas['status_1'] = array_filter(array_column($status, 'status_1'));
        $datas['status_2'] = array_filter(array_column($status, 'status_2'));
        $datas['status_3'] = array_filter(array_column($status, 'status_3'));
        $datas['status_4'] = array_filter(array_column($status, 'status_4'));
        $datas['status_5'] = array_filter(array_column($status, 'status_5'));
        $datas['status_6'] = array_filter(array_column($status, 'status_6'));
        $datas['employs'] = $data ??[];
        $datas['jobtitles'] = HrJobTitles::orderBy('created_at', 'desc')->get()->toArray();
        $datas['projects'] = HrProjects::orderBy('created_at', 'desc')->get()->toArray();
        $datas['searchedData'] = $request->all();

        $locked = $unlocked = 0;
        foreach ($datas['employs'] as $employ){
            if(isset($employ['islocked']) && $employ['islocked'] == 1){
                ++$locked;
            }else{
                ++$unlocked;
            }
        }
        $locData['totalUnlockRecords'] = $unlocked;
        $locData['totalLockRecords'] = $locked;
        $locData['totalRecords'] = $unlocked+$locked;
        return view('pages.hr.employ', compact('datas', 'locData'));
    }

    function attendances(Request $request){
        $datas = [];
        $projects = HrProjects::select('hrprojects.*', 'users.name as supervisorname')
                ->join('users', 'hrprojects.supervisorid', 'users.id')
                ->get()->toArray();
        foreach ($projects as $key => $project){
            $projects[$key]['numemployes'] = employ::where('projectid', $project['id'])->where('send', 1)->get()->count();
        }
        $datas['projects'] = $projects;
        return view('pages.hr.attendances', compact('datas'));
    }

    public function attendance(Request $request){
        $userid = Auth()->user()->id;
        $datas = employ::select('employee.*', 'hrprojects.name as projectname', 'hrjobtitles.name as jobtitlename')
            ->join('hrprojects', 'hrprojects.id', 'employee.projectid' )
            ->join('hrjobtitles', 'hrjobtitles.id', 'employee.jobtitleid' )
            ->where('hrprojects.supervisorid', $userid)
            //->where('employee.islocked', 1)
            ->where('employee.send', 1)
            ->get()->toArray();

        foreach ($datas as $key => $data){
            $attendances = attendance::where('employeid', $data['id'])->orderBy('created_at', 'desc')->take(1)->get() ?? [];
            $attendances = $attendances->toArray();
            if(!empty($attendances)) {
                foreach ($attendances as $key2 => $attendanceData) {
                    $datas[$key]['attendace'][$key2]['note'] = $attendanceData['note'] ?? '';
                    $datas[$key]['attendace'][$key2]['created_at'] = $attendanceData['created_at'] ?? '';

                    if (!empty($attendanceData)) {
                        $attendance = json_decode($attendanceData['value']);
                    }
                    /*if(!empty($attendance)){
                        $extratime = json_decode($attendanceData['extratime']);

                    }*/

                    if(!empty($attendance)){
                        $extratime = json_decode($attendanceData['extratime']);
                        $overTime = 0;
                        foreach ($extratime as $fkey => $eTime){
                            if(isset($attendance[$fkey]) && $attendance[$fkey] == 5){
                                $overTime += $eTime;
                            }else{
                                $extratime[$fkey] = 0;
                            }
                        }
                        //echo "<pre>"; print_r($attendance); print_r($extratime); exit;
                    }
                    $datas[$key]['attendace'][$key2]['attendance'] = $attendance;
                    $datas[$key]['attendace'][$key2]['extratime'] = $extratime;
                    $datas[$key]['attendace'][$key2]['overtime'] = $overTime;
                    $present = array_count_values($attendance)[1] ?? 0;
                    $present += array_count_values($attendance)[5] ?? 0;
                    $present += array_count_values($attendance)[3] ?? 0;
                    $present += array_count_values($attendance)[6] ?? 0;
                    $datas[$key]['attendace'][$key2]['present'] = $present;
                }
            }else{
                $key2 = 0;
                $datas[$key]['attendace'][$key2]['overtime'] =  0;
                $datas[$key]['attendace'][$key2]['note'] =  '';
                $datas[$key]['attendace'][$key2]['created_at'] = '';
                for ($i = 0; $i<=30; $i++){
                    $datas[$key]['attendace'][$key2]['attendance'][] = 0;
                    $datas[$key]['attendace'][$key2]['extratime'][] = 0;
                }
                $datas[$key]['attendace'][$key2]['overtime'] = 0;
                $datas[$key]['attendace'][$key2]['present'] = 0;
                $datas[$key]['attendace'][$key2]['created_at'] = date('Y-m-d');
            }
            $datas[$key]['attendance'] = $attendance ?? [];
            $datas[$key]['present'] = isset($attendance) ? array_count_values($attendance)[1] ?? 0 : 0;
        }
        $datas['data'] = $datas;
       // echo "<pre>"; print_r($datas['data']); exit;
        return view('pages.attendances', compact('datas'));
    }

    function saveAttendance(Request $request){
        $data = collect($request->all())->except(['_token'])->toArray();
        foreach ($data['employeeid'] as $key => $employid){
            $attendanceKey = 'attendacne'.$key;
            $attendance = json_encode($data[$attendanceKey]);
            $extratimeKey = 'extratime'.$key;
            $extratime = json_encode($data[$extratimeKey]);
            $insert = [
                'employeid' => $employid,
                'value' => $attendance,
                'extratime' => $extratime,
                'date' => date('d.m.Y', time()),
                'overtime' => $data['overtime'][$key] ?? 0,
                'note' => (string) $data['note'][$key] ?? '',
            ];
            if(attendance::where('employeid', $employid)->whereMonth('created_at', date('m'))
                ->whereYear('created_at', date('Y'))->exists()){
                attendance::where('employeid', $employid)->update($insert);
            }else{
                attendance::insert($insert);
            }

        }
        $request->session()->flash('success','Record updated successfully!');
        return redirect()->back();

    }

    function hrattendance(Request $request){
        $datas = employ::select('employee.*', 'hrprojects.name as projectname', 'hrjobtitles.name as jobtitlename')
            ->join('hrprojects', 'hrprojects.id', 'employee.projectid' )
            ->join('hrjobtitles', 'hrjobtitles.id', 'employee.jobtitleid' )
            ->where('employee.send', 1);
        if(isset($request->projectid)){
            $datas->where('projectid', $request->projectid);
        }
        /*if(isset($request->supervisorid)){
            $datas->where('projectid', $request->projectid);
        }*/
        $from = false;
        if(isset($request->from) && $request->from){
            $fromdate = date('Y-m-d 00:00:00', strtotime($request->from));
            //$datas->where('jdate', '>=', $date);
            $from = true;
        }
        $to = false;
        if(isset($request->to) && $request->to){
            $todate = date('Y-m-d 23:59:59', strtotime($request->to));
            //$datas->where('jdate', '<=', $date);
            $to = true;
        }
        $datas = $datas->get()->toArray();
        //echo "<pre>"; print_r($datas); exit;


        foreach ($datas as $key => $data){
            $search = true;
            if($from && $to){
                $attendances = attendance::where('employeid', $data['id'])
                        ->where('created_at', '<=', $todate)
                        ->where('created_at', '>=', $fromdate)->get() ?? [];
            }else if($from){
                $attendances = attendance::where('employeid', $data['id'])->where('created_at', '>=', $fromdate)->get() ?? [];
            }else if($to){
                $attendances = attendance::where('employeid', $data['id'])->where('created_at', '<=', $todate)->get() ?? [];
            }else{
                $search = false;
                $attendances = attendance::where('employeid', $data['id'])->orderBy('created_at', 'desc')->take(1)->get() ?? [];
            }
            $attendances = $attendances->toArray();
           // echo "<pre>";print_r($attendances); //exit;
            if(!empty($attendances)){
                foreach($attendances as $key2 => $attendanceData){
                    $datas[$key]['attendace'][$key2]['note'] = $attendanceData['note'] ?? '';
                    $datas[$key]['attendace'][$key2]['created_at'] = $attendanceData['created_at'] ?? '';
                    if(!empty($attendanceData)){
                        $attendance = json_decode($attendanceData['value']);
                    }

                    if(!empty($attendance)){
                        $extratime = json_decode($attendanceData['extratime']);
                        $overTime = 0;
                        foreach ($extratime as $fkey => $eTime){
                            if(isset($attendance[$fkey]) && $attendance[$fkey] == 5){
                                $overTime += $eTime;
                            }else{
                                $extratime[$fkey] = 0;
                            }
                        }
                        //echo "<pre>"; print_r($attendance); print_r($extratime); exit;
                    }
                    $datas[$key]['attendace'][$key2]['attendance'] = $attendance;
                    $datas[$key]['attendace'][$key2]['extratime'] = $extratime;
                    $datas[$key]['attendace'][$key2]['overtime'] = $overTime;
                    $present = array_count_values($attendance)[1] ?? 0;
                    $present += array_count_values($attendance)[5] ?? 0;
                    $present += array_count_values($attendance)[3] ?? 0;
                    $present += array_count_values($attendance)[6] ?? 0;
                    $datas[$key]['attendace'][$key2]['present'] = $present;
                }
            }else if(!$search){
                $key2 = 0;
                $datas[$key]['attendace'][$key2]['note'] =  '';
                $datas[$key]['attendace'][$key2]['created_at'] = '';
                for ($i = 0; $i<=30; $i++){
                    $datas[$key]['attendace'][$key2]['attendance'][] = 0;
                    $datas[$key]['attendace'][$key2]['extratime'][] = 0;
                }
                $datas[$key]['attendace'][$key2]['overtime'] = 0;
                $datas[$key]['attendace'][$key2]['present'] = 0;
                $datas[$key]['attendace'][$key2]['created_at'] = date('Y-m-d');
            }

        }
        $datas['data'] = $datas;
        $datas['projects'] = HrProjects::withTrashed()->get()->toArray();
        $datas['supervisors'] = User::where('title', 3)->get()->toArray();
        $searches = $request->all();
//echo"<pre>"; print_r($datas['data']); exit;
        return view('pages.attendances', compact('datas', 'searches'));
    }

    function getCustomerbyProjectId(Request $request){
        $user = User::select('users.name')
            ->join('hrprojects','hrprojects.supervisorid','users.id')
            ->where('hrprojects.id', $request->id)->first();
        $name = '';
        if(!empty($user)){
            $name = $user->toArray()['name'] ?? '';
        }
        return $name;
    }

}
