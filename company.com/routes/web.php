<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users;
use App\Http\Controllers\Marketing;
use App\Http\Controllers\HumanResources;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/dashboard1', function () {
    return view('dashboard1');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/users', [Users::class, 'getUsers'])->name('getUsers');
    Route::post('/addUsers', [Users::class, 'addUsers'])->name('adduser');


    Route::get('/marketing/customers', [Marketing::class, 'customers'])->name('marketing.customers');
    Route::post('/marketing/addcustomer', [Marketing::class, 'addCustomer'])->name('marketing.addcustomer');

    Route::get('/marketing/category', [Marketing::class, 'category'])->name('marketing.category');
    Route::post('/marketing/addcategory', [Marketing::class, 'addCategory'])->name('marketing.addcategory');

    Route::get('/marketing/project', [Marketing::class, 'projects'])->name('marketing.projects');
    Route::post('/marketing/addproject', [Marketing::class, 'addProject'])->name('marketing.addproject');


    Route::get('/marketing/services', [Marketing::class, 'services'])->name('marketing.services');
    Route::post('/marketing/addservices', [Marketing::class, 'addServices'])->name('marketing.addservices');

    Route::get('/marketing/locations', [Marketing::class, 'locations'])->name('marketing.locations');
    Route::post('/marketing/addlocations', [Marketing::class, 'addLocations'])->name('marketing.addlocations');

    Route::get('/marketing/paymentmethods', [Marketing::class, 'paymentMethods'])->name('marketing.paymentmethods');
    Route::post('/marketing/addpaymentmethod', [Marketing::class, 'addPaymentMethod'])->name('marketing.addpaymentmethod');

    Route::get('/marketing/jobtitles', [Marketing::class, 'jobTitles'])->name('marketing.jobtitles');
    Route::post('/marketing/addjobtitles', [Marketing::class, 'addJobTitles'])->name('marketing.addjobtitles');

    Route::post('/delete', [Marketing::class, 'delete'])->name('delete');
    Route::get('/marketing/edit/{action?}/{id?}', [Marketing::class, 'edit'])->name('marketing.edit');
    Route::post('/marketing/edit/update/{action?}/{id?}', [Marketing::class, 'update'])->name('marketing.update');


    Route::get('/marketing/payments', [Marketing::class, 'payments'])->name('marketing.payments');
    Route::get('/marketing/payments/{id}', [Marketing::class, 'paymentsEdit'])->name('marketing.payments.edit');
    Route::post('/marketing/updatepayments', [Marketing::class, 'paymentsUpdate'])->name('marketing.payments.update');
    Route::get('/marketing/addpayments', [Marketing::class, 'create'])->name('marketing.addpayments');
    Route::post('/marketing/addpayments', [Marketing::class, 'store'])->name('marketing.savepayment');
    Route::post('/marketing/lock', [Marketing::class, 'lock'])->name('marketing.lock');
    Route::post('/marketing/lockall', [Marketing::class, 'lockAll'])->name('marketing.lockall');
    Route::post('/marketing/lockfromto', [Marketing::class, 'lockFromTo'])->name('marketing.lockfromto');
    Route::get('/marketing/getCustomerIdbyConctact', [Marketing::class, 'getCustomerIdbyConctact'])->name('marketing.getCustomerIdbyConctact');

    Route::get('/marketing/paymentbyrefno/{id?}', [Marketing::class, 'paymentByRefNo'])->name('marketing.paymentbyrefno');
    Route::get('/marketing/search', [Marketing::class, 'searchPayments'])->name('marketing.payments.search');

    ///Hr
    Route::get('/hr/jobtitles', [HumanResources::class, 'jobTitles'])->name('hr.jobtitles');
    Route::post('/hr/addjobtitles', [HumanResources::class, 'addJobTitles'])->name('hr.addjobtitles');

    Route::get('/hr/projects', [HumanResources::class, 'projects'])->name('hr.projects');
    Route::post('/hr/addprojects', [HumanResources::class, 'addProjects'])->name('hr.addprojects');


    Route::get('/hr/status', [HumanResources::class, 'status'])->name('hr.status');
    Route::post('/hr/addstatus', [HumanResources::class, 'addStatus'])->name('hr.addstatus');

    Route::post('/hr/delete', [HumanResources::class, 'delete'])->name('hr.delete');

    Route::get('/hr/edit/{action?}/{id?}', [HumanResources::class, 'edit'])->name('hr.edit');
    Route::post('/hr/edit/update/{action?}/{id?}', [HumanResources::class, 'update'])->name('hr.update');

    Route::get('/hr/addemploy', [HumanResources::class, 'addEmploy'])->name('hr.addemploy');
    Route::get('/hr/employ', [HumanResources::class, 'employ'])->name('hr.employ');
    Route::post('/hr/employeesave', [HumanResources::class, 'employeeSave'])->name('hr.saveemployee');
    Route::post('/hr/employeeupdate', [HumanResources::class, 'employeeUpdate'])->name('hr.updateemployee');

    Route::post('/hr/lock', [HumanResources::class, 'lock'])->name('hr.lock');
    Route::post('/hr/lockall', [HumanResources::class, 'lockAll'])->name('hr.lockall');

    Route::get('/hr/employ/edit/{action?}/{id?}', [HumanResources::class, 'employEdit'])->name('hr.employ.edit');
    Route::post('/hr/employ/edit/update/{action?}/{id?}', [HumanResources::class, 'employUpdate'])->name('hr.employ.update');

    Route::get('/hr/search', [HumanResources::class, 'searchEmploy'])->name('hr.employ.search');
    Route::post('/hr/send', [HumanResources::class, 'sendEmploy'])->name('hr.employ.send');
    Route::post('/hr/undosend', [HumanResources::class, 'undoSend'])->name('hr.undosend');
    Route::post('/hr/sendall', [HumanResources::class, 'sendAllEmploy'])->name('hr.employ.sendall');
    Route::get('/hr/attendances', [HumanResources::class, 'attendances'])->name('hr.attendances');


    Route::get('/attendance', [HumanResources::class, 'attendance'])->name('attendance');
    Route::post('/saveattendance', [HumanResources::class, 'saveAttendance'])->name('saveattendance');

    Route::get('/resetpassword', [Users::class, 'resetPassword'])->name('resetpassword');
    Route::POST('/updatepassword', [Users::class, 'updatepassword'])->name('updatepassword');

    Route::get('/hrattendance', [HumanResources::class, 'hrattendance'])->name('hrattendance');
    Route::get('/getcustomernamebyprojectid', [HumanResources::class, 'getCustomerbyProjectId'])->name('getcustomernamebyprojectid');

    Route::get('/download', function (Request $request){
        $filename = $request->filename;
        return response()->download(storage_path('/app/public/'. $filename));

        $file=Storage::disk('public')->get($filename);

        return (new Response($file, 200))
            ->header('Content-Type', 'image/jpeg');
    })->name('download');
});

Route::get('/cache', function(){
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
});

Route::get('/time', function(){
    echo date('d.m.Y H:i:s');
});

Route::get('/dumpdb', function(){
    Spatie\DbDumper\Databases\MySql::create()
        ->setDbName('company')
        ->setUserName('root')
        ->setPassword('')
        ->dumpToFile('dump.sql');
});

Route::get('/backupdb', [Users::class, 'backupDB'])->name('backupdb');

