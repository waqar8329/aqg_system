
@extends('main')

@section('content')
    <div class="auth-box row">
        <div class="col-lg-7 col-md-5 modal-bg-img" style="background-image: url({{asset('assets/dist/imgs/big/3.1.jpg')}});">
        </div>
        <div class="col-lg-5 col-md-7 bg-white">
            <div class="p-3">
                <div class="text-center">
                    <img src="{{asset('assets/dist/imgs/small/abraj.jpg')}}" width="200px" alt="aqg">
                </div>
                <!--<h3 class="mt-3 text-center">LOGIN</h3><br>-->
                <br>
                <h6 class="card-subtitle">Enter your username and password to login to your panel.</h6>
                @if(count($errors) > 0)
                    @foreach( $errors->all() as $message )
                        <div class="alert alert-danger display-hide" style="margin-top: 15px;font-size: 14px;">
                            <button class="close" data-close="alert"></button>
                            <span>{{ $message }}</span>
                        </div>
                    @endforeach
                @endif
                <form class="mt-4" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <h6 class="text-dark" for="uname"><b>Username</b></h6>
                                <input class="form-control" name="email" id="uname" type="text" style="font-size: 0.9rem;"
                                       placeholder="Enter your username..">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <h6 class="text-dark" for="pwd"><b>Password</b></h6>
                                <input class="form-control" name="password" id="pwd" type="password" style="font-size: 0.9rem;"
                                       placeholder="Enter your password..">
                            </div>

                        </div>

                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn btn-block btn-maroon">LOGIN</button>
                        </div>
                        <!--<div class="col-lg-12 text-center mt-2">
                          <small><a href="#" class="text-danger">Forgot Password?</a></small>
                        </div>-->



                        <div class="col-lg-12 text-center mt-5">
                            <h6>Copyright &copy;<script>document.write(new Date().getFullYear());</script> - All Rights Reserved by ABRAJ QATAR GROUP</h6>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

{{--<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" />
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-jet-button class="ml-4">
                    {{ __('Log in') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
--}}
