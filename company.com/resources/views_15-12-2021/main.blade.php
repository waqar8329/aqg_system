<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta property="og:type" content="website"/>
    {{--    <meta property="og:image" content="http://my.site.com/images/thumb.png" />--}}

    <title>@yield('title','AQG | Abraj Qatar Group')</title>
    @includeIf('include.styles')
</head>
<body class="hold-transition sidebar-mini">

<div class="main-wrapper">
    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center position-relative"
    >
    @yield('content')
    </div>
    @includeIf('include.footer')
</div>
@includeIf('include.scripts')

</body>
</html>

