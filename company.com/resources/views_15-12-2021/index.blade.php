<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta property="og:type" content="website"/>
    {{--    <meta property="og:image" content="http://my.site.com/images/thumb.png" />--}}


    <title>@yield('title','AQG | Abraj Qatar Group')</title>
    @includeIf('include.styles')
</head>
<body class="hold-transition sidebar-mini">

<div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
     data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
    @includeIf('include.navbar')
    @includeIf('include.sidebar')
    @yield('content')
    @includeIf('include.footer')
</div>
@includeIf('include.scripts')

</body>
</html>

