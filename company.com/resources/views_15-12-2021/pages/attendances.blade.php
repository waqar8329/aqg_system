@extends('index')

@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('dashboard')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;Attendance</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            <!--
            <div class="row mt-5">
                <div class="col-sm-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Supervisor</h4>
                            <form action="projects.html" class="mt-5">
                                <div class="form-body">

                                   <div class="row">
                                           <div class="col-md-2">
                                           <label class="text-center text-dark text-right">Name</label>
                                           </div>

                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="supervisor_name"
                                                    placeholder="Enter supervisor name..">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions mt-3">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-maroon">Save</button>
                                        <button type="reset" class="btn btn-dark">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> -->
            @if(Auth::user()->title == 6 || Auth::user()->title == 0 )
            <div class="row mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title">Search By</h4>
                            <form action="{{route('hrattendance')}}" method="GET">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Projects</label>
                                            <select onchange="getSupervisor()" class="custom-select form-control" name="projectid">
                                                <option value="" selected="selected">Choose</option>
                                                @foreach($datas['projects'] as $data)
                                                    <option {{isset($searches['projectid']) && $searches['projectid'] == $data['id']? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Supervisor</label>
                                            <input value="{{$searches['supervisorname'] ?? ''}}" name="supervisorname" readonly class="custom-select form-control" id="supervisor" />
                                        </div>
                                    </div>

                                    {{--<div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Supervisor</label>
                                            <select class="custom-select form-control" name="supervisorid">
                                                <option value="" selected="selected">Choose</option>
                                                @foreach($datas['supervisors'] as $data)
                                                    <option {{isset($searches['supervisorid']) && $searches['supervisorid'] == $data['id']? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>--}}


                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">From</label>
                                            <input  value="{{isset($searches['from']) && $searches['from'] ? $searches['from'] :''}}" name="from" type="date" class="form-control">
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">To</label>
                                            <input value="{{isset($searches['to']) && $searches['to'] ?$searches['to']:''}}" name="to" type="date" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right" style="visibility: hidden"> Search</label>
                                            <input type="submit" value="Search" class="btn btn-block btn-primary">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right" style="visibility: hidden"> Reset</label>
                                            <input type="reset" onclick="resetPage('{{route('hrattendance')}}')" value="Reset" class="btn btn-block btn-dark">
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>

            </div>
            @endif
            <div class="row mt-5">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Current Attendance</h4>
                            @include('include.flash-messages')
                            <form method="post" action="{{route('saveattendance')}}">
                                @csrf
                            <div class="table-responsive mt-5">
                                <table id="attendance_default" class="table table-striped table-bordered display no-wrap"
                                       style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee #</th>
                                        <th>Name</th>
                                        <th>Job Title</th>
                                        <th>Project</th>
                                        <!-- days of the month -->
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <th>4</th>
                                        <th>5</th>

                                        <th>6</th>
                                        <th>7</th>
                                        <th>8</th>
                                        <th>9</th>
                                        <th>10</th>

                                        <th>11</th>
                                        <th>12</th>
                                        <th>13</th>
                                        <th>14</th>
                                        <th>15</th>

                                        <th>16</th>
                                        <th>17</th>
                                        <th>18</th>
                                        <th>19</th>
                                        <th>20</th>

                                        <th>21</th>
                                        <th>22</th>
                                        <th>23</th>
                                        <th>24</th>
                                        <th>25</th>

                                        <th>26</th>
                                        <th>27</th>
                                        <th>28</th>
                                        <th>29</th>
                                        <th>30</th>
                                        <th>31</th>
                                        <!-- end - days of the month -->

                                        <th>Created at</th>
                                        <th>Total Days</th>
                                        <th>Over Time</th>
                                        <th>Note</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($datas['data'] as $key => $data2)
                                        @if(isset($data2['attendace']))
                                        @foreach($data2['attendace'] as $data)
                                    <tr id="{{$data2['id']}}">
                                        @php $sno = $key+1 @endphp
                                        <td>{{$sno}}</td> <!-- sno-->
                                        <td><input type="hidden" value="{{$data2['id']}}" name="employeeid[]">{{$data2['id']}}</td>  <!-- id-->
                                        <td>{{$data2['name']}}</td>  <!-- empname-->
                                        <td>{{$data2['jobtitlename']}}</td>  <!-- job title-->
                                        <td>{{$data2['projectname']}}</td>  <!-- prject name-->

                                        <!-- attendance day -->
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][0]) && $data['attendance'][0] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][0]) && $data['attendance'][0] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][0]) && $data['attendance'][0] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][0]) && $data['attendance'][0] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][1]) && $data['attendance'][1] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][1]) && $data['attendance'][1] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][1]) && $data['attendance'][1] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][1]) && $data['attendance'][1] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][2]) && $data['attendance'][2] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][2]) && $data['attendance'][2] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][2]) && $data['attendance'][2] == 3? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][2]) && $data['attendance'][2] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][3]) && $data['attendance'][3] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][3]) && $data['attendance'][3] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][3]) && $data['attendance'][3] ==3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][3]) && $data['attendance'][3] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][4]) && $data['attendance'][4] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][4]) && $data['attendance'][4] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][4]) && $data['attendance'][4] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][4]) && $data['attendance'][4] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>

                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][5]) && $data['attendance'][5] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][5]) && $data['attendance'][5] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][5]) && $data['attendance'][5] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][5]) && $data['attendance'][5] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][6]) && $data['attendance'][6] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][6]) && $data['attendance'][6] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][6]) && $data['attendance'][6] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][6]) && $data['attendance'][6] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][7]) && $data['attendance'][7] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][7]) && $data['attendance'][7] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][7]) && $data['attendance'][7] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][7]) && $data['attendance'][7] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][8]) && $data['attendance'][8] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][8]) && $data['attendance'][8] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][8]) && $data['attendance'][8] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][8]) && $data['attendance'][8] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][9]) && $data['attendance'][9] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][9]) && $data['attendance'][9] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][9]) && $data['attendance'][9] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][9]) && $data['attendance'][9] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>

                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][10]) && $data['attendance'][10] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][10]) && $data['attendance'][10] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][10]) && $data['attendance'][10] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][10]) && $data['attendance'][10] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][11]) && $data['attendance'][11] == 1 ? 'selected':'' }} value=""1">Present</option>
                                                <option {{isset($data['attendance'][11]) && $data['attendance'][11] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][11]) && $data['attendance'][11] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][11]) && $data['attendance'][11] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][12]) && $data['attendance'][12] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][12]) && $data['attendance'][12] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][12]) && $data['attendance'][12] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][12]) && $data['attendance'][12] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][13]) && $data['attendance'][13] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][13]) && $data['attendance'][13] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][13]) && $data['attendance'][13] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][13]) && $data['attendance'][13] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][14]) && $data['attendance'][14] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][14]) && $data['attendance'][14] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][14]) && $data['attendance'][14] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][14]) && $data['attendance'][14] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>

                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][15]) && $data['attendance'][15] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][15]) && $data['attendance'][15] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][15]) && $data['attendance'][15] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][15]) && $data['attendance'][15] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][16]) && $data['attendance'][16] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][16]) && $data['attendance'][16] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][16]) && $data['attendance'][16] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][16]) && $data['attendance'][16] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][17]) && $data['attendance'][17] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][17]) && $data['attendance'][17] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][17]) && $data['attendance'][17] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][17]) && $data['attendance'][17] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][18]) && $data['attendance'][18] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][18]) && $data['attendance'][18] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][18]) && $data['attendance'][18] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][18]) && $data['attendance'][18] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][19]) && $data['attendance'][19] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][19]) && $data['attendance'][19] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][19]) && $data['attendance'][19] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][19]) && $data['attendance'][19] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>

                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][20]) && $data['attendance'][20] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][20]) && $data['attendance'][20] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][20]) && $data['attendance'][20] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][20]) && $data['attendance'][20] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][21]) && $data['attendance'][21] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][21]) && $data['attendance'][21] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][21]) && $data['attendance'][21] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][21]) && $data['attendance'][21] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][22]) && $data['attendance'][22] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][22]) && $data['attendance'][22] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][22]) && $data['attendance'][22] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][22]) && $data['attendance'][22] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][23]) && $data['attendance'][23] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][23]) && $data['attendance'][23] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][23]) && $data['attendance'][23] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][23]) && $data['attendance'][23] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][24]) && $data['attendance'][24] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][24]) && $data['attendance'][24] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][24]) && $data['attendance'][24] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][24]) && $data['attendance'][24] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>

                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][25]) && $data['attendance'][25] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][25]) && $data['attendance'][25] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][25]) && $data['attendance'][25] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][25]) && $data['attendance'][25] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][26]) && $data['attendance'][26] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][26]) && $data['attendance'][26] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][26]) && $data['attendance'][26] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][26]) && $data['attendance'][26] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][27]) && $data['attendance'][27] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][27]) && $data['attendance'][27] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][27]) && $data['attendance'][27] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][27]) && $data['attendance'][27] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][28]) && $data['attendance'][28] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][28]) && $data['attendance'][28] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][28]) && $data['attendance'][28] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][28]) && $data['attendance'][28] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][29]) && $data['attendance'][29] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][29]) && $data['attendance'][29] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][29]) && $data['attendance'][29] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][29]) && $data['attendance'][29] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][30]) && $data['attendance'][30] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][30]) && $data['attendance'][30] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][30]) && $data['attendance'][30] == 3 ? 'selected':'' }} value="3">Leave</option>
                                                <option {{isset($data['attendance'][30]) && $data['attendance'][30] == 4 ? 'selected':'' }} value="4">Off</option>
                                            </select>
                                        </td>
                                        <!-- end attendance day -->

                                        <td>{{date("d/m/Y", strtotime($data['created_at'])) ?? 0}}</td>
                                        <td>{{$data['present'] ?? 0}}</td>
                                        <td><input type="text" name="overtime[]" value="{{$data['overtime'] ?? 0}}"></td>
                                        <td><input type="text" name="note[]" value="{{$data['note'] ?? ''}}"></td>
                                    </tr>
                                        @endforeach
                                        @endif
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee #</th>
                                        <th>Name</th>
                                        <th>Job Title</th>
                                        <th>Project</th>
                                        <!-- days of the month -->
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <th>4</th>
                                        <th>5</th>

                                        <th>6</th>
                                        <th>7</th>
                                        <th>8</th>
                                        <th>9</th>
                                        <th>10</th>

                                        <th>11</th>
                                        <th>12</th>
                                        <th>13</th>
                                        <th>14</th>
                                        <th>15</th>

                                        <th>16</th>
                                        <th>17</th>
                                        <th>18</th>
                                        <th>19</th>
                                        <th>20</th>

                                        <th>21</th>
                                        <th>22</th>
                                        <th>23</th>
                                        <th>24</th>
                                        <th>25</th>

                                        <th>26</th>
                                        <th>27</th>
                                        <th>28</th>
                                        <th>29</th>
                                        <th>30</th>
                                        <th>31</th>
                                        <!-- end - days of the month -->

                                        <th>Created at</th>
                                        <th>Total Days</th>
                                        <th>Over Time</th>
                                        <th>Note</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                                @if(!in_array(Auth::user()->title, [1,2, 6]))
                            <div class="row mt-5">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-block btn-success"><i class="fas fa-save"></i>&emsp;Save</button>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                                    @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>
    </div>
    <style>
        .dt-button-collection .dropdown-menu{
            display: block  !important;
        }
        #attendance_default select{
            width: 111px;
        }
    </style>
    <script>
        function getSupervisor(){
            var projectid = $('select[name="projectid"] option:selected').val();

            $.ajax({
                type: "GET",
                url: "{{route('getcustomernamebyprojectid')}}",
                data: {id: projectid},
                cache: false,
                success: function (data) {
                    $('#supervisor').val(data)
                }
            });
        }
    </script>
@stop
