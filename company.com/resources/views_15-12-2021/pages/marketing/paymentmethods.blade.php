@extends('index')

@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('dashboard')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;Payment Methods</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            @if(!in_array(Auth::user()->title, [1,2]))
            <div class="row mt-5">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Payment Method</h4>
                            <div  class="mt-5">
                                <div class="form-body">
                                    @csrf
                                    <div class="row">
                                        <div id="alert" style="display: none" class="alert alert-danger col-md-12">
                                            <strong>Note! </strong><label> Please read the comments carefully.</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Name</label>
                                                <input type="text" class="form-control" name="name"
                                                       placeholder="Enter method name..">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-actions mt-3">
                                    <div class="text-right">
                                        <button type="submit" onClick="addUser()"  class="btn btn-maroon">Save</button>
                                        <button type="reset" onClick="window.location.reload();" class="btn btn-dark">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <script>
                function addUser(){
                    $('#alert').hide();
                    var name = $('input[name="name"]').val();
                    var token = $('input[name="_token"]').val();
                    var error = '';
                    if(name.length < 1){
                        error = 'Name is required.';
                    }
                    if(error === ''){
                        $.ajax({
                            type: "POST",
                            url: "{{route('marketing.addpaymentmethod')}}",
                            data: {name: name, _token:token},
                            cache: false,
                            success: function (data) {
                                if(data == 1){
                                    successAlert(1)
                                }else {
                                    $('#alert').show();
                                    $('#alert label').html(data);
                                }
                            }
                        });
                    }else{
                        $('#alert').show();
                        $('#alert label').html(error);
                    }
                }
                function deleteMe(id){
                    $('#idfordelete').val(id);
                    $('#action').val('paymentmethods');
                }
            </script>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">All Payment Methods</h4>
                            <input hidden id="editurl" value="{{route('marketing.edit')}}">
                            <input hidden id="editid" value="">
                            <div class="table-responsive mt-5">
                                <table id="default_order" class="table table-striped table-bordered display no-wrap"
                                       style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($datas as $key => $data)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>{{$data['name']}}</td>
                                            <td>{{date("d/m/Y", strtotime($data['created_at']))}}</td>
                                            <td>
                                                @if(!in_array(Auth::user()->title, [1,2]))
                                                <button  onClick="edit('paymentmethods', {{$data['id']}})"  class="btn btn-sm btn-outline-dark" title="Edit"><i class="fas fa-edit"></i>&nbsp;Edit</button>&emsp;<button data-toggle="modal" data-target="#delete" onClick="deleteMe({{$data['id']}})" class="btn btn-sm btn-danger" title="Delete"><i class="fas fa-trash"></i>&nbsp;Delete</button>
                                            @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @includeIf('include.deletemodal')
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>

@stop
