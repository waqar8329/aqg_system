@extends('index')

@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('dashboard')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;Assigned Employees</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            <!--
            <div class="row mt-5">
                <div class="col-sm-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Supervisor</h4>
                            <form action="projects.html" class="mt-5">
                                <div class="form-body">

                                   <div class="row">
                                           <div class="col-md-2">
                                           <label class="text-center text-dark text-right">Name</label>
                                           </div>

                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="supervisor_name"
                                                    placeholder="Enter supervisor name..">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions mt-3">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-maroon">Save</button>
                                        <button type="reset" class="btn btn-dark">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="row mt-5">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="table-responsive mt-5">
                                <table id="attendance_default_order" class="table table-striped table-bordered display no-wrap"
                                       style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Project name</th>
                                        <th>Supervisor name</th>
                                        <th>No of employees</th>

                                        <th>Created Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $sno = 0; @endphp
                                    @foreach($datas['projects'] as $key => $data)
                                        @if($data['numemployes'])
                                            <tr>
                                                <td>{{++$sno}}</td>
                                                <td>{{$data['name']}}</td>
                                                <td>{{$data['supervisorname']}}</td>
                                                <td>{{$data['numemployes']}}</td>
                                                <td>{{date("d/m/Y", strtotime($data['created_at']))}}</td>
                                                <td>
                                                    @if(!in_array(Auth::user()->title, [1,2]))
                                                        <a href="{{route('hr.employ.search')}}?locked=1&project={{$data['id']}}&hideother=true"><button class="btn btn-sm btn-outline-dark" title="Edit"><i class="fas fa-eye"></i>View</button></a>
                                                        <button onclick="undoSend({{$data['id']}})" data-toggle="modal" data-target="#delete"  class="btn btn-sm btn-danger" title="Delete"><i class="fas fa-trash"></i>&nbsp;Delete</button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Project name</th>
                                        <th>Supervisor name</th>
                                        <th>No of employees</th>
                                        <th>Created Date</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>
        <script>
            function undoSend(prjectid){
                $.ajax({
                    type: "POST",
                    url: "{{route('hr.undosend')}}",
                    data: {prjectid: prjectid, _token:$('input[name="_token"]').val()},
                    cache: false,
                    success: function (data) {
                        location.reload()
                    }
                });
            }
        </script>
    </div>
    <style>
        #attendance_default_order_length{
            display: none;
        }
    </style>
@stop
