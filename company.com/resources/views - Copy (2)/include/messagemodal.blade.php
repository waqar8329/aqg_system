<div id="message_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="add_message"></p>
            </div>
        </div>
    </div>
</div>
<style>
#message_modal .modal-header{
    background: #7c8897;
    color: #fff;
}
</style>
<script>
    var isRedirect = false;
    function showModal(msg, redirect = false){
        isRedirect = redirect;
        $('#message_modal').modal('show')
        $('#message_modal #add_message').html(msg);
    }
    $('#message_modal').on('hidden.bs.modal', function () {
        var url = $('#redirecturl').val();
        if(isRedirect){
            if(typeof url != 'undefined' && url != "") {
                window.location.href = url;
            }else{
                location.reload()
            }
        }
    });
</script>
