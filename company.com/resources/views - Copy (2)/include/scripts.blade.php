<script src="{{asset('assets/dist/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/js/popper.min.js')}}"></script>
<script src="{{asset('assets/dist/js/bootstrap.min.js')}}"></script>



<script src="{{asset('assets/dist/js/app-style-switcher.js')}}"></script>
<script src="{{asset('assets/dist/js/feather.min.js')}}"></script>
<script src="{{asset('assets/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/js/sidebarmenu.js')}}"></script>
<script src="{{asset('assets/dist/js/custom.min.js')}}"></script>
<script src="{{asset('assets/dist/js/d3.min.js')}}"></script>
<script src="{{asset('assets/dist/js/c3.min.js')}}"></script>

<script src="{{asset('assets/dist/js/chartist.min.js')}}"></script>
<script src="{{asset('assets/dist/js/chartist-plugin-tooltip.min.js')}}"></script>
<script src="{{asset('assets/dist/js/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{asset('assets/dist/js/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('assets/dist/js/dashboard1.min.js')}}"></script>



<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.colVis.min.js"></script>

<!--Date picket
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>-->

<!--Search drop down-->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>

<script src="{{asset('assets/dist/js/custom.js')}}"></script>
<script>
    $(".preloader ").fadeOut();
</script>
@stack('scripts')
