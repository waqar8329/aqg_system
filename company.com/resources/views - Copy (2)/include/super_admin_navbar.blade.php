<nav class="main-header navbar navbar-expand navbar-blue navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
        @if(!\App\Helpers\Helper::isSuperAdmin(auth()->user()->email))
            <li class="nav-item d-none d-sm-inline-block {{ Route::current()->uri == 'dashboard' ? 'active' : '' }}">
                <a href="{{url('dashboard')}}" class="nav-link">Home</a>
            </li>
            @permission('order.readonly|order.fullaccess')
            <li class="nav-item d-none d-sm-inline-block {{ Route::current()->uri == 'list-orders' ? 'active' : '' || Route::current()->uri == 'order' ? 'active' : '' }}">
                <a href="{{url('list-orders')}}" class="nav-link">Orders</a>
            </li>
            @endpermission
            @permission('quote.readonly|quote.fullaccess')
            <li class="nav-item d-none d-sm-inline-block {{ Route::current()->uri == 'quote' ? 'active' : '' }}">
                <a href="{{url('quote')}}" class="nav-link">Quote</a>
            </li>
            @endpermission
            {{--@permission('ship.readonly|ship.fullaccess')
                <li class="nav-item d-none d-sm-inline-block {{ Route::current()->uri == 'ship' ? 'active' : '' }}">
                    <a href="{{url('ship')}}" class="nav-link">Ship</a>
                </li>
            @endpermission--}}
            @permission('shipments.readonly|shipments.fullaccess')
            <li class="nav-item d-none d-sm-inline-block {{ (Route::current()->uri == 'shipments' || Route::current()->uri == 'makeduplicateofshipment' || Route::current()->uri == 'singleshipmentdetail') ? 'active' : '' }}">
                <a href="{{url('shipments')}}" class="nav-link">Shipments</a>
            </li>
            @endpermission
            @permission('users.readonly|users.fullaccess|addressbook.readonly|addressbook.fullaccess|commodities.readonly|commodities.fullaccess')
            <li class="nav-item d-none d-sm-inline-block
                {{
                    Route::current()->uri == 'users' ? 'active' : '' ||
                    Route::current()->uri == 'user-address-book' ? 'active' : '' ||
                    Route::current()->uri == 'freight-commodities' ? 'active' : '' ||
                    Route::current()->uri == 'parcel-box-sizes' ? 'active' : '' ||
                    Route::current()->uri == 'handling_units' ? 'active' : ''
                }}
                ">
                <a href="{{route('users')}}" class="nav-link">Directories</a>
            </li>
            @endpermission
            @permission('storeconnections.readonly|storeconnections.fullaccess')
            <li class="nav-item d-none d-sm-inline-block
            {{
                Route::current()->uri == 'settings' ? 'active' : '' ||
                Route::current()->uri == 'connections' ? 'active' : ''
            }}
                ">
                <a href="{{url('connections')}}" class="nav-link">Settings</a>
            </li>
            @endpermission
        @endif
    </ul>
    {{--<div class="mobile-menu">
        <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</span>
        <ul class="sub-menu dropdown-menu" aria-labelledby="sub_menu">
            @if(\App\Helpers\Helper::isSuperAdmin(auth()->user()->email))
                <li class="{{ Route::current()->uri == 'dashboard' ? 'active' : '' }}">
                    <a href="{{url('/')}}">Home</a>
                </li>

                @permission('order.readonly|order.fullaccess')
                <li class="{{ Route::current()->uri == 'list-orders' ? 'active' : '' || Route::current()->uri == 'order' ? 'active' : '' }}">
                    <a href="{{url('list-orders')}}">Orders</a>
                </li>
                @endpermission
                @permission('quote.readonly|quote.fullaccess')
                <li class="{{ Route::current()->uri == 'quote' ? 'active' : '' }}">
                    <a href="{{url('quote')}}">Quote</a>
                </li>
                @endpermission
                --}}{{--@permission('ship.readonly|ship.fullaccess')
                    <li class="{{ Route::current()->uri == 'ship' ? 'active' : '' }}">
                        <a href="{{url('ship')}}">Ship</a>
                    </li>
                @endpermission--}}{{--
                @permission('shipments.readonly|.shipmentsfullaccess')
                <li class="{{ (Route::current()->uri == 'shipments' || Route::current()->uri == 'singleshipmentdetail') ? 'active' : '' }}">
                    <a href="{{url('shipments')}}">Shipments</a>
                </li>
                @endpermission
                @permission('users.readonly|users.fullaccess|addressbook.readonly|addressbook.fullaccess|commodities.readonly|commodities.fullaccess|handlingunit.fullaccess|handlingunit.readonly')
                <li class="{{ Route::current()->uri == 'users' ? 'active' : '' || Route::current()->uri == 'user-address-book' ? 'active' : '' || Route::current()->uri == 'freight-commodities' ? 'active' : '' || Route::current()->uri == 'parcel-box-sizes' ? 'active' : '' || Route::current()->uri == 'handling_units' ? 'active' : '' }}">
                    <a href="{{route('users')}}">Directories</a>
                </li>
                @endpermission
                @permission('storeconnections.readonly|storeconnections.fullaccess')
                <li class="{{ Route::current()->uri == 'settings' ? 'active' : '' || Route::current()->uri == 'connections' ? 'active' : '' }} ">
                    <a href="{{url('connections')}}">Settings</a>
                </li>
                @endpermission
            @endif
        </ul>
    </div>--}}
    @if(!\App\Helpers\Helper::isSuperAdmin(auth()->user()->email))
        <a href="javascript:;" class="btn-feedback" data-toggle="modal" data-target="#feed_back_modal">Feedback</a>
@endif
<!-- Right navbar links -->
    <ul class="navbar-nav ml-auto v-center">
        <div class="quick-menu">
            <div class="dropdown">
                <a class="guest-links pro-file-img" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                    @if(isset(auth()->user()->profile_image) && auth()->user()->profile_image != '' && auth()->user()->profile_image != '')
                        @if(config('app.env') == 'local')
                            <img class="img-fluid"
                                 src="{{URL::to('assets/profile_images/'.auth()->user()->profile_image)}}">
                        @else
                            <img class="img-fluid"
                                 src="{{URL::to('public/assets/profile_images/'.auth()->user()->profile_image)}}">
                        @endif
                    @else
                        <i class="fa fa-user"></i>
                    @endif
                </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                    <li>
                        <h6 class="mb-0">
                            <strong>{{ isset(Auth::user()->firstname) ? ucwords(Auth::user()->firstname) : ''  }} {{isset(Auth::user()->lastname ) ? ucwords(Auth::user()->lastname)  : ''  }}</strong>
                            <small>{{ isset(Auth::user()->email ) ? Auth::user()->email  : ''   }}</small>
                        </h6>
                    </li>
                    <li class="
                        {{ Route::current()->uri == 'profile' ? 'active' : '' ||
                        Route::current()->uri == 'preferences' ? 'active' : '' ||
                        Route::current()->uri == 'company-profile' ? 'active' : '' }}
                        ">
                        <a href="{{ route('profile') }}">Profile &amp; preferences</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Sign out') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </ul>
</nav>
<!-- /.navbar -->
