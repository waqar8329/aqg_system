<link rel="shortcut icon" href="{{asset('assets/backend/dist/img/favicon.png')}}">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">





<link type="text/css" rel="stylesheet" href="{{asset('assets/dist/css/c3.min.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/dist/css/chartist.min.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/dist/css/jquery-jvectormap-2.0.2.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('assets/dist/css/style.css')}}">
<!--Date picker-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">

<!--Search drop down-->
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<link type="text/css" rel="stylesheet" href="{{asset('assets/dist/css/custom.css')}}">

@stack('styles')
