<div id="delete" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete?</p>
            </div>
            <input type="hidden" id="idfordelete" />
            <input type="hidden" id="action" />
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" onClick="deleteRecord()" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</div>
<script>
    function deleteRecord(){
        var id = $('#idfordelete').val();
        var action = $('#action').val();
        var token = $('input[name="_token"]').val();
        $.ajax({
            type: "POST",
            url: "{{route('delete')}}",
            data: {action: action, id: id, _token:token},
            cache: false,
            success: function (data) {
                $('#delete').modal('hide');
                successAlert(3)
            }
        });
    }
</script>
