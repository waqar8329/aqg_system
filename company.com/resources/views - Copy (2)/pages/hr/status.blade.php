@extends('index')

@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('dashboard')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;Status</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            @if(!in_array(Auth::user()->title, [1,2]))
            <div class="row mt-5">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Status</h4>
                            <div  class="mt-5">
                                <div class="form-body">
                                    @csrf
                                    <div class="row">
                                        <div id="alert" style="display: none" class="alert alert-danger col-md-12">
                                            <strong>Note! </strong><label> Please read the comments carefully.</label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 1</label>
                                                <input type="text" class="form-control" name="status_1" placeholder="Enter status 1 name..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 2</label>
                                                <input type="text" class="form-control" name="status_2" placeholder="Enter status 2 name..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 3</label>
                                                <input type="text" class="form-control" name="status_3" placeholder="Enter status 3 name..">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 4</label>
                                                <input type="text" class="form-control" name="status_4" placeholder="Enter status 4 name..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 5</label>
                                                <input type="text" class="form-control" name="status_5" placeholder="Enter status 5 name..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 6</label>
                                                <input type="text" class="form-control" name="status_6" placeholder="Enter status 6 name..">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-actions mt-3">
                                    <div class="text-right">
                                        <button type="submit" onClick="addUser()"  class="btn btn-maroon">Save</button>
                                        <button type="reset" onClick="window.location.reload();" class="btn btn-dark">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <script>
                function addUser(){
                    $('#alert').hide();
                    var status_1 = $('input[name="status_1"]').val();
                    var status_2 = $('input[name="status_2"]').val();
                    var status_3 = $('input[name="status_3"]').val();
                    var status_4 = $('input[name="status_4"]').val();
                    var status_5 = $('input[name="status_5"]').val();
                    var status_6 = $('input[name="status_6"]').val();
                    var token = $('input[name="_token"]').val();
                    var error = '';
                    if(status_1.length < 1 && status_2.length < 1 && status_3.length < 1 && status_4.length < 1 && status_5.length < 1 && status_6.length < 1){
                        error = 'At least one field should be filled.';
                    }
                    if(error === ''){
                        $.ajax({
                            type: "POST",
                            url: "{{route('hr.addstatus')}}",
                            data: {status_1: status_1, status_2: status_2, status_3: status_3, status_4: status_4, status_5: status_5, status_6: status_6, _token:token},
                            cache: false,
                            success: function (data) {
                                if(data == 1){
                                    successAlert(1)
                                }else {
                                    $('#alert').show();
                                    $('#alert label').html(data);
                                }
                            }
                        });
                    }else{
                        $('#alert').show();
                        $('#alert label').html(error);
                    }
                }
                function deleteMe(id){
                    $('#idfordelete').val(id);
                    $('#action').val('status');
                }
            </script>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">All Statuses</h4>
                            <input hidden id="editurl" value="{{route('hr.edit')}}">
                            <input hidden id="editid" value="">
                            <div class="table-responsive mt-5">
                                <table id="default_order" class="table table-striped table-bordered display no-wrap"
                                       style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Status 1</th>
                                        <th>Status 2</th>
                                        <th>Status 3</th>
                                        <th>Status 4</th>
                                        <th>Status 5</th>
                                        <th>Status 6</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($datas as $key => $data)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$data['status_1']}}</td>
                                        <td>{{$data['status_2']}}</td>
                                        <td>{{$data['status_3']}}</td>
                                        <td>{{$data['status_4']}}</td>
                                        <td>{{$data['status_5']}}</td>
                                        <td>{{$data['status_6']}}</td>
                                        <td>
                                            @if(!in_array(Auth::user()->title, [1,2]))
                                            <button onClick="edit('status', {{$data['id']}})" class="btn btn-sm btn-outline-dark" title="Edit"><i class="fas fa-edit"></i>Edit</button>&emsp;<button data-toggle="modal" data-target="#delete" onClick="deleteMe({{$data['id']}})" class="btn btn-sm btn-danger" title="Delete"><i class="fas fa-trash"></i>&nbsp;Delete</button>
                                        @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Status 1</th>
                                        <th>Status 2</th>
                                        <th>Status 3</th>
                                        <th>Status 4</th>
                                        <th>Status 5</th>
                                        <th>Status 6</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @includeIf('include.deletemodalhr')
    <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>

@stop
