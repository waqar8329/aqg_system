@extends('index')
@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('hr.employ')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;Employee</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            <div class="row mt-5">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Employee</h4>
                            <form method="post" enctype="multipart/form-data" action="{{route('hr.saveemployee')}}" class="mt-5">
                                @csrf
                                <div class="form-body">
                                    @include('include.flash-messages')
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Employee No.</label>
                                                <input onkeypress="return /[0-9a-zA-Z]/i.test(event.key)" value="{{old('employeeno')}}" required type="text" class="form-control" name="employeeno"
                                                       placeholder="Enter employee number..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Name</label>
                                                <input value="{{old('employeeno')}}" required type="text" class="form-control" name="emp_name"
                                                       placeholder="Enter employee name..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Job Title</label>
                                                <select required class="custom-select form-control" name="job_title">
                                                    <option value="" selected="selected">Choose title</option>
                                                    @foreach($datas['jobtitles'] as $data)
                                                        <option {{old('job_title') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">


                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Upload File</label>
                                            <div class="form-group">
                                                <div class="input-group-prepend">
                                                </div>
                                                <div class="custom-file">
                                                    <input name="file" type="file" class="custom-file-input" id="inputGroupFile01">
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Gender</label>
                                                <select required class="custom-select form-control" name="gender">
                                                    <option value="" selected="selected">Choose gender</option>
                                                    <option {{old('gender') == 'male' ? 'selected':''}} value="male">Male</option>
                                                    <option {{old('gender') == 'female' ? 'selected':''}} value="female">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label  class="text-center text-dark text-right">Nationality</label>

                                                <select required id="nationality" name="nationality" class="form-control">
                                                    <option value="" selected="selected" disabled>Choose</option>
                                                    <option value="Afghanistan">Afghanistan</option>
                                                    <option value="Åland Islands">Åland Islands</option>
                                                    <option value="Albania">Albania</option>
                                                    <option value="Algeria">Algeria</option>
                                                    <option value="American Samoa">American Samoa</option>
                                                    <option value="Andorra">Andorra</option>
                                                    <option value="Angola">Angola</option>
                                                    <option value="Anguilla">Anguilla</option>
                                                    <option value="Antarctica">Antarctica</option>
                                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                    <option value="Argentina">Argentina</option>
                                                    <option value="Armenia">Armenia</option>
                                                    <option value="Aruba">Aruba</option>
                                                    <option value="Australia">Australia</option>
                                                    <option value="Austria">Austria</option>
                                                    <option value="Azerbaijan">Azerbaijan</option>
                                                    <option value="Bahamas">Bahamas</option>
                                                    <option value="Bahrain">Bahrain</option>
                                                    <option value="Bangladesh">Bangladesh</option>
                                                    <option value="Barbados">Barbados</option>
                                                    <option value="Belarus">Belarus</option>
                                                    <option value="Belgium">Belgium</option>
                                                    <option value="Belize">Belize</option>
                                                    <option value="Benin">Benin</option>
                                                    <option value="Bermuda">Bermuda</option>
                                                    <option value="Bhutan">Bhutan</option>
                                                    <option value="Bolivia">Bolivia</option>
                                                    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                    <option value="Botswana">Botswana</option>
                                                    <option value="Bouvet Island">Bouvet Island</option>
                                                    <option value="Brazil">Brazil</option>
                                                    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                    <option value="Bulgaria">Bulgaria</option>
                                                    <option value="Burkina Faso">Burkina Faso</option>
                                                    <option value="Burundi">Burundi</option>
                                                    <option value="Cambodia">Cambodia</option>
                                                    <option value="Cameroon">Cameroon</option>
                                                    <option value="Canada">Canada</option>
                                                    <option value="Cape Verde">Cape Verde</option>
                                                    <option value="Cayman Islands">Cayman Islands</option>
                                                    <option value="Central African Republic">Central African Republic</option>
                                                    <option value="Chad">Chad</option>
                                                    <option value="Chile">Chile</option>
                                                    <option value="China">China</option>
                                                    <option value="Christmas Island">Christmas Island</option>
                                                    <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                    <option value="Colombia">Colombia</option>
                                                    <option value="Comoros">Comoros</option>
                                                    <option value="Congo">Congo</option>
                                                    <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                                    <option value="Cook Islands">Cook Islands</option>
                                                    <option value="Costa Rica">Costa Rica</option>
                                                    <option value="Cote D'ivoire">Cote D'ivoire</option>
                                                    <option value="Croatia">Croatia</option>
                                                    <option value="Cuba">Cuba</option>
                                                    <option value="Cyprus">Cyprus</option>
                                                    <option value="Czech Republic">Czech Republic</option>
                                                    <option value="Denmark">Denmark</option>
                                                    <option value="Djibouti">Djibouti</option>
                                                    <option value="Dominica">Dominica</option>
                                                    <option value="Dominican Republic">Dominican Republic</option>
                                                    <option value="Ecuador">Ecuador</option>
                                                    <option value="Egypt">Egypt</option>
                                                    <option value="El Salvador">El Salvador</option>
                                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                    <option value="Eritrea">Eritrea</option>
                                                    <option value="Estonia">Estonia</option>
                                                    <option value="Ethiopia">Ethiopia</option>
                                                    <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                    <option value="Faroe Islands">Faroe Islands</option>
                                                    <option value="Fiji">Fiji</option>
                                                    <option value="Finland">Finland</option>
                                                    <option value="France">France</option>
                                                    <option value="French Guiana">French Guiana</option>
                                                    <option value="French Polynesia">French Polynesia</option>
                                                    <option value="French Southern Territories">French Southern Territories</option>
                                                    <option value="Gabon">Gabon</option>
                                                    <option value="Gambia">Gambia</option>
                                                    <option value="Georgia">Georgia</option>
                                                    <option value="Germany">Germany</option>
                                                    <option value="Ghana">Ghana</option>
                                                    <option value="Gibraltar">Gibraltar</option>
                                                    <option value="Greece">Greece</option>
                                                    <option value="Greenland">Greenland</option>
                                                    <option value="Grenada">Grenada</option>
                                                    <option value="Guadeloupe">Guadeloupe</option>
                                                    <option value="Guam">Guam</option>
                                                    <option value="Guatemala">Guatemala</option>
                                                    <option value="Guernsey">Guernsey</option>
                                                    <option value="Guinea">Guinea</option>
                                                    <option value="Guinea-bissau">Guinea-bissau</option>
                                                    <option value="Guyana">Guyana</option>
                                                    <option value="Haiti">Haiti</option>
                                                    <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                                    <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                    <option value="Honduras">Honduras</option>
                                                    <option value="Hong Kong">Hong Kong</option>
                                                    <option value="Hungary">Hungary</option>
                                                    <option value="Iceland">Iceland</option>
                                                    <option value="India">India</option>
                                                    <option value="Indonesia">Indonesia</option>
                                                    <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                    <option value="Iraq">Iraq</option>
                                                    <option value="Ireland">Ireland</option>
                                                    <option value="Isle of Man">Isle of Man</option>
                                                    <option value="Italy">Italy</option>
                                                    <option value="Jamaica">Jamaica</option>
                                                    <option value="Japan">Japan</option>
                                                    <option value="Jersey">Jersey</option>
                                                    <option value="Jordan">Jordan</option>
                                                    <option value="Kazakhstan">Kazakhstan</option>
                                                    <option value="Kenya">Kenya</option>
                                                    <option value="Kiribati">Kiribati</option>
                                                    <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                                                    <option value="Korea, Republic of">Korea, Republic of</option>
                                                    <option value="Kuwait">Kuwait</option>
                                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                    <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                                    <option value="Latvia">Latvia</option>
                                                    <option value="Lebanon">Lebanon</option>
                                                    <option value="Lesotho">Lesotho</option>
                                                    <option value="Liberia">Liberia</option>
                                                    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                    <option value="Liechtenstein">Liechtenstein</option>
                                                    <option value="Lithuania">Lithuania</option>
                                                    <option value="Luxembourg">Luxembourg</option>
                                                    <option value="Macao">Macao</option>
                                                    <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                                    <option value="Madagascar">Madagascar</option>
                                                    <option value="Malawi">Malawi</option>
                                                    <option value="Malaysia">Malaysia</option>
                                                    <option value="Maldives">Maldives</option>
                                                    <option value="Mali">Mali</option>
                                                    <option value="Malta">Malta</option>
                                                    <option value="Marshall Islands">Marshall Islands</option>
                                                    <option value="Martinique">Martinique</option>
                                                    <option value="Mauritania">Mauritania</option>
                                                    <option value="Mauritius">Mauritius</option>
                                                    <option value="Mayotte">Mayotte</option>
                                                    <option value="Mexico">Mexico</option>
                                                    <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                    <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                    <option value="Monaco">Monaco</option>
                                                    <option value="Mongolia">Mongolia</option>
                                                    <option value="Montenegro">Montenegro</option>
                                                    <option value="Montserrat">Montserrat</option>
                                                    <option value="Morocco">Morocco</option>
                                                    <option value="Mozambique">Mozambique</option>
                                                    <option value="Myanmar">Myanmar</option>
                                                    <option value="Namibia">Namibia</option>
                                                    <option value="Nauru">Nauru</option>
                                                    <option value="Nepal">Nepal</option>
                                                    <option value="Netherlands">Netherlands</option>
                                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                    <option value="New Caledonia">New Caledonia</option>
                                                    <option value="New Zealand">New Zealand</option>
                                                    <option value="Nicaragua">Nicaragua</option>
                                                    <option value="Niger">Niger</option>
                                                    <option value="Nigeria">Nigeria</option>
                                                    <option value="Niue">Niue</option>
                                                    <option value="Norfolk Island">Norfolk Island</option>
                                                    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                    <option value="Norway">Norway</option>
                                                    <option value="Oman">Oman</option>
                                                    <option value="Pakistan">Pakistan</option>
                                                    <option value="Palau">Palau</option>
                                                    <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                    <option value="Panama">Panama</option>
                                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                                    <option value="Paraguay">Paraguay</option>
                                                    <option value="Peru">Peru</option>
                                                    <option value="Philippines">Philippines</option>
                                                    <option value="Pitcairn">Pitcairn</option>
                                                    <option value="Poland">Poland</option>
                                                    <option value="Portugal">Portugal</option>
                                                    <option value="Puerto Rico">Puerto Rico</option>
                                                    <option value="Qatar">Qatar</option>
                                                    <option value="Reunion">Reunion</option>
                                                    <option value="Romania">Romania</option>
                                                    <option value="Russian Federation">Russian Federation</option>
                                                    <option value="Rwanda">Rwanda</option>
                                                    <option value="Saint Helena">Saint Helena</option>
                                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                    <option value="Saint Lucia">Saint Lucia</option>
                                                    <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                    <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                                    <option value="Samoa">Samoa</option>
                                                    <option value="San Marino">San Marino</option>
                                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                                    <option value="Senegal">Senegal</option>
                                                    <option value="Serbia">Serbia</option>
                                                    <option value="Seychelles">Seychelles</option>
                                                    <option value="Sierra Leone">Sierra Leone</option>
                                                    <option value="Singapore">Singapore</option>
                                                    <option value="Slovakia">Slovakia</option>
                                                    <option value="Slovenia">Slovenia</option>
                                                    <option value="Solomon Islands">Solomon Islands</option>
                                                    <option value="Somalia">Somalia</option>
                                                    <option value="South Africa">South Africa</option>
                                                    <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                                    <option value="Spain">Spain</option>
                                                    <option value="Sri Lanka">Sri Lanka</option>
                                                    <option value="Sudan">Sudan</option>
                                                    <option value="Suriname">Suriname</option>
                                                    <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                    <option value="Swaziland">Swaziland</option>
                                                    <option value="Sweden">Sweden</option>
                                                    <option value="Switzerland">Switzerland</option>
                                                    <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                    <option value="Taiwan">Taiwan</option>
                                                    <option value="Tajikistan">Tajikistan</option>
                                                    <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                    <option value="Thailand">Thailand</option>
                                                    <option value="Timor-leste">Timor-leste</option>
                                                    <option value="Togo">Togo</option>
                                                    <option value="Tokelau">Tokelau</option>
                                                    <option value="Tonga">Tonga</option>
                                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                    <option value="Tunisia">Tunisia</option>
                                                    <option value="Turkey">Turkey</option>
                                                    <option value="Turkmenistan">Turkmenistan</option>
                                                    <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                    <option value="Tuvalu">Tuvalu</option>
                                                    <option value="Uganda">Uganda</option>
                                                    <option value="Ukraine">Ukraine</option>
                                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                                    <option value="United Kingdom">United Kingdom</option>
                                                    <option value="United States">United States</option>
                                                    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                    <option value="Uruguay">Uruguay</option>
                                                    <option value="Uzbekistan">Uzbekistan</option>
                                                    <option value="Vanuatu">Vanuatu</option>
                                                    <option value="Venezuela">Venezuela</option>
                                                    <option value="Viet Nam">Viet Nam</option>
                                                    <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                    <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                    <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                    <option value="Western Sahara">Western Sahara</option>
                                                    <option value="Yemen">Yemen</option>
                                                    <option value="Zambia">Zambia</option>
                                                    <option value="Zimbabwe">Zimbabwe</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Date Of Birth</label>
                                                <input value="{{old('dob') ?? ''}}" required type="date" class="form-control datepicker" name="dob">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Basic Salary</label>
                                                <input  value="{{old('basic_salary')}}" required type="number" class="form-control" name="basic_salary"
                                                       placeholder="Enter basic salary..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Main Allowance</label>
                                                <input  value="{{old('main_allowance')}}" type="number" class="form-control" name="main_allowance"
                                                       placeholder="Enter main allowance..">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Joining Date</label>
                                                <input value="{{old('jdate') ?? ''}}" type="date" class="form-control datepicker" name="jdate" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">ID No.</label>
                                                <input onkeypress="return /[0-9a-zA-Z]/i.test(event.key)" value="{{old('idno')}}" required type="number" class="form-control" name="idno"
                                                       placeholder="Enter ID number..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">ID Expiry Date</label>
                                                <input value="{{old('idepirydate') ?? ''}}" required type="date" class="form-control datepicker" name="idepirydate">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Passport Number</label>
                                                <input onkeypress="return /[0-9a-zA-Z]/i.test(event.key)" value="{{old('p_number')}}" required type="text" class="form-control" name="p_number"
                                                       placeholder="Enter passport number..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Passport Expiry Date</label>
                                                <input  value="{{old('pexpirydate') ?? ''}}" required type="date" class="form-control datepicker" name="pexpirydate">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">H-Card Expired Date</label>
                                                <input  value="{{old('hcardexpirydate') ?? ''}}" type="date" class="form-control datepicker" name="hcardexpirydate">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Contract Start Date</label>
                                                <input value="{{old('contractstartdate') ?? ''}}" type="date" class="form-control datepicker" name="contractstartdate">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Contract Expiry Date</label>
                                                <input value="{{old('contactexpirydate') ?? ''}}" type="date" class="form-control datepicker" name="contactexpirydate">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Last Settlement Received Date</label>
                                                <input value="{{old('lastsetrecdate') ?? ''}}" type="date" class="form-control datepicker" name="lastsetrecdate">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Insurance Start Date</label>
                                                <input value="{{old('insurancestartdate')  ?? ''}}" type="date" class="form-control datepicker" name="insurancestartdate">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Insurance Expiry Date</label>
                                                <input value="{{old('insuranceexpirydate') ?? ''}}" type="date" class="form-control datepicker" name="insuranceexpirydate">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-4" style="display: none;">
                                            <label class="text-center text-dark text-right">Note:</label>
                                            <div class="form-group">
                                                <textarea  class="form-control" rows="3" name="note" placeholder="Add note here..">{{old('note')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-5">
                                        <div class="col-md-6">
                                            <h5 class="card-title">Project Details </h5>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Project</label>
                                                <select onchange="getSupervisor()" required class="custom-select form-control" name="project">
                                                    <option  value="" selected="selected">Choose project</option>
                                                    @foreach($datas['projects'] as $data)
                                                        <option {{old('project') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Supervisor</label>
                                                <input value="{{old('supervisorname')}}" name="supervisorname" readonly class="custom-select form-control" id="supervisor" />
                                            </div>
                                        </div>
                                    </div>

                                    <!-- payment 1 row -->
                                    <div class="row mt-5">
                                        <div class="col-md-6">
                                            <h5 class="card-title">Statuses</h5>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 1</label>
                                                <select name="status_1" class="custom-select form-control" id="status_1">
                                                    <option value="" selected="selected">Choose status</option>
                                                    @foreach($datas['status_1'] as $data)
                                                        <option {{old('status_1') == $data ? 'selected':''}} value="{{$data}}">{{$data}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 2</label>
                                                <select name="status_2" class="custom-select form-control" id="status_2">
                                                    <option value="" selected="selected">Choose status</option>
                                                    @foreach($datas['status_2'] as $data)
                                                        <option {{old('status_2') == $data ? 'selected':''}} value="{{$data}}">{{$data}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 3</label>
                                                <select name="status_3" class="custom-select form-control" id="status_3">
                                                    <option value="" selected="selected">Choose status</option>
                                                    @foreach($datas['status_3'] as $data)
                                                        <option {{old('status_3') == $data ? 'selected':''}} value="{{$data}}">{{$data}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 4</label>
                                                <select value="" name="status_4" class="custom-select form-control" id="status_4">
                                                    <option value="" selected="selected">Choose status</option>
                                                    @foreach($datas['status_4'] as $data)
                                                        <option {{old('status_4') == $data ? 'selected':''}} value="{{$data}}">{{$data}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 5</label>
                                                <select  name="status_5" class="custom-select form-control" id="status_5">
                                                    <option value="" selected="selected">Choose status</option>
                                                    @foreach($datas['status_5'] as $data)
                                                        <option {{old('status_5') == $data ? 'selected':''}} value="{{$data}}">{{$data}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Status 6</label>
                                                <select name="status_6" class="custom-select form-control" id="status_6">
                                                    <option value="" selected="selected">Choose status</option>
                                                    @foreach($datas['status_6'] as $data)
                                                        <option {{old('status_6') == $data ? 'selected':''}} value="{{$data}}">{{$data}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                                <div class="form-actions mt-3">
                                    <div class="text-right">
                                        <button onclick="save()" class="btn btn-maroon">Save</button>
                                        <button type="reset" onClick="window.location.reload();" class="btn btn-dark">Reset</button>
                                        <button type="reset" onClick="window.location.href = '{{route('hr.employ')}}'" class="btn btn-dark">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>

    </div>
    <script>
        document.getElementById('nationality').value = "{{old('nationality')}}"
        function save(){
            $('#alert').hide();
            var error = '';
            if($('#referenceno').val().length < 1){
                error = "Reference no";
            }else if($('#contact').val().length < 1){
                error = "Contact";
            }else if($('#customer option:selected').val().length < 1){
                error = "Customer name";
            }else if($('#category option:selected').val().length < 1){
                error = "Category";
            }else if($('#project option:selected').val().length < 1){
                error = "Project";
            }else if($('#supervisor option:selected').val().length < 1){
                error = "supervisor";
            }else if($('#service option:selected').val().length < 1){
                error = "service";
            }else if($('#zone option:selected').val().length < 1){
                error = "Location";
            }
            var amount_1 = $('input[name="amount_1"]').val() ? parseInt($('input[name="amount_1"]').val()) : 0;
            var amount_2 = $('input[name="amount_2"]').val() ? parseInt($('input[name="amount_2"]').val()) : 0;
            var amount_3 = $('input[name="amount_3"]').val() ? parseInt($('input[name="amount_3"]').val()) : 0;
            var amount_4 = $('input[name="amount_4"]').val() ? parseInt($('input[name="amount_4"]').val()) : 0;

            var fees = parseInt($('input[name="fees"]').val());
            var total_amount = amount_1+amount_2+amount_3+amount_4;
            if(total_amount > fees){
                error = "Total of Payments should be less than the Income Fees.";
                $('#alert').show();
                $('#alert label').html(error);
                event.preventDefault();
            }else {
                if (error !== "") {
                    error = error + ' is required.';
                    $('#alert').show();
                    $('#alert label').html(error);
                    event.preventDefault();
                }
            }
        }
        function getSupervisor(){
            var projectid = $('select[name="project"] option:selected').val();
            $.ajax({
                type: "GET",
                url: "{{route('getcustomernamebyprojectid')}}",
                data: {id: projectid},
                cache: false,
                success: function (data) {
                    $('#supervisor').val(data)
                }
            });
        }
        document.querySelector("input[name='idno']").addEventListener("keypress", function (evt) {
            preventData(evt);
        })
        function preventData(evt){
            if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                evt.preventDefault();
            }
        }
    </script>
    @if ($message = Session::get('success'))
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            $('document').ready(function (){
                showModal("{{ $message }}")
            })
        </script>
    @endif
    @if ($message = Session::get('error'))
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            $('document').ready(function (){
                showModal("{{ $message }}")
            })
        </script>
    @endif
@stop
