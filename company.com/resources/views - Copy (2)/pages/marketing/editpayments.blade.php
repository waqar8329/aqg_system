@extends('index')
@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('marketing.payments')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;Payments</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            <div class="row mt-5">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Edit Payment</h4>
                            <form method="post" enctype="multipart/form-data" action="{{route('marketing.payments.update')}}" class="mt-5">
                                @csrf
                                <input hidden name="id" value="{{$payment['id']}}">
                                <div class="form-body">
                                    <div class="row">
                                        <div id="alert" style="display: none" class="alert alert-danger col-md-12">
                                            <strong>Note! </strong><label> Please read the comments carefully.</label>
                                        </div>
                                        {{--@include('include.flash-messages')--}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Reference No.</label>
                                                <input type="text" class="form-control" name="referenceno"
                                                       id="referenceno" value="{{$payment['referenceno']}}" placeholder="Enter reference number..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Contact No.</label>
                                                {{--<input value="{{$payment['contact']}}" required type="number" class="form-control" name="contact" id="contact" placeholder="Enter contact number..">--}}
                                                <select onchange="selectContact()" class="form-control" name="contact" id="contactlist">
                                                    <option value="" selelected>Select</option>
                                                    @foreach($datas['customers'] as $data)
                                                        <option {{$payment['contact'] == $data['contact'] ? 'selected' :'' }} value="{{$data['contact']}}"> {{$data['contact']}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Customer Name</label>
                                                <input type="hidden" readonly="readonly" required name="customerid" class="custom-select form-control" value="{{$payment['customerid'] ?? ''}}" placeholder="Customer" id="customer" >
                                                <select disabled name="testcustomer" class="custom-select form-control" id="testcustomer">
                                                    <option value="" selected="selected">Choose customer</option>
                                                    @foreach($datas['customers'] as $data)
                                                        <option {{ $payment['customerid'] == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Category</label>
                                                <select required name="categoryid" class="custom-select form-control" id="category">
                                                    <option value="" selected="selected">Choose category</option>
                                                    @foreach($datas['category'] as $data)
                                                        <option {{ $payment['categoryid'] == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Project</label>
                                                <select name="projectid" class="custom-select form-control" id="project">
                                                    <option value="" selected="selected">Choose project</option>
                                                    @foreach($datas['project'] as $data)
                                                        <option {{$payment['projectid'] == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Supervisor</label>
                                                <select required name="supervisor" class="custom-select form-control" id="supervisor">
                                                    <option value="" selected="selected">Choose supervisor</option>
                                                    @foreach($datas['supervisors'] as $data)
                                                        <option {{$payment['supervisor'] == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Service Date:</label>
                                                <input required value="{{ $payment['date'] }}" name="date" type="date" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Service</label>
                                            <div class="form-group">
                                                <select required name="serviceid" class="custom-select form-control" id="service">
                                                    <option value="" selected="selected">Choose service</option>
                                                    @foreach($datas['service'] as $data)
                                                        <option {{ $payment['serviceid'] == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Location</label>
                                                <select required name="locationid" class="custom-select form-control" id="zone">
                                                    <option value="" selected="selected">Choose location</option>
                                                    @foreach($datas['locations'] as $data)
                                                        <option {{$payment['locationid'] == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Income Fees</label>
                                            <div class="form-group">
                                                <input value="{{ $payment['fees'] }}" required name="fees" type="number" class="form-control" name="income_fees"
                                                       placeholder="Enter income fees..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Upload File
                                                {!! $payment['file'] !=='' && $payment['file'] != 0 ? '<font color="blue">(File exists)</font>':'' !!}
                                            </label>
                                            <div class="form-group">
                                                <div class="input-group-prepend">
                                                </div>
                                                <div class="custom-file">
                                                    <input value="{{$payment['file'] }}" name="file" type="file" class="custom-file-input" id="inputGroupFile01">
                                                    <label class="custom-file-label" for="inputGroupFile01">{{$payment['file'] !=='' && $payment['file'] != 0 ? $payment['file'] : 'Choose file'}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Note:</label>
                                            <div class="form-group">
                                                <textarea name="note" class="form-control" rows="3" placeholder="Add note here..">{{ $payment['note'] }}</textarea>
                                            </div>
                                        </div>

                                    </div>


                                    <!-- payment 1 row -->
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <h5 class="card-title">Payment 1</h5>

                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Amount</label>
                                            <div class="form-group">
                                                <input type="number" class="form-control" value="{{ $amount[0]['amount'] ?? ''}}" name="amount_1"
                                                       placeholder="Enter amount..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Date</label>
                                            <div class="form-group">
                                                <input type="date" name="date_1" value="{{$amount[0]['date'] ?? ''}}" class="form-control datepicker">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Method</label>
                                            <select class="custom-select form-control" name="method_1"  id="method_1">
                                                <option value="" selected="selected">Choose method</option>
                                                @foreach($datas['paymentmethods'] as $data)
                                                    <option {{ isset($amount[0]['paymentmethodid']) && $amount[0]['paymentmethodid'] == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <!-- payment 2 row -->
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <h5 class="card-title">Payment 2</h5>

                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Amount</label>
                                            <div class="form-group">
                                                <input type="number" class="form-control" value="{{$amount[1]['amount'] ?? ''}}" name="amount_2"
                                                       placeholder="Enter amount..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Date</label>
                                            <div class="form-group">
                                                <input type="date" class="form-control datepicker" name="date_2" value="{{$amount[1]['date'] ?? ''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Method</label>
                                            <select class="custom-select form-control" name="method_2"  id="method_2">
                                                <option value="" selected="selected">Choose method</option>
                                                @foreach($datas['paymentmethods'] as $data)
                                                    <option {{isset($amount[1]['paymentmethodid']) && $amount[1]['paymentmethodid'] == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <!-- payment 3 row -->
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <h5 class="card-title">Payment 3</h5>

                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Amount</label>
                                            <div class="form-group">
                                                <input type="number" class="form-control" value="{{$amount[2]['amount'] ?? ''}}" name="amount_3"
                                                       placeholder="Enter amount..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Date</label>
                                            <div class="form-group">
                                                <input type="date" class="form-control datepicker" name="date_3" value="{{$amount[2]['date'] ?? ''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Method</label>
                                            <select class="custom-select form-control" name="method_3"  id="method_3">
                                                <option value="" selected="selected">Choose method</option>
                                                @foreach($datas['paymentmethods'] as $data)
                                                    <option {{isset($amount[2]['paymentmethodid']) && $amount[2]['paymentmethodid'] == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <!-- payment 4 row -->
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <h5 class="card-title">Payment 4</h5>

                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Amount</label>
                                            <div class="form-group">
                                                <input type="number" class="form-control" value="{{$amount[3]['amount'] ?? ''}}" name="amount_4"
                                                       placeholder="Enter amount..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Date</label>
                                            <div class="form-group">
                                                <input type="date" class="form-control datepicker" name="date_4" value="{{$amount[3]['date'] ?? ''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Method</label>
                                            <select class="custom-select form-control" name="method_4" id="method_4">
                                                <option value="" selected="selected">Choose method</option>
                                                @foreach($datas['paymentmethods'] as $data)
                                                    <option {{ isset($amount[3]['paymentmethodid']) && $amount[3]['paymentmethodid'] == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>



                                </div>
                                <div class="form-actions mt-3">
                                    <div class="text-right">
                                        <button onclick="save()" class="btn btn-maroon">Save</button>
                                        <button type="reset" onClick="window.location.href = '{{route('marketing.payments')}}'" class="btn btn-dark">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>

    </div>
    @if ($message = Session::get('success'))
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            $('document').ready(function (){
                showModal("{{ $message }}")
            })
        </script>
    @endif
    @if ($message = Session::get('error'))
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            $('document').ready(function (){
                showModal("{{ $message }}")
            })
        </script>
    @endif
    <script>
        function save(){
            $('#alert').hide();
            var error = '';
            var error2 = '';
            if($('#contactlist option:selected').val().length < 1){
                error = "Contact is required.";
            }else if($('#customer').val().length < 1){
                error = "Customer name is required.";
            }else if($('#category option:selected').val().length < 1){
                error = "Category is required.";
            }else if($('#supervisor option:selected').val().length < 1){
                error = "Supervisor is required.";
            }else if($('input[name="date"').val().length < 1){
                error = "Date is required.";
            }else if($('#service option:selected').val().length < 1){
                error = "Service is required.";
            }else if($('#zone option:selected').val().length < 1){
                error = "Location is required.";
            }else if($('input[name="fees"]').val().length < 1){
                error = "Income fees is required.";
            }
            var amount_1 = $('input[name="amount_1"]').val() ? parseInt($('input[name="amount_1"]').val()) : 0;
            var amount_2 = $('input[name="amount_2"]').val() ? parseInt($('input[name="amount_2"]').val()) : 0;
            var amount_3 = $('input[name="amount_3"]').val() ? parseInt($('input[name="amount_3"]').val()) : 0;
            var amount_4 = $('input[name="amount_4"]').val() ? parseInt($('input[name="amount_4"]').val()) : 0;
            if(amount_1 > 0 && ($('#method_1 option:selected').val() == "" || $('input[name="date_1"]').val() === "")){
                error2 = "Date and method are required for payment 1";
            }
            if(amount_2 > 0 && ($('#method_2 option:selected').val() == "" || $('input[name="date_2"]').val() === "")){
                error2 = "Date and method are required for payment 2";
            }
            if(amount_3 > 0 && ($('#method_3 option:selected').val() == "" || $('input[name="date_3"]').val() === "")){
                error2 = "Date and method are required for payment 3";
            }
            if(amount_4 > 0 && ($('#method_4 option:selected').val() == "" || $('input[name="date_4"]').val() === "")){
                error2 = "Date and method are required for payment 4";
            }
            if (error2 !== "") {
                showModal(error2)
                event.preventDefault();
            }
            var fees = parseInt($('input[name="fees"]').val());
            var total_amount = amount_1+amount_2+amount_3+amount_4;
            if(total_amount > fees){
                error = "Total of Payments should be less than the Income Fees.";
                showModal(error)
                event.preventDefault();
            }else {
                if (error !== "") {
                    error = error + ' is required.';
                    showModal(error)
                    event.preventDefault();
                }
            }
        }
        function selectContact(){
            var contact = $('#contactlist option:selected').val();
            $.ajax({
                type: "GET",
                url: "{{route('marketing.getCustomerIdbyConctact')}}",
                data: {contact: contact},
                cache: false,
                success: function (data) {
                    $('#testcustomer').val(data)
                    setTimeout(
                        setCustomerName()
                        , 3000);


                }
            });
        }
        function setCustomerName(){
            var customername = $('#testcustomer option:selected').val()
            $('#customer').val(customername)
        }
    </script>
    <style>
        .select2-container{
            display: block;
            height: calc(1.5em + 0.75rem + 2px);
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #4F5467;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #e9ecef;
            border-radius: 2px;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        .select2-container--default .select2-selection--single{
            border: none;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            color: #4F5467;
        }
    </style>
@stop
