@extends('index')
@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('dashboard')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;View Payments</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            {{--<div class="row mt-5">
                <div class="col-md-12">
                    <div class="card bg-dark">
                        <div class="card-body">
                            <h4 class="card-title text-white">Message:</h4>
                            <h6 class="card-subtitle text-white">There are no locked records in this table...</h6>

                        </div>
                    </div>
                </div>
            </div>--}}
            <div class="card-group mt-3">
                <div class="card border-right">
                    <div class="card-body">
                        <div class="d-flex d-lg-flex d-md-block align-items-center">
                            <div>
                                <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium"><sup
                                        class="set-doller">QR</sup>18,306</h2>
                                <h6 class="text-dark font-weight-normal mb-0 w-100 text-truncate">Total Income
                                </h6>
                            </div>
                            <div class="ml-auto mt-md-3 mt-lg-0">
                                <span class="opacity-7 text-dark"><i data-feather="pocket"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card border-right">
                    <div class="card-body">
                        <div class="d-flex d-lg-flex d-md-block align-items-center">
                            <div>
                                <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium"><sup
                                        class="set-doller">QR</sup>18,306</h2>
                                <h6 class="text-dark font-weight-normal mb-0 w-100 text-truncate">Collected Payments
                                </h6>
                            </div>
                            <div class="ml-auto mt-md-3 mt-lg-0">
                                <span class="opacity-7 text-dark"><i data-feather="check-circle"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card border-right">
                    <div class="card-body">
                        <div class="d-flex d-lg-flex d-md-block align-items-center">
                            <div>
                                <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium"><sup
                                        class="set-doller">QR</sup>18,306</h2>
                                <h6 class="text-dark font-weight-normal mb-0 w-100 text-truncate">Uncollected Payments
                                </h6>
                            </div>
                            <div class="ml-auto mt-md-3 mt-lg-0">
                                <span class="opacity-7 text-dark"><i data-feather="book-open"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card col-lg-2">
                    <div class="card-body">
                        <div class="d-flex d-lg-flex d-md-block align-items-center col-md-4">
                            <div>
                                <button onClick="window.location.reload();" class="btn btn-outline-info"><i data-feather="rotate-ccw"></i>&nbsp;RESET</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- search filters -->
            <div class="row mt-5">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Lock/Unlock Records</h4>
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">From:</label>
                                            <input type="date" class="form-control" value="2018-05-13">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">To:</label>
                                            <input type="date" class="form-control" value="2018-05-13">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Lock Status:</label>
                                            <select class="custom-select form-control" id="service">
                                                <option selected="selected">Choose status</option>
                                                <option value="locked">Lock</option>
                                                <option value="unlocked">Unlock</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <button class="btn btn-block btn-success"><i class="fas fa-save"></i>&emsp;Save</button>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- bulk actions -->
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Bulk Actions</h4>
                            <div class="form-group mt-5 mb-3">
                                <button onclick="lockAll(true)" class="btn btn-block btn-success"><i class="fas fa-lock"></i>&emsp;Lock All Records</button>
                                <button onclick="lockAll(false)" class="btn btn-block btn-info"><i class="fas fa-unlock"></i>&emsp;Unlock All Records</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- print options -->
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Export</h4>
                            <h6 class="card-subtitle">Select export option from dropdown and click print</h6><br>
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12 mt-2 mb-5">
                                        <div class="input-group">
                                            <select class="custom-select form-control" id="service">
                                                <option selected="selected">Choose..</option>
                                                <option value="csv">CSV</option>
                                                <option value="excel">Excel</option>
                                                <option value="print">Print</option>
                                                <option value="selected_column">Columns</option>
                                            </select>
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button"><i class="fas fa-print"></i>&nbsp;Print</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="row mt-3">
                <!-- search filters -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Search By</h4>
                            <form action="{{route('marketing.payments.search')}}" method="GET">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group mt-3">
                                            <label class="text-center text-dark text-right">Reference No.</label>
                                            <input type="number" class="form-control" name="reference_number"
                                                   placeholder="Enter reference number..">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Contact No.</label>
                                            <input type="number" class="form-control" name="contact_number"
                                                   placeholder="Enter contact number..">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Customer Name</label>
                                            <input type="number" class="form-control" name="customer_name" placeholder="Enter customer name..">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Category</label>
                                            <select class="custom-select form-control" id="job_title">
                                                <option value="" selected="selected">Choose category</option>
                                                @foreach($datas['category'] as $data)
                                                    <option value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Project</label>
                                            <select class="custom-select form-control" id="project">
                                                <option selected="selected">Choose project</option>
                                                @foreach($datas['project'] as $data)
                                                    <option value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Supervisor</label>
                                            <select class="custom-select form-control" id="supervisor">
                                                <option selected="selected">Choose supervisor</option>
                                                @foreach($datas['supervisors'] as $data)
                                                    <option value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="text-center text-dark text-right">Service</label>
                                        <div class="form-group">
                                            <select class="custom-select form-control" id="service">
                                                <option selected="selected">Choose service</option>
                                                @foreach($datas['service'] as $data)
                                                    <option value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Location</label>
                                            <select class="custom-select form-control" id="zone">
                                                <option selected="selected">Choose location</option>
                                                @foreach($datas['locations'] as $data)
                                                    <option value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right ">From:</label>
                                            <input type="date" class="form-control" value="2018-05-13">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right ">To:</label>
                                            <input type="date" class="form-control" value="2018-05-13">
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Income Fees</label>
                                            <input type="number" class="form-control" name="income_fees"
                                                   placeholder="Enter income fees..">
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Payment Status:</label>
                                            <select class="custom-select form-control" id="service">
                                                <option selected="selected">Choose status</option>
                                                <option value="completed">Completed</option>
                                                <option value="incompleted">Incompleted</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Lock Status:</label>
                                            <select class="custom-select form-control" id="service">
                                                <option selected="selected">Choose status</option>
                                                <option value="1">Lock</option>
                                                <option value="0">Unlock</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right"> Search</label>
                                            <input type="submit" value="Search" class="btn btn-block btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Payments</h4>

                            <div class="table-responsive mt-5 ">
                                <table id="default_order" class="table table-striped table-bordered display no-wrap"
                                       style="width:100%">
                                    <thead>

                                    <tr>
                                        <th>Lock <br>Status</th>
                                        <th>Ref. #</th>
                                        <th>Contact #</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Project</th>
                                        <th>Supervisor</th>
                                        <th>Service</th>
                                        <th>Date</th>
                                        <th>Location</th>
                                        <th>Income <br>fees</th>
                                        <th>P1</th>
                                        <th>P1 <br>Date</th>
                                        <th>P1 Creation <br>Date</th>
                                        <th>P1 Method</th>
                                        <th>P2</th>
                                        <th>P2 <br>Date</th>
                                        <th>P2 Creation <br>Date</th>
                                        <th>P2 Method</th>
                                        <th>P3</th>
                                        <th>P3 <br>Date</th>
                                        <th>P3 Creation <br>Date</th>
                                        <th>P3 Method</th>
                                        <th>P4</th>
                                        <th>P4 <br>Date</th>
                                        <th>P4 Creation <br>Date</th>
                                        <th>P4 Method</th>
                                        <th>Balance</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    @foreach($datas['data'] as $data)
                                        <tr id="{{$data['id']}}" class="{{$data['islocked'] ? 'locked':''}}">
                                            <td> <!-- lock status -->
                                                <fieldset class="checkbox">
                                                    <label>
                                                        <input name="locked" onchange="lock({{$data['id']}})" {{$data['islocked'] ? 'checked':''}} type="checkbox" value=""> Locked
                                                    </label>
                                                </fieldset>
                                            </td>
                                            <td>{{$data['referenceno']}}</td> <!-- reference -->
                                            <td>{{$data['contact']}}</td> <!-- contact -->
                                            <td>{{$data['cname']}}</td> <!-- name -->
                                            <td>{{$data['catname']}}</td> <!-- category -->
                                            <td>{{$data['pname']}}</td> <!-- project -->
                                            <td>{{$data['supervisor']}}</td> <!-- supervisor -->
                                            <td>{{$data['servicename']}}</td> <!-- service -->
                                            <td>{{$data['date']}}</td> <!-- date -->
                                            <td>{{$data['locname']}}</td> <!-- location -->
                                            <td>{{$data['fees']}}</td> <!-- income fees -->
                                            <!-- payment 1 -->
                                            <td>{{$data['amount'][0]['amount'] ?? ''}}</td>
                                            <td>{{isset($data['amount'][0]['date']) ? date("d/m/Y", strtotime($data['amount'][0]['created_at']) ): ''}}</td>
                                            <td>{{isset($data['amount'][0]['created_at']) ? date("d/m/Y", strtotime($data['amount'][0]['created_at']) ): ''}}</td>
                                            <td>{{$data['amount'][0]['paymentmethod'] ?? ''}}</td>

                                            <!-- payment 2 -->
                                            <td>{{$data['amount'][1]['amount'] ?? ''}}</td>
                                            <td>{{isset($data['amount'][1]['date']) ? date("d/m/Y", strtotime($data['amount'][1]['created_at']) ): ''}}</td>
                                            <td>{{ isset($data['amount'][1]['created_at']) ? date("d/m/Y", strtotime($data['amount'][1]['created_at']) ): ''}}</td>
                                            <td>{{$data['amount'][1]['paymentmethod'] ?? ''}}</td>

                                            <!-- payment 3 -->
                                            <td>{{$data['amount'][2]['amount'] ?? ''}}</td>
                                            <td>{{isset($data['amount'][2]['date']) ? date("d/m/Y", strtotime($data['amount'][1]['created_at']) ): ''}}</td>
                                            <td>{{isset($data['amount'][2]['created_at']) ? date("d/m/Y", strtotime($data['amount'][2]['created_at']) ): ''}}</td>
                                            <td>{{$data['amount'][2]['paymentmethod'] ?? ''}}</td>

                                            <!-- payment 4 -->
                                            <td>{{$data['amount'][3]['amount'] ?? ''}}</td>
                                            <td>{{isset($data['amount'][3]['date']) ? date("d/m/Y", strtotime($data['amount'][1]['created_at']) ): ''}}</td>
                                            <td>{{isset($data['amount'][3]['created_at']) ? date("d/m/Y", strtotime($data['amount'][3]['created_at']) ): ''}}</td>
                                            <td>{{$data['amount'][3]['paymentmethod'] ?? ''}}</td>
                                            <td>
                                                @php
                                                    $fees = $data['fees'] ?? 0;
                                                    $amount1 = $data['amount'][0]['amount'] ?? 0;
                                                    $amount2 = $data['amount'][1]['amount'] ?? 0;
                                                    $amount3 = $data['amount'][2]['amount'] ?? 0;
                                                    $amount4 = $data['amount'][3]['amount'] ?? 0;
                                                    $balance = $fees - $amount1 - $amount2 - $amount3 - $amount4;
                                                @endphp
                                                {{$balance}}
                                            </td> <!-- balance -->
                                            <td><span class="badge badge-warning">{{$balance == 0 ? 'Complete':'Incomplete'}}</span></td> <!-- status -->
                                            <td><button onclick="location.href = '{{route('marketing.payments.edit', $data['id'])}}';" class="btn btn-sm btn-outline-dark" title="Edit"><i class="fas fa-edit"></i>&nbsp;Edit</button>&emsp;<button data-toggle="modal" data-target="#delete" onClick="deleteMe({{$data['id']}})" class="btn btn-sm btn-danger" title="Delete"><i class="fas fa-trash"></i>&nbsp;Delete</button>
                                                <a href="{{route('marketing.paymentbyrefno')}}/{{$data['referenceno']}}"> <button class="btn btn-sm btn-outline-primary" title="Duplicate file"><i class="fas fa-clone"></i>&nbsp;Duplicate</button></a>
                                                <button class="btn btn-sm btn-outline-primary" title="Download file"><i class="fas fa-download"></i>&nbsp;Download</button></td> <!-- action -->
                                        </tr>
                                    @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Lock <br>Status</th>
                                        <th>Ref. #</th>
                                        <th>Contact #</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Project</th>
                                        <th>Supervisor</th>
                                        <th>Service</th>
                                        <th>Date</th>
                                        <th>Location</th>
                                        <th>Income <br>fees</th>
                                        <th>P1</th>
                                        <th>P1 <br>Date</th>
                                        <th>P1 Creation <br>Date</th>
                                        <th>P1 Method</th>
                                        <th>P2</th>
                                        <th>P2 <br>Date</th>
                                        <th>P2 Creation <br>Date</th>
                                        <th>P2 Method</th>
                                        <th>P3</th>
                                        <th>P3 <br>Date</th>
                                        <th>P3 Creation <br>Date</th>
                                        <th>P3 Method</th>
                                        <th>P4</th>
                                        <th>P4 <br>Date</th>
                                        <th>P4 Creation <br>Date</th>
                                        <th>P4 Method</th>
                                        <th>Balance</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        @includeIf('include.deletemodal')
        <script>
            function deleteMe(id){
                $('#idfordelete').val(id);
                $('#action').val('payment');
            }
            function lock(id){
                var action = $('#'+id+' input[name="locked"]').is(':checked');
                $.ajax({
                    type: "POST",
                    url: "{{route('marketing.lock')}}",
                    data: {id: id, action: action, _token:$('input[name="_token"]').val()},
                    cache: false,
                    success: function (data) {
                        console.log(action)
                        if(action){
                            $('tr#'+id).addClass('locked');
                        }else{
                            $('tr#'+id).removeClass('locked');
                        }
                    }
                });
            }
            function lockAll(action){
                $.ajax({
                    type: "POST",
                    url: "{{route('marketing.lockall')}}",
                    data: { action: action ? 1 : 0, _token:$('input[name="_token"]').val()},
                    cache: false,
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        </script>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>
    </div>
    <style>
        tr.locked{
            color: #cd0a0a;
        }
    </style>
@stop
