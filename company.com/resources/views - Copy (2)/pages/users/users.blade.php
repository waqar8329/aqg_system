@extends('index')

@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('dashboard')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;Users</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            @if(!in_array(Auth::user()->title, [1,2]))
            <div class="row mt-5">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add User</h4>
                            <div  class="mt-5">
                                <div class="form-body">
                                    @csrf
                                    <div class="row">
                                        <div id="alert" style="display: none" class="alert alert-danger col-md-12">
                                            <strong>Note! </strong><label> Please read the comments carefully.</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Name</label>
                                                <input type="text" class="form-control" name="fullname"
                                                       placeholder="Enter name..">
                                            </div>
                                        </div>


                                        <div class="col-md-6" id="title">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Job Title</label>
                                                <select class="custom-select form-control" id="job_title">
                                                    <option value="" selected="selected">Choose</option>
                                                    @foreach($jobTitles as $job)
                                                    <option value="{{$job['id']}}">{{$job['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Contact</label>
                                                <input type="number" class="form-control" name="contact_number"
                                                    placeholder="Enter contact number..">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">E-mail</label>
                                                <input type="email" class="form-control" name="e_mail"
                                                    placeholder="Enter email address..">
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Username</label>
                                                <input type="text" class="form-control" name="email"
                                                       placeholder="Enter username..">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Password</label>
                                                <input type="password" class="form-control" name="password"
                                                       placeholder="Enter password..">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions mt-3">
                                    <div class="text-right">
                                        <button type="submit" onClick="addUser()"  class="btn btn-maroon">Save</button>
                                        <button type="reset" onclick="reset()" class="btn btn-dark">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <script>
                function addUser(){
                    $('#alert').hide();
                    var name = $('input[name="fullname"]').val();
                    var email = $('input[name="email"]').val();
                    var password = $('input[name="password"]').val();
                    var job_title = $('#job_title option:selected').val();
                    var token = $('input[name="_token"]').val();
                    var error = '';
                    if(name.length < 1){
                        error = 'Name is required.';
                    }else if(email.length < 1){
                        error = 'Email is required.';
                    }else if(password.length < 1){
                        error = 'Password is required.';
                    }else if(job_title.length < 1){
                        error = 'Job title is required.';
                    }
                    if(error === ''){
                        $.ajax({
                            type: "POST",
                            url: "{{route('adduser')}}",
                            data: {name: name, email: email, password: password, job_title: job_title, _token:token},
                            cache: false,
                            success: function (data) {
                                if(data == 1){
                                    successAlert(1);
                                }else {
                                    $('#alert').removeClass('alert-success').addClass('alert-danger').show();
                                    $('#alert label').html(data);
                                }
                            }
                        });
                    }else{
                        $('#alert').show();
                        $('#alert label').html(error);
                    }
                }
                function validateEmail(email) {
                    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(String(email).toLowerCase());
                }
                function deleteMe(id){
                    $('#idfordelete').val(id);
                    $('#action').val('user');
                }

            </script>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">All Users</h4>
                            <input hidden id="editurl" value="{{route('marketing.edit')}}">
                            <input hidden id="editid" value="">
                            <div class="table-responsive mt-5">
                                <table id="default_order" class="table table-striped table-bordered display no-wrap"
                                       style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Job Title</th>
                                        <!--<th>E-mail</th>
                                        <th>Username</th>-->
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key => $user)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$user['name']}}</td>
                                        <td>{{$user['email']}}</td>
                                        <td>{{$user['jobtitle']}}</td>
                                        <td>{{date("d/m/Y", strtotime($user['created_at']))}}</td>
                                        <td>
                                            @if(!in_array(Auth::user()->title, [1,2]))
                                            <button onClick="edit('user', {{$user['id']}})" class="btn btn-sm btn-outline-dark" title="Edit"><i class="fas fa-edit"></i>&nbsp;Edit</button>&emsp;<button data-toggle="modal" data-target="#delete" onClick="deleteMe({{$user['id']}})" class="btn btn-sm btn-danger" title="Delete"><i class="fas fa-trash"></i>&nbsp;Delete</button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Job Title</th>
                                        <!--<th>E-mail</th>
                                        <th>Username</th>-->
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @includeIf('include.deletemodal')
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>

@stop
