@extends('index')

@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('dashboard')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;Users</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            @if(!in_array(Auth::user()->title, [1,2]))
                <div class="row mt-5">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Reset Password</h4>
                                <div  class="mt-5">
                                    <div class="form-body">
                                        @csrf
                                        <div class="row">
                                            <div id="alert" style="display: none" class="alert alert-danger col-md-12">
                                                <strong>Note! </strong><label> Please read the comments carefully.</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-center text-dark text-right">New password</label>
                                                    <input type="text" class="form-control" name="password"
                                                           placeholder="Enter name..">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions mt-3">
                                        <div class="text-right">
                                            <button type="submit" onClick="addUser()"  class="btn btn-maroon">Save</button>
                                            <button type="reset" class="btn btn-dark">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <script>
                function addUser(){
                    $('#alert').hide();
                    var password = $('input[name="password"]').val();
                    var error = '';
                    if(password.length < 6){
                        error = 'Password length must be greater then 5.';
                    }
                    if(error === ''){
                        $.ajax({
                            type: "POST",
                            url: "{{route('updatepassword')}}",
                            data: {password: password, id: {{Auth::user()->id}}, _token:$('input[name="_token"]').val()},
                            cache: false,
                            success: function (data) {
                                if(data == 1){
                                    $('#alert').show();
                                    $('#alert').removeClass('alert-danger').addClass('alert-success');
                                    $('#alert').html('Password updated successfully.')
                                }
                            }
                        });
                    }else{
                        $('#alert').show();
                        $('#alert label').html(error);
                    }
                }
                function validateEmail(email) {
                    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(String(email).toLowerCase());
                }
                function deleteMe(id){
                    $('#idfordelete').val(id);
                    $('#action').val('user');
                }

            </script>
        </div>
    @includeIf('include.deletemodal')
    <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>

@stop
