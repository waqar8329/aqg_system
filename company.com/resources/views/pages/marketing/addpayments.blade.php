@extends('index')
@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('dashboard')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;Payments</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            <div class="row mt-5">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Payment</h4>
                            <form method="post" enctype="multipart/form-data" action="{{route('marketing.savepayment')}}" class="mt-5">
                                @csrf
                                <div class="form-body">
                                    <div class="row">
                                        <div id="alert" style="display: none" class="alert alert-danger col-md-12">
                                            <strong>Note! </strong><label> Please read the comments carefully.</label>
                                        </div>
                                        {{--@include('include.flash-messages')--}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Reference No.</label>
                                                <input hidden value="{{route('marketing.paymentbyrefno')}}" id="paymentbyrefno">
                                                <input autocomplete="off" list="referencenolist11" type="text" class="form-control referencenolist" name="referenceno"
                                                       id="referenceno" value="{{old('referenceno')}}" placeholder="Enter reference number..">
                                                <datalist id="referencenolist">
                                                    @foreach($datas['refnos'] as $data)
                                                        <option value="{{$data['referenceno']}}"> {{$data['referenceno']}} </option>
                                                    @endforeach
                                                </datalist>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Contact No.</label>
                                                {{--<input autocomplete="off" onchange="selectContact()" list="contactlist" value="{{old('contact')}}" required type="text" class="form-control" name="contact" id="contact" placeholder="Enter contact number..">
                                                <datalist id="contactlist">
                                                    @foreach($datas['customers'] as $data)
                                                        <option value="{{$data['contact']}}"> {{$data['contact']}} </option>
                                                    @endforeach
                                                </datalist>--}}
                                                <select onchange="selectContact()" class="form-control" name="contact" id="contactlist">
                                                    <option value="" selelected>Select</option>
                                                    @foreach($datas['customers'] as $data)
                                                        <option {{old('contact') == $data['contact'] ? 'selected' :'' }} value="{{$data['contact']}}"> {{$data['contact']}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Customer Name</label>
                                                <input type="hidden" readonly="readonly" required name="customerid" class="custom-select form-control" value="{{old('customerid')}}" placeholder="Customer" id="customer" >
                                                <select disabled required name="testcustomer" class="custom-select form-control" id="testcustomer">
                                                    <option  selected="selected">Choose customer</option>
                                                    @foreach($datas['customers'] as $data)
                                                        <option {{old('customerid') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Category</label>
                                                <select required name="categoryid" class="custom-select form-control" id="category">
                                                    <option value="" selected="selected">Choose category</option>
                                                    @foreach($datas['category'] as $data)
                                                        <option {{old('categoryid') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Project</label>
                                                <select name="projectid" class="custom-select form-control" id="project">
                                                    <option value="" selected="selected">Choose project</option>
                                                    @foreach($datas['project'] as $data)
                                                        <option {{old('projectid') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Supervisor</label>
                                                <select required name="supervisor" class="custom-select form-control" id="supervisor">
                                                    <option value="" selected="selected">Choose supervisor</option>
                                                    @foreach($datas['supervisors'] as $data)
                                                        <option {{old('supervisor') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Service Date</label>
                                                <input required value="{{ old('date') ??  '' }}" data-date="" name="date" type="date" autocomplete="off" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Service</label>
                                            <div class="form-group">
                                                <select required name="serviceid" class="custom-select form-control" id="service">
                                                    <option value="" selected="selected">Choose service</option>
                                                    @foreach($datas['service'] as $data)
                                                        <option {{old('serviceid') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="text-center text-dark text-right">Location</label>
                                                <select required name="locationid" class="custom-select form-control" id="zone">
                                                    <option value="" selected="selected">Choose location</option>
                                                    @foreach($datas['locations'] as $data)
                                                        <option {{old('locationid') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Income Fees</label>
                                            <div class="form-group">
                                                <input value="{{old('fees')}}" required name="fees" type="number" class="form-control" name="income_fees"
                                                       placeholder="Enter income fees..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Upload File</label>
                                            <div class="form-group">
                                                <div class="input-group-prepend">
                                                </div>
                                                <div class="custom-file">
                                                    <input value="{{old('file')}}" name="file" type="file" class="custom-file-input" id="inputGroupFile01">
                                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Note:</label>
                                            <div class="form-group">
                                                <textarea name="note" class="form-control" rows="3" placeholder="Add note here..">{{old('note')}}</textarea>
                                            </div>
                                        </div>

                                    </div>


                                    <!-- payment 1 row -->
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <h5 class="card-title">Payment 1</h5>

                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Amount</label>
                                            <div class="form-group">
                                                <input type="number" class="form-control" value="{{old('amount_1')}}" name="amount_1"
                                                       placeholder="Enter amount..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Date</label>
                                            <div class="form-group">
                                                <input type="date" name="date_1" value="{{ old('date_1') ??  '' }}" data-date="" class="form-control  datepicker">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Method</label>
                                            <select class="custom-select form-control" name="method_1"  id="method_1">
                                                <option value="" selected="selected">Choose method</option>
                                                @foreach($datas['paymentmethods'] as $data)
                                                    <option {{old('method_1') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <!-- payment 2 row -->
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <h5 class="card-title">Payment 2</h5>

                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Amount</label>
                                            <div class="form-group">
                                                <input type="number" class="form-control" value="{{old('amount_2')}}" name="amount_2"
                                                       placeholder="Enter amount..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Date</label>
                                            <div class="form-group">
                                                <input type="date" class="form-control datepicker" name="date_2" value="{{ old('date_2') ??  '' }}" data-date="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Method</label>
                                            <select class="custom-select form-control" name="method_2"  id="method_2">
                                                <option value="" selected="selected">Choose method</option>
                                                @foreach($datas['paymentmethods'] as $data)
                                                    <option {{old('method_2') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <!-- payment 3 row -->
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <h5 class="card-title">Payment 3</h5>

                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Amount</label>
                                            <div class="form-group">
                                                <input type="number" class="form-control" value="{{old('amount_3')}}" name="amount_3"
                                                       placeholder="Enter amount..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Date</label>
                                            <div class="form-group">
                                                <input type="date" class="form-control datepicker" name="date_3" value="{{ old('date_3') ??  '' }}" data-date="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Method</label>
                                            <select class="custom-select form-control" name="method_3"  id="method_3">
                                                <option value="" selected="selected">Choose method</option>
                                                @foreach($datas['paymentmethods'] as $data)
                                                    <option {{old('method_3') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <!-- payment 4 row -->
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <h5 class="card-title">Payment 4</h5>

                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Amount</label>
                                            <div class="form-group">
                                                <input type="number" class="form-control" value="{{old('amount_4')}}" name="amount_4"
                                                       placeholder="Enter amount..">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Date</label>
                                            <div class="form-group">
                                                <input type="date" class="form-control datepicker" name="date_4" value="{{ old('date_4') ??  '' }}" data-date="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="text-center text-dark text-right">Method</label>
                                            <select class="custom-select form-control" name="method_4" id="method_4">
                                                <option value="" selected="selected">Choose method</option>
                                                @foreach($datas['paymentmethods'] as $data)
                                                    <option {{old('method_4') == $data['id'] ? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>



                                </div>
                                <div class="form-actions mt-3">
                                    <div class="text-right">
                                        <button onclick="save()" class="btn btn-maroon">Save</button>
                                        <button type="reset" onClick="window.location.reload();" class="btn btn-dark">Reset</button>
                                        <button type="reset" onClick="window.location.href = '{{route('marketing.payments')}}'" class="btn btn-dark">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>

    </div>
    <script>
        function save(){
            $('#alert').hide();
            var error = '';
            if($('#contactlist option:selected').val().length < 1){
                error = "Contact is required.";
            }else if($('#customer').val().length < 1){
                error = "Customer name is required.";
            }else if($('#category option:selected').val().length < 1){
                error = "Category is required.";
            }else if($('#supervisor option:selected').val().length < 1){
                error = "Supervisor is required.";
            }else if($('input[name="date"').val().length < 1){
                error = "Date is required.";
            }else if($('#service option:selected').val().length < 1){
                error = "Service is required.";
            }else if($('#zone option:selected').val().length < 1){
                error = "Location is required.";
            }else if($('input[name="fees"]').val().length < 1){
                error = "Income fees is required.";
            }
            var amount_1 = $('input[name="amount_1"]').val() ? parseInt($('input[name="amount_1"]').val()) : 0;
            var amount_2 = $('input[name="amount_2"]').val() ? parseInt($('input[name="amount_2"]').val()) : 0;
            var amount_3 = $('input[name="amount_3"]').val() ? parseInt($('input[name="amount_3"]').val()) : 0;
            var amount_4 = $('input[name="amount_4"]').val() ? parseInt($('input[name="amount_4"]').val()) : 0;
            if(amount_1 > 0 && ($('#method_1 option:selected').val() === "" || $('input[name="date_1"]').val() === "")){
                error = "Date and method are required for payment 1";
            }
            if(amount_2 > 0 && ($('#method_2 option:selected').val() === "" || $('input[name="date_2"]').val() === "")){
                error = "Date and method are required for payment 2";
            }
            if(amount_3 > 0 && ($('#method_3 option:selected').val() === "" || $('input[name="date_3"]').val() === "")){
                error = "Date and method are required for payment 3";
            }
            if(amount_4 > 0 && ($('#method_4 option:selected').val() === "" || $('input[name="date_4"]').val() === "")){
                error = "Date and method are required for payment 4";
            }
            var fees = parseInt($('input[name="fees"]').val());
            var total_amount = amount_1+amount_2+amount_3+amount_4;
            if(total_amount > fees){
                error = "Total of payments should be less than Income fees.";
                /*$('#alert').show();
                $('#alert label').html(error);*/
                showModal(error)
                event.preventDefault();
            }else {
                if (error !== "") {
                    showModal(error)
                    event.preventDefault();
                }
            }
        }
        function selectContact(){
            var contact = $('#contactlist option:selected').val();
            $.ajax({
                type: "GET",
                url: "{{route('marketing.getCustomerIdbyConctact')}}",
                data: {contact: contact},
                cache: false,
                success: function (data) {
                    $('#testcustomer').val(data)
                    setTimeout(
                        setCustomerName()
                        , 3000);


                }
            });
        }

        function setCustomerName(){
            var customername = $('#testcustomer option:selected').val()
            $('#customer').val(customername)
        }
        document.querySelector("input[name='amount_1']").addEventListener("keypress", function (evt) {
            preventData(evt);
        })
        document.querySelector("input[name='amount_2']").addEventListener("keypress", function (evt) {
            preventData(evt);
        })
        document.querySelector("input[name='amount_3']").addEventListener("keypress", function (evt) {
            preventData(evt);
        })
        document.querySelector("input[name='amount_4']").addEventListener("keypress", function (evt) {
            preventData(evt);
        })
        document.querySelector("input[name='fees']").addEventListener("keypress", function (evt) {
            preventData(evt);
        })
        function preventData(evt){
            if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                evt.preventDefault();
            }
        }
    </script>
    @if ($message = Session::get('success'))
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            $('document').ready(function (){
                showModal("{{ $message }}")
            })
        </script>
    @endif
    @if ($message = Session::get('error'))
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            $('document').ready(function (){
                showModal("{{ $message }}")
            })
        </script>
    @endif
@stop
