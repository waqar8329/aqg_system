@extends('index')
@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            @php
                $olddata = $datas['searchedData'] ?? [];
            @endphp
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('dashboard')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;View Employees</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            @if(!isset($olddata['hideother']))
            <div class="card-group mt-5">
                <div class="card border-right">
                    <div class="card-body">
                        <div class="d-flex d-lg-flex d-md-block align-items-center">
                            <div>
                                <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium">{{$locData['totalRecords'] ?? 0}}</h2>
                                <h6 class="text-dark font-weight-normal mb-0 w-100 text-truncate">Total Employees
                                </h6>
                            </div>
                            <div class="ml-auto mt-md-3 mt-lg-0">
                                <span class="opacity-7 text-dark"><i data-feather="users"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card border-right">
                    <div class="card-body">
                        <div class="d-flex d-lg-flex d-md-block align-items-center">
                            <div>
                                <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium"> {{$locData['totalLockRecords'] ?? 0}}</h2>
                                <h6 class="text-dark font-weight-normal mb-0 w-100 text-truncate">Total Locked Records
                                </h6>
                            </div>
                            <div class="ml-auto mt-md-3 mt-lg-0">
                                <span class="opacity-7 text-dark"><i data-feather="lock"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card border-right">
                    <div class="card-body">
                        <div class="d-flex d-lg-flex d-md-block align-items-center">
                            <div>
                                <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium">  {{$locData['totalUnlockRecords'] ?? 0}}</h2>
                                <h6 class="text-dark font-weight-normal mb-0 w-100 text-truncate">Total Unlocked Records
                                </h6>
                            </div>
                            <div class="ml-auto mt-md-3 mt-lg-0">
                                <span class="opacity-7 text-dark"><i data-feather="unlock"></i></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
                @if(Auth::user()->title == 0)
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Bulk Actions</h4> <br>
                            <div class="form-group  mb-2">
                                <button onclick="lockAll(true)" class="btn btn-block btn-success"><i class="fas fa-lock"></i>&emsp;Lock All Records</button>
                                <button onclick="lockAll(false)" class="btn btn-block btn-info"><i class="fas fa-unlock"></i>&emsp;Unlock All Records</button>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- print options -->
               {{-- <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Export</h4>
                            <h6 class="card-subtitle">Select export option from dropdown and click print</h6><br>
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12 mt-3 mb-3">
                                        <div class="input-group">
                                            <select class="custom-select form-control" id="service">
                                                <option selected="selected">Choose..</option>
                                                <option value="csv">CSV</option>
                                                <option value="excel">Excel</option>
                                                <option value="print">Print</option>
                                                <option value="selected_column">Columns</option>
                                            </select>
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button"><i class="fas fa-print"></i>&nbsp;Print</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
@endif

            <!-- lock/unlock actions -->
            <div class="row mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title">Search By</h4>
                            <form action="{{route('hr.employ.search')}}" method="GET">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Employee No.</label>
                                        <input value="{{$olddata['employee_number'] ?? ''}}" type="text" onkeypress="return /[0-9a-zA-Z]/i.test(event.key)" class="form-control" name="employee_number" placeholder="Enter employee number..">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Name</label>
                                        <input value="{{$olddata['emp_name'] ?? ''}}" type="text" class="form-control" name="emp_name"
                                               placeholder="Enter employee name..">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Job Title</label>
                                        <select class="custom-select form-control" name="job_title">
                                            <option value="" selected="selected">Choose title</option>
                                            @foreach($datas['jobtitles'] as $data)
                                                <option {{isset($olddata['job_title']) && $olddata['job_title'] == $data['id'] ?'selected':'' }} value="{{$data['id']}}">{{$data['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Project</label>
                                        <select class="custom-select form-control" name="project">
                                            <option value="" selected="selected">Choose project</option>
                                            @foreach($datas['projects'] as $data)
                                                <option {{isset($olddata['project']) && $olddata['project'] == $data['id'] ?'selected':'' }} value="{{$data['id']}}">{{$data['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Gender</label>
                                        <select name="gender" name="" class="custom-select form-control" id="gender">
                                            <option value="" selected="selected">Choose gender</option>
                                            <option {{isset($olddata['gender']) && $olddata['gender'] == 'male' ?'selected':'' }} value="male">Male</option>
                                            <option {{isset($olddata['gender']) && $olddata['gender'] == 'female' ?'selected':'' }} value="female">Female</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Nationality</label>
                                        <input  value="{{$olddata['nationality'] ?? ''}}" type="text" class="form-control" name="nationality"
                                               placeholder="Enter nationality..">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Date Of Birth (From)</label>
                                        <input  value="{{$olddata['dob_from'] ?? ''}}" name="dob_from" type="date" class="form-control datepicker">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Date Of Birth (To)</label>
                                        <input  value="{{$olddata['dob_to'] ??  ''}}" name="dob_to" type="date" class="form-control datepicker">
                                    </div>
                                </div>

                            </div>


                            <div class="row mt-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Basic Salary</label>
                                        <input  value="{{$olddata['basic_salary'] ?? ''}}" type="number" class="form-control" name="basic_salary"
                                               placeholder="Enter basic salary..">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Main Allowance</label>
                                        <input  value="{{$olddata['main_allowance'] ?? ''}}" type="number" class="form-control" name="main_allowance"
                                               placeholder="Enter main allowance..">
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Joining Date (From)</label>
                                        <input  value="{{$olddata['joining_from'] ??  ''}}" name="joining_from" type="date" class="form-control datepicker">
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Joining Date (To)</label>
                                        <input  value="{{$olddata['joining_to'] ?? ''}}" name="joining_to" type="date" class="form-control datepicker">
                                    </div>
                                </div>
                            </div>


                            <div class="row  mt-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">ID No.</label>
                                        <input  value="{{$olddata['id_no'] ?? ''}}" type="number" class="form-control" name="id_no"
                                               placeholder="Enter ID number..">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">ID Expiry Date (From)</label>
                                        <input  value="{{$olddata['id_expired_from'] ?? ''}}" name="id_expired_from" type="date" class="form-control datepicker">
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">ID Expiry Date (To)</label>
                                        <input  value="{{$olddata['id_expired_to'] ?? ''}}" name="id_expired_to" type="date" class="form-control datepicker">
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Passport Number</label>
                                        <input  value="{{$olddata['p_number'] ?? ''}}" type="number" class="form-control" name="p_number"
                                               placeholder="Enter passport number..">
                                    </div>
                                </div>
                            </div>

                            <div class="row  mt-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Passport Expiry Date (From)</label>
                                        <input value="{{$olddata['passport_expired_from'] ??  ''}}" name="passport_expired_from" type="date" class="form-control datepicker" value="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Passport Expiry Date (To)</label>
                                        <input value="{{$olddata['passport_expired_to'] ??  ''}}" name="passport_expired_to"  type="date" class="form-control datepicker">
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">H-Card Expired Date (From)</label>
                                        <input value="{{$olddata['hcart_expired_from'] ??  ''}}" name="hcart_expired_from" type="date" class="form-control datepicker">
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">H-Card Expired Date (To)</label>
                                        <input value="{{$olddata['hcart_expired_to'] ??  ''}}" name="hcart_expired_to" type="date" class="form-control datepicker" value="">
                                    </div>
                                </div>
                            </div>


                            <div class="row  mt-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Contract Start Date (From)</label>
                                        <input value="{{$olddata['contract_start_from'] ?? ''}}" name="contract_start_from" type="date" class="form-control datepicker">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Contract Start Date (To)</label>
                                        <input value="{{$olddata['contract_start_to'] ??  ''}}" name="contract_start_to" type="date" class="form-control datepicker">
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Contract Expiry Date (From)</label>
                                        <input value="{{$olddata['contract_expiry_from'] ?? ''}}" name="contract_expiry_from" type="date" class="form-control datepicker">
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Contract Expiry Date (To)</label>
                                        <input value="{{$olddata['contract_expiry_to'] ??  ''}}" name="contract_expiry_to" type="date" class="form-control datepicker">
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-3">
                                    <div class="form-group">

                                        <label class="text-center text-dark text-right">Insurane Start Date (From)</label>
                                        <input value="{{$olddata['insurance_start_from'] ??  ''}}" name="insurance_start_from" type="date" class="form-control datepicker">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Insurance Start Date (To)</label>
                                        <input value="{{$olddata['insurance_start_to'] ?? ''}}" name="insurance_start_to" type="date" class="form-control datepicker">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Insurane Expiry Date (From)</label>
                                        <input value="{{$olddata['insurance_expiry_from'] ??  ''}}" name="insurance_expiry_from" type="date" class="form-control datepicker">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Insurance Expiry Date (To)</label>
                                        <input value="{{$olddata['insurance_expiry_to'] ??  ''}}" name="insurance_expiry_to" type="date" class="form-control datepicker"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Last Settlement Received Date (From)</label>
                                        <input value="{{$olddata['set_from'] ??  ''}}" name="set_from" type="date" class="form-control datepicker">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Last Settlement Received Date (To)</label>
                                        <input value="{{$olddata['set_to'] ?? ''}}" name="set_to" type="date" class="form-control datepicker">
                                    </div>
                                </div>



                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Status 1</label>
                                        <select name="status_1" class="custom-select form-control" id="status_1">
                                            <option value="" selected="selected">Choose status</option>
                                            @foreach($datas['status_1'] as $data)
                                                <option {{isset($olddata['status_1']) && $olddata['status_1'] == $data ?'selected':'' }} value="{{$data}}">{{$data}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Status 2</label>
                                        <select name="status_2" class="custom-select form-control" id="status_2">
                                            <option value="" selected="selected">Choose status</option>
                                            @foreach($datas['status_2'] as $data)
                                                <option {{isset($olddata['status_2']) && $olddata['status_2'] == $data ?'selected':'' }} value="{{$data}}">{{$data}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="row mt-3">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Status 3</label>
                                        <select name="status_3" class="custom-select form-control" id="status_3">
                                            <option value="" selected="selected">Choose status</option>
                                            @foreach($datas['status_3'] as $data)
                                                <option {{isset($olddata['status_3']) && $olddata['status_3'] == $data ?'selected':'' }} value="{{$data}}">{{$data}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Status 4</label>
                                        <select value="" name="status_4" class="custom-select form-control" id="status_4">
                                            <option value="" selected="selected">Choose status</option>
                                            @foreach($datas['status_4'] as $data)
                                                <option {{isset($olddata['status_4']) && $olddata['status_4'] == $data ?'selected':'' }} value="{{$data}}">{{$data}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Status 5</label>
                                        <select  name="status_5" class="custom-select form-control" id="status_5">
                                            <option value="" selected="selected">Choose status</option>
                                            @foreach($datas['status_5'] as $data)
                                                <option {{isset($olddata['status_5']) && $olddata['status_5'] == $data ?'selected':'' }} value="{{$data}}">{{$data}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="text-center text-dark text-right">Status 6</label>
                                        <select name="status_6" class="custom-select form-control" id="status_6">
                                            <option value="" selected="selected">Choose status</option>
                                            @foreach($datas['status_6'] as $data)
                                                <option {{isset($olddata['status_6']) && $olddata['status_6'] == $data ?'selected':'' }} value="{{$data}}">{{$data}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Lock Status:</label>
                                            <select name="locked" class="custom-select form-control" id="status_2">
                                                <option value="" selected="selected">Select</option>
                                                <option {{isset($olddata['locked']) && $olddata['locked'] == 1 ?'selected':'' }} value="1">Locked</option>
                                                <option {{isset($olddata['locked']) && $olddata['locked'] == 0 ?'selected':'' }} value="0">Un Locked</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right" style="visibility: hidden"> Search</label>
                                            <input type="submit" value="Search" class="btn btn-block btn-primary" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right" style="visibility: hidden"> Reset</label>
                                            <input type="reset" onclick="resetPage('{{route('hr.employ')}}')" value="Reset" class="btn btn-block btn-dark" />
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>

            </div>

            @endif

            <div class="row mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12" style="margin-botton: -30px !important">
                                    <h4 class="card-title">Employees</h4>
                                </div>

                                <div id="alert" style="display: none" class="alert alert-danger col-md-12">
                                    <strong>Note! </strong><label> Please read the comments carefully.</label>
                                </div>
                            </div>

                            <div class="table-responsive">
                                @if(!in_array(Auth::user()->title, [1,2, 0]))
                                    @if(!isset($olddata['hideother']))
                                        <div class="input-group-append" style="width:212px; margin-bottom: -10px;">
                                            <button class="btn btn-primary btn-block" onclick="sendData();" type="button">ASSIGN</button>
                                        </div>
                                    @endif
                                @endif

                                <table id="empoyee_default_order" class="table table-striped table-bordered display no-wrap"
                                       style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>SNO</th>
                                        <th class="hideforhr">Lock <br>Status</th>
                                        <th>Employee #</th>
                                        <th>Name</th>
                                        <th>Job Title</th>
                                        <th>Project</th>
                                        <th>Supervisor</th>
                                        <th>Gender</th>
                                        <th>Nationality</th>
                                        <th>Date Of Birth</th>
                                        <th>Basic Salary</th>
                                        <th>Main <br>Allowance</th>
                                        <th>Joining <br>Date</th>
                                        <th>ID <br>No.</th>
                                        <th>ID Expiry <br>Date</th>
                                        <th>Passport <br>Number</th>
                                        <th>Passport <br>Expiry Date</th>
                                        <th>H-Card <br>Expiry Date</th>
                                        <th>Cotract <br>Start Date</th>
                                        <th>Contract <br>Expiry Date</th>
                                        <th>Last Settlement <br>Received Date</th>
                                        <th>Insurance <br>Start Date</th>
                                        <th>Insurance <br>Expiry Date</th>
                                        <th>Status 1</th>
                                        <th>Status 2</th>
                                        <th>Status 3</th>
                                        <th>Status 4</th>
                                        <th>Status 5</th>
                                        <th>Status 6</th>
                                        <th>Created Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $sno = 0; @endphp
                                    @foreach($datas['employs'] as $data)
                                    <tr id="{{$data['id']}}" class="{{$data['islocked'] ? 'locked':'unlocked'}}">
                                        <td>{{++$sno}}</td>
                                        <td class="hideforhr"> <!-- lock status -->
                                            <fieldset class="checkbox">
                                                <label>
                                                    @if(Auth::user()->title == 0)<input name="locked" onchange="lock({{$data['id']}})" {{$data['islocked'] ? 'checked':''}} type="checkbox"> @endif Locked
                                                </label>
                                            </fieldset>
                                        </td>
                                        <td>{{$data['employeeno']}}</td> <!-- employee number -->
                                        <td>{{$data['name']}}</td> <!-- employee name -->
                                        <td>{{$data['title_name']}}</td> <!-- job title -->
                                        <td>{{$data['projectname'] ?? ''}}</td> <!-- project -->
                                        <td>{{$data['supervisor'] ?? ''}}</td> <!-- supervisro -->
                                        <td>{{$data['gender']}}</td> <!-- gender -->
                                        <td>{{$data['nationality']}}</td> <!-- nationality -->
                                        <td>{{isset($data['dob']) ? date("d/m/Y", strtotime($data['dob']) ): ''}}</td> <!-- dob -->
                                        <td>{{$data['bsalary']}}</td> <!-- basic salary -->
                                        <td>{{$data['allownce']}}</td> <!-- main allowance -->
                                        <td>{{isset($data['jdate']) && $data['jdate'] ? date("d/m/Y", strtotime($data['jdate']) ): ''}}</td> <!-- joining date -->
                                        <td>{{$data['idno']}}</td> <!-- id number -->
                                        <td class="{{isset($data['idexpiry']) && $data['idexpiry'] && strtotime($data['idexpiry']) < strtotime(now())-24*3600 ? 'expired': ''}}">{{isset($data['idexpiry']) && $data['idexpiry'] ? date("d/m/Y", strtotime($data['idexpiry']) ): ''}}</td> <!-- id expiry date -->
                                        <td>{{$data['passportno']}}</td> <!-- passport number -->
                                        <td class="{{isset($data['passportexpiry'])  && $data['passportexpiry'] && $data['passportexpiry'] && strtotime($data['passportexpiry']) < strtotime(now())-24*3600 ? 'expired': ''}}">
                                            {{isset($data['passportexpiry'])&& $data['passportexpiry']  && $data['passportexpiry'] ? date("d/m/Y", strtotime($data['passportexpiry']) ) : ''}}
                                        </td> <!-- passport expiry date -->
                                        <td class="{{isset($data['hcardexpiry']) && $data['hcardexpiry']  && strtotime($data['hcardexpiry']) < strtotime(now())-24*3600 ? 'expired': ''}}">{{ isset($data['hcardexpiry']) && $data['hcardexpiry'] ?  date("d/m/Y", strtotime($data['hcardexpiry']) ) : ''}}</td> <!-- h-card expiry date -->
                                        <td>{{isset($data['contractsdate']) && $data['contractsdate'] ? date("d/m/Y", strtotime($data['contractsdate']) ) : ''}}</td> <!-- contract start date -->
                                        <td class="{{isset($data['contractexdate']) && $data['contractexdate'] && strtotime($data['contractexdate']) < strtotime(now())-24*3600 ? 'expired': ''}}">{{isset($data['contractexdate']) && $data['contractexdate'] ? date("d/m/Y", strtotime($data['contractexdate']) ) : ''}}</td> <!-- contract expiry date -->
                                        <td>{{isset($data['last_set_rec_date']) && $data['last_set_rec_date'] ? date("d/m/Y", strtotime($data['last_set_rec_date']) ) :'' }}</td> <!-- last settlement received date -->
                                        <td>{{isset($data['insurancestartdate']) && $data['insurancestartdate'] ? date("d/m/Y", strtotime($data['insurancestartdate']) ) :'' }}</td> <!-- insurance start date -->

                                        <td class="{{isset($data['insuranceexpirydate']) && $data['insuranceexpirydate'] && strtotime($data['insuranceexpirydate']) < strtotime(now())-24*3600 ? 'expired': ''}}">{{isset($data['insuranceexpirydate']) && $data['insuranceexpirydate'] ? date("d/m/Y", strtotime($data['insuranceexpirydate']) ) : ''}}</td><!--  insurance expiry date -->
                                        <!-- statuses 1 -->
                                        <td>{{$data['status1']}}</td>
                                        <td>{{$data['status2']}}</td>
                                        <td>{{$data['status3']}}</td>
                                        <td>{{$data['status4']}}</td>
                                        <td>{{$data['status5']}}</td>
                                        <td>{{$data['status6']}}</td>
                                        <td>{{isset($data['created_at']) ? date("d/m/Y", strtotime($data['created_at']) ): ''}}</td> <!-- created date -->
                                        <td>
                                            @if(!in_array(Auth::user()->title, [1,2]))
                                                @if($data['islocked'] && Auth::user()->title == 6)
                                                    @if(isset($data['uploadfile']) && $data['uploadfile'] !== '')
                                                    <button onclick="location.href ='{{ route('download').'?filename='.$data['uploadfile']}}'" class="btn btn-sm btn-outline-primary download" title="Download file"><i class="fas fa-download"></i>&nbsp;Download</button>
                                                        @endif

                                                    @else
                                            <button onclick="location.href = '{{route('hr.employ.edit', $data['id'])}}';" class="btn btn-sm btn-outline-dark" title="Edit"><i class="fas fa-edit"></i>&nbsp;Edit</button>&emsp;

                                            <button data-toggle="modal" data-target="#delete" onClick="deleteMe({{$data['id']}})" class="btn btn-sm btn-danger" title="Delete"><i class="fas fa-trash"></i>&nbsp;Delete</button> @endif
                                                    @if(isset($data['uploadfile']) && $data['uploadfile'] !== '')
                                                    <button onclick="location.href ='{{ route('download').'?filename='.$data['uploadfile']}}'" class="btn btn-sm btn-outline-primary download" title="Download file"><i class="fas fa-download"></i>&nbsp;Download</button>
                                                    @endif
                                                @endif
                                        </td> <!-- action -->
                                    </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>SNO</th>
                                        <th class="hideforhr">Lock <br>Status</th>
                                        <th>Employee #</th>
                                        <th>Name</th>
                                        <th>Job Title</th>
                                        <th>Project</th>
                                        <th>Supervisor</th>
                                        <th>Gender</th>
                                        <th>Nationality</th>
                                        <th>Date Of Birth</th>
                                        <th>Basic Salary</th>
                                        <th>Main <br>Allowance</th>
                                        <th>Joining <br>Date</th>
                                        <th>ID <br>No.</th>
                                        <th>ID Expiry <br>Date</th>
                                        <th>Passport <br>Number</th>
                                        <th>Passport <br>Expiry Date</th>
                                        <th>H-Card <br>Expiry Date</th>
                                        <th>Cotract <br>Start Date</th>
                                        <th>Contract <br>Expiry Date</th>
                                        <th>Last Settlement <br>Received Date</th>
                                        <th>Insurance <br>Start Date</th>
                                        <th>Insurance <br>Expiry Date</th>
                                        <th>Status 1</th>
                                        <th>Status 2</th>
                                        <th>Status 3</th>
                                        <th>Status 4</th>
                                        <th>Status 5</th>
                                        <th>Status 6</th>
                                        <th>Created Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>

    </div>
    @includeIf('include.deletemodalhr')
    <script>
        document.querySelector("input[name='id_no']").addEventListener("keypress", function (evt) {
            preventData(evt);
        })
        function preventData(evt){
            if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                evt.preventDefault();
            }
        }
        document.getElementById('nationality').value = "{{old('nationality')}}"
        function deleteMe(id){
            $('#idfordelete').val(id);
            $('#action').val('employee');
        }
        function lock(id){
            var action = $('#'+id+' input[name="locked"]').is(':checked');
            $.ajax({
                type: "POST",
                url: "{{route('hr.lock')}}",
                data: {id: id, action: action ? 1 : 0, _token:$('input[name="_token"]').val()},
                cache: false,
                success: function (data) {
                    console.log(action)
                    if(action){
                        $('tr#'+id).addClass('locked');
                       // $('tr#'+id+' td:first-child').html('Locked');
                        $('tr#'+id+' input[name="reference"]').show()
                    }else{
                        $('tr#'+id).removeClass('locked');
                        //$('tr#'+id+' td:first-child').html('Unlocked');
                        $('tr#'+id+' input[name="reference"]').hide()
                    }
                }
            });
        }
        function lockAll(action){
            $.ajax({
                type: "POST",
                url: "{{route('hr.lockall')}}",
                data: { action: action ? 1 : 0, _token:$('input[name="_token"]').val()},
                cache: false,
                success: function (data) {
                    location.reload();
                }
            });
        }
        function send(id){
            $.ajax({
                type: "POST",
                url: "{{route('hr.employ.send')}}",
                data: {id: id, action: $('tr#'+id+' input[name="reference"]').is(':checked') ? 1 : 0, _token:$('input[name="_token"]').val()},
                cache: false,
                success: function (data) {

                }
            });
        }
        function sendData(){
            if($('.unlocked').length  >0 ){
                //$('#alert label').html('Records must be locked before sending.')
                //$('#alert').show()
                successAlert('Records must be locked before sending.', false)
                return false;
            }
            var id = [];
            $("table > tbody > tr").each(function () {
                id.push($(this).attr('id'))
            });
            id = JSON.stringify(id)
            $.ajax({
                type: "POST",
                url: "{{route('hr.employ.sendall')}}",
                data: {id: id, _token:$('input[name="_token"]').val()},
                cache: false,
                success: function (data) {
                    //$('#alert label').html(data+' records has been send.')
                    successAlert(data+' records has been send.', false)
                    //$('#alert').show()
                }
            });
        }
    </script>
    <style>
        tr.locked{
            color: #cd0a0a;
        }
        .dt-button-collection .dropdown-menu{
            display: block  !important;
        }
        .dt-buttons{
            margin-top: 17px !important;
        }
        .expired{
            background: #fbfb4c;
        }
        .download:nth-child(2){
            display: none;
        }
    </style>
    @if(Auth()->user()->title == 6)
    <style>
        .hideforhr{
            display: none;
        }
    </style>
    @endif
@stop
