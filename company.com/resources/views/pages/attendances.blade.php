@extends('index')

@section('content')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"><a href="{{route('dashboard')}}" class="custom_a"><i class="fas fa-arrow-left"></i> </a>&nbsp;Attendance Sheet</h3>
                    <div class="d-flex align-items-center">
                    </div>
                </div>

            </div>
            <!--
            <div class="row mt-5">
                <div class="col-sm-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Supervisor</h4>
                            <form action="projects.html" class="mt-5">
                                <div class="form-body">

                                   <div class="row">
                                           <div class="col-md-2">
                                           <label class="text-center text-dark text-right">Name</label>
                                           </div>

                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="supervisor_name"
                                                    placeholder="Enter supervisor name..">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions mt-3">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-maroon">Save</button>
                                        <button type="reset" class="btn btn-dark">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> -->
            @if(Auth::user()->title == 6 || Auth::user()->title == 0 )
            <div class="row mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="card-title">Search By</h4>
                            <form action="{{route('hrattendance')}}" method="GET">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Projects</label>
                                            <select onchange="getSupervisor()" class="custom-select form-control" name="projectid">
                                                <option value="" selected="selected">Choose</option>
                                                @foreach($datas['projects'] as $data)
                                                    <option {{isset($searches['projectid']) && $searches['projectid'] == $data['id']? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Supervisor</label>
                                            <input value="{{$searches['supervisorname'] ?? ''}}" name="supervisorname" readonly class="custom-select form-control" id="supervisor" />
                                        </div>
                                    </div>

                                    {{--<div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">Supervisor</label>
                                            <select class="custom-select form-control" name="supervisorid">
                                                <option value="" selected="selected">Choose</option>
                                                @foreach($datas['supervisors'] as $data)
                                                    <option {{isset($searches['supervisorid']) && $searches['supervisorid'] == $data['id']? 'selected':''}} value="{{$data['id']}}">{{$data['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>--}}


                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">From</label>
                                            <input  value="{{isset($searches['from']) && $searches['from'] ? $searches['from'] : ''}}" name="from" type="date" class="form-control datepicker">
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right">To</label>
                                            <input value="{{isset($searches['to']) && $searches['to'] ?$searches['to']: ''}}" name="to" type="date" class="form-control datepicker">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right" style="visibility: hidden"> Search</label>
                                            <input type="submit" value="Search" class="btn btn-block btn-primary">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-center text-dark text-right" style="visibility: hidden"> Reset</label>
                                            <input type="reset" onclick="resetPage('{{route('hrattendance')}}')" value="Reset" class="btn btn-block btn-dark">
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>

            </div>
            @endif
            <div class="row mt-5">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Current Attendance</h4>
                            <form method="post" action="{{route('saveattendance')}}">
                                @csrf
                            <div class="table-responsive mt-5">
                                <table id="attendance_default" class="table table-striped table-bordered display no-wrap"
                                       style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee #</th>
                                        <th>Name</th>
                                        <th>Job Title</th>
                                        <th>Project</th>

                                        <th>ID No</th>
                                        <th>Passport No</th>
                                        <th class="hidesupervisor">Status 1</th>
                                        <th class="hidesupervisor">Status 2</th>
                                        <th class="hidesupervisor">Status 3</th>
                                        <th class="hidesupervisor">Status 4</th>
                                        <th class="hidesupervisor">Status 5</th>
                                        <th class="hidesupervisor">Status 6</th>
                                        <!-- days of the month -->
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <th>4</th>
                                        <th>5</th>

                                        <th>6</th>
                                        <th>7</th>
                                        <th>8</th>
                                        <th>9</th>
                                        <th>10</th>

                                        <th>11</th>
                                        <th>12</th>
                                        <th>13</th>
                                        <th>14</th>
                                        <th>15</th>

                                        <th>16</th>
                                        <th>17</th>
                                        <th>18</th>
                                        <th>19</th>
                                        <th>20</th>

                                        <th>21</th>
                                        <th>22</th>
                                        <th>23</th>
                                        <th>24</th>
                                        <th>25</th>

                                        <th>26</th>
                                        <th>27</th>
                                        <th>28</th>
                                        <th>29</th>
                                        <th>30</th>
                                        <th>31</th>
                                        <!-- end - days of the month -->

                                        <th>Added Date</th>
                                        <th>A. Date</th>
                                        <th>Total Days</th>
                                        <th>Over Time</th>
                                        <th>Notes</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $attendanceLables = ['', 'Present','Absent', 'Sick', 'Off', 'Overtime', 'Holiday'];
                                        @endphp
                                    @foreach($datas['data'] as $key => $data2)
                                        @if(isset($data2['attendace']))
                                        @foreach($data2['attendace'] as $data)
                                    <tr id="{{$data2['id']}}" class="row{{$key}}">
                                        @php $sno = $key+1 @endphp
                                        <td>{{$sno}}</td> <!-- sno-->
                                        <td><input type="hidden" value="{{$data2['id']}}" name="employeeid[]">{{$data2['employeeno']}}</td>  <!-- id-->
                                        <td>{{$data2['name']}}</td>  <!-- empname-->
                                        <td>{{$data2['jobtitlename']}}</td>  <!-- job title-->
                                        <td>{{$data2['projectname']}}</td>  <!-- prject name-->
                                        <td>{{$data2['idno']}}</td>
                                        <td>{{$data2['passportno'] ?? ''}}</td>

                                        <td class="hidesupervisor">{{$data2['status1']}}</td>
                                        <td class="hidesupervisor">{{$data2['status2']}}</td>
                                        <td class="hidesupervisor">{{$data2['status3']}}</td>
                                        <td class="hidesupervisor">{{$data2['status4']}}</td>
                                        <td class="hidesupervisor">{{$data2['status5']}}</td>
                                        <td class="hidesupervisor">{{$data2['status6']}}</td>

                                        <!-- attendance day -->
                                        @for($index = 0; $index<31; $index++)
                                        @php
                                            $value = $data['attendance'][$index] == 5? $attendanceLables[$data['attendance'][$index]].' '.$data['extratime'][$index] : $attendanceLables[$data['attendance'][$index]];
                                        @endphp
                                        <td class="{{$index}}" onclick="attendanceModal('attendance', {{$data2['id']}}, {{$index}})">
                                            <label>{{$value}}</label>
                                            <input class="form-control d-none" name="attendacne{{$key}}[]" value="{{$data['attendance'][$index]}}"/>
                                            <input class="form-control d-none" name="extratime{{$key}}[]" value="{{$data['extratime'][$index] ?? ''}}"/>
                                        </td>
                                        @endfor
                                        {{--<td>
                                            <select onchange="handleExtraTime('{{$key}}', 1)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][1]) && $data['attendance'][1] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][1]) && $data['attendance'][1] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][1]) && $data['attendance'][1] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][1]) && $data['attendance'][1] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][1]) && $data['attendance'][1] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][1]) && $data['attendance'][1] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][1]) && $data['attendance'][1] == 5 ? 'show':'hide'}} extratime-{{$key}}-1" name="extratime{{$key}}[]" value="{{$data['extratime'][1] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 2)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][2]) && $data['attendance'][2] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][2]) && $data['attendance'][2] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][2]) && $data['attendance'][2] == 3? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][2]) && $data['attendance'][2] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][2]) && $data['attendance'][2] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][2]) && $data['attendance'][2] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][2]) && $data['attendance'][2] == 5 ? 'show':'hide'}} extratime-{{$key}}-2" name="extratime{{$key}}[]" value="{{$data['extratime'][2] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 3)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][3]) && $data['attendance'][3] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][3]) && $data['attendance'][3] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][3]) && $data['attendance'][3] ==3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][3]) && $data['attendance'][3] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][3]) && $data['attendance'][3] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][3]) && $data['attendance'][3] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][3]) && $data['attendance'][3] == 5 ? 'show':'hide'}} extratime-{{$key}}-3" name="extratime{{$key}}[]" value="{{$data['extratime'][3] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 4)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][4]) && $data['attendance'][4] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][4]) && $data['attendance'][4] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][4]) && $data['attendance'][4] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][4]) && $data['attendance'][4] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][4]) && $data['attendance'][4] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][4]) && $data['attendance'][4] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][4]) && $data['attendance'][4] == 5 ? 'show':'hide'}} extratime-{{$key}}-4" name="extratime{{$key}}[]" value="{{$data['extratime'][4] ?? ''}}"/>
                                        </td>

                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 5)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][5]) && $data['attendance'][5] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][5]) && $data['attendance'][5] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][5]) && $data['attendance'][5] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][5]) && $data['attendance'][5] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][5]) && $data['attendance'][5] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][5]) && $data['attendance'][5] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][5]) && $data['attendance'][5] == 5 ? 'show':'hide'}} extratime-{{$key}}-5" name="extratime{{$key}}[]" value="{{$data['extratime'][5] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 6)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][6]) && $data['attendance'][6] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][6]) && $data['attendance'][6] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][6]) && $data['attendance'][6] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][6]) && $data['attendance'][6] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][6]) && $data['attendance'][6] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][6]) && $data['attendance'][6] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][6]) && $data['attendance'][6] == 5 ? 'show':'hide'}} extratime-{{$key}}-6" name="extratime{{$key}}[]" value="{{$data['extratime'][6] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 7)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][7]) && $data['attendance'][7] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][7]) && $data['attendance'][7] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][7]) && $data['attendance'][7] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][7]) && $data['attendance'][7] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][7]) && $data['attendance'][7] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][7]) && $data['attendance'][7] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][7]) && $data['attendance'][7] == 5 ? 'show':'hide'}} extratime-{{$key}}-7" name="extratime{{$key}}[]" value="{{$data['extratime'][7] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 8)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][8]) && $data['attendance'][8] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][8]) && $data['attendance'][8] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][8]) && $data['attendance'][8] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][8]) && $data['attendance'][8] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][8]) && $data['attendance'][8] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][8]) && $data['attendance'][8] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][8]) && $data['attendance'][8] == 5 ? 'show':'hide'}} extratime-{{$key}}-8" name="extratime{{$key}}[]" value="{{$data['extratime'][8] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 9)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][9]) && $data['attendance'][9] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][9]) && $data['attendance'][9] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][9]) && $data['attendance'][9] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][9]) && $data['attendance'][9] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][9]) && $data['attendance'][9] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][9]) && $data['attendance'][9] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][9]) && $data['attendance'][9] == 5 ? 'show':'hide'}} extratime-{{$key}}-9" name="extratime{{$key}}[]" value="{{$data['extratime'][9] ?? ''}}"/>
                                        </td>

                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 10)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][10]) && $data['attendance'][10] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][10]) && $data['attendance'][10] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][10]) && $data['attendance'][10] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][10]) && $data['attendance'][10] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][10]) && $data['attendance'][10] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][10]) && $data['attendance'][10] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][10]) && $data['attendance'][10] == 5 ? 'show':'hide'}} extratime-{{$key}}-10" name="extratime{{$key}}[]" value="{{$data['extratime'][10] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 11)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][11]) && $data['attendance'][11] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][11]) && $data['attendance'][11] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][11]) && $data['attendance'][11] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][11]) && $data['attendance'][11] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][11]) && $data['attendance'][11] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][11]) && $data['attendance'][11] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][11]) && $data['attendance'][11] == 5 ? 'show':'hide'}} extratime-{{$key}}-11" name="extratime{{$key}}[]" value="{{$data['extratime'][11] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 12)"  class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][12]) && $data['attendance'][12] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][12]) && $data['attendance'][12] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][12]) && $data['attendance'][12] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][12]) && $data['attendance'][12] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][12]) && $data['attendance'][12] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][12]) && $data['attendance'][12] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][12]) && $data['attendance'][12] == 5 ? 'show':'hide'}} extratime-{{$key}}-12" name="extratime{{$key}}[]" value="{{$data['extratime'][12] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 13)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][13]) && $data['attendance'][13] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][13]) && $data['attendance'][13] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][13]) && $data['attendance'][13] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][13]) && $data['attendance'][13] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][13]) && $data['attendance'][13] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][13]) && $data['attendance'][13] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][13]) && $data['attendance'][13] == 5 ? 'show':'hide'}} extratime-{{$key}}-13" name="extratime{{$key}}[]" value="{{$data['extratime'][13] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 14)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][14]) && $data['attendance'][14] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][14]) && $data['attendance'][14] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][14]) && $data['attendance'][14] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][14]) && $data['attendance'][14] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][14]) && $data['attendance'][14] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][14]) && $data['attendance'][14] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][14]) && $data['attendance'][14] == 5 ? 'show':'hide'}} extratime-{{$key}}-14" name="extratime{{$key}}[]" value="{{$data['extratime'][14] ?? ''}}"/>
                                        </td>

                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 15)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][15]) && $data['attendance'][15] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][15]) && $data['attendance'][15] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][15]) && $data['attendance'][15] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][15]) && $data['attendance'][15] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][15]) && $data['attendance'][15] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][15]) && $data['attendance'][15] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][15]) && $data['attendance'][15] == 5 ? 'show':'hide'}} extratime-{{$key}}-15" name="extratime{{$key}}[]" value="{{$data['extratime'][15] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 16)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][16]) && $data['attendance'][16] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][16]) && $data['attendance'][16] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][16]) && $data['attendance'][16] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][16]) && $data['attendance'][16] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][16]) && $data['attendance'][16] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][16]) && $data['attendance'][16] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][16]) && $data['attendance'][16] == 5 ? 'show':'hide'}} extratime-{{$key}}-16" name="extratime{{$key}}[]" value="{{$data['extratime'][16] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 17)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][17]) && $data['attendance'][17] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][17]) && $data['attendance'][17] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][17]) && $data['attendance'][17] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][17]) && $data['attendance'][17] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][17]) && $data['attendance'][17] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][17]) && $data['attendance'][17] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][17]) && $data['attendance'][17] == 5 ? 'show':'hide'}} extratime-{{$key}}-17" name="extratime{{$key}}[]" value="{{$data['extratime'][17] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 18)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][18]) && $data['attendance'][18] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][18]) && $data['attendance'][18] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][18]) && $data['attendance'][18] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][18]) && $data['attendance'][18] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][18]) && $data['attendance'][18] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][18]) && $data['attendance'][18] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][18]) && $data['attendance'][18] == 5 ? 'show':'hide'}} extratime-{{$key}}-18" name="extratime{{$key}}[]" value="{{$data['extratime'][18] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 19)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][19]) && $data['attendance'][19] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][19]) && $data['attendance'][19] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][19]) && $data['attendance'][19] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][19]) && $data['attendance'][19] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][19]) && $data['attendance'][19] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][19]) && $data['attendance'][19] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][19]) && $data['attendance'][19] == 5 ? 'show':'hide'}} extratime-{{$key}}-19" name="extratime{{$key}}[]" value="{{$data['extratime'][19] ?? ''}}"/>
                                        </td>

                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 20)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][20]) && $data['attendance'][20] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][20]) && $data['attendance'][20] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][20]) && $data['attendance'][20] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][20]) && $data['attendance'][20] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][20]) && $data['attendance'][20] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][20]) && $data['attendance'][20] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][20]) && $data['attendance'][20] == 5 ? 'show':'hide'}} extratime-{{$key}}-20" name="extratime{{$key}}[]" value="{{$data['extratime'][20] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 21)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][21]) && $data['attendance'][21] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][21]) && $data['attendance'][21] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][21]) && $data['attendance'][21] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][21]) && $data['attendance'][21] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][21]) && $data['attendance'][21] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][21]) && $data['attendance'][21] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][21]) && $data['attendance'][21] == 5 ? 'show':'hide'}} extratime-{{$key}}-21" name="extratime{{$key}}[]" value="{{$data['extratime'][21] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 22)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][22]) && $data['attendance'][22] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][22]) && $data['attendance'][22] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][22]) && $data['attendance'][22] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][22]) && $data['attendance'][22] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][22]) && $data['attendance'][22] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][22]) && $data['attendance'][22] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][22]) && $data['attendance'][22] == 5 ? 'show':'hide'}} extratime-{{$key}}-22" name="extratime{{$key}}[]" value="{{$data['extratime'][22] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 23)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][23]) && $data['attendance'][23] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][23]) && $data['attendance'][23] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][23]) && $data['attendance'][23] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][23]) && $data['attendance'][23] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][23]) && $data['attendance'][23] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][23]) && $data['attendance'][23] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][23]) && $data['attendance'][23] == 5 ? 'show':'hide'}} extratime-{{$key}}-23" name="extratime{{$key}}[]" value="{{$data['extratime'][23] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 24)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][24]) && $data['attendance'][24] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][24]) && $data['attendance'][24] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][24]) && $data['attendance'][24] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][24]) && $data['attendance'][24] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][24]) && $data['attendance'][24] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][24]) && $data['attendance'][24] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][24]) && $data['attendance'][24] == 5 ? 'show':'hide'}} extratime-{{$key}}-24" name="extratime{{$key}}[]" value="{{$data['extratime'][24] ?? ''}}"/>
                                        </td>

                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 25)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][25]) && $data['attendance'][25] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][25]) && $data['attendance'][25] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][25]) && $data['attendance'][25] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][25]) && $data['attendance'][25] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][25]) && $data['attendance'][25] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][25]) && $data['attendance'][25] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][25]) && $data['attendance'][25] == 5 ? 'show':'hide'}} extratime-{{$key}}-25" name="extratime{{$key}}[]" value="{{$data['extratime'][25] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 26)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][26]) && $data['attendance'][26] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][26]) && $data['attendance'][26] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][26]) && $data['attendance'][26] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][26]) && $data['attendance'][26] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][26]) && $data['attendance'][26] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][26]) && $data['attendance'][26] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][26]) && $data['attendance'][26] == 5 ? 'show':'hide'}} extratime-{{$key}}-26" name="extratime{{$key}}[]" value="{{$data['extratime'][26] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 27)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][27]) && $data['attendance'][27] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][27]) && $data['attendance'][27] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][27]) && $data['attendance'][27] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][27]) && $data['attendance'][27] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][27]) && $data['attendance'][27] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][27]) && $data['attendance'][27] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][27]) && $data['attendance'][27] == 5 ? 'show':'hide'}} extratime-{{$key}}-27" name="extratime{{$key}}[]" value="{{$data['extratime'][27] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 28)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][28]) && $data['attendance'][28] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][28]) && $data['attendance'][28] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][28]) && $data['attendance'][28] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][28]) && $data['attendance'][28] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][28]) && $data['attendance'][28] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][28]) && $data['attendance'][28] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][28]) && $data['attendance'][28] == 5 ? 'show':'hide'}} extratime-{{$key}}-28" name="extratime{{$key}}[]" value="{{$data['extratime'][28] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 29)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][29]) && $data['attendance'][29] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][29]) && $data['attendance'][29] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][29]) && $data['attendance'][29] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][29]) && $data['attendance'][29] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][29]) && $data['attendance'][29] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][29]) && $data['attendance'][29] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][29]) && $data['attendance'][29] == 5 ? 'show':'hide'}} extratime-{{$key}}-29" name="extratime{{$key}}[]" value="{{$data['extratime'][29] ?? ''}}"/>
                                        </td>
                                        <td>
                                            <select onchange="handleExtraTime('{{$key}}', 30)" class="custom-select form-control" name="attendacne{{$key}}[]">
                                                <option   value="0" selected="selected">Choose</option>
                                                <option {{isset($data['attendance'][30]) && $data['attendance'][30] == 1 ? 'selected':'' }} value="1">Present</option>
                                                <option {{isset($data['attendance'][30]) && $data['attendance'][30] == 2 ? 'selected':'' }} value="2">Absent</option>
                                                <option {{isset($data['attendance'][30]) && $data['attendance'][30] == 3 ? 'selected':'' }} value="3">Sick</option>
                                                <option {{isset($data['attendance'][30]) && $data['attendance'][30] == 4 ? 'selected':'' }} value="4">Off</option><option {{isset($data['attendance'][30]) && $data['attendance'][30] == 6 ? 'selected':'' }} value="6">Holiday</option>
                                                <option {{isset($data['attendance'][30]) && $data['attendance'][30] == 5 ? 'selected':'' }} value="5">Overtime</option>
                                            </select>
                                            <input class="form-control {{isset($data['attendance'][30]) && $data['attendance'][30] == 5 ? 'show':'hide'}} extratime-{{$key}}-30" name="extratime{{$key}}[]" value="{{$data['extratime'][30] ?? ''}}"/>
                                        </td>--}}
                                        <!-- end attendance day -->

                                        <td>{{isset($data['created_at']) && $data['created_at'] != "" ? date("d/m/Y", strtotime($data['created_at'])): date('d/m/Y')}}</td>
                                        <td>{{isset($data['created_at']) && $data['created_at'] != "" ? date("m/Y", strtotime($data['created_at'])): date('m/Y')}}</td>
                                        <td>{{$data['present'] ?? 0}}</td>
                                        <td>{{$data['overtime'] ?? 0}}</td>
                                        <td onclick="attendanceModal('notes',{{$data2['id']}})" ><label class="notes" >{{$data['note'] ?? ''}}</label><input type="hidden" class="notesvalue" name="note[]" value="{{$data['note'] ?? ''}}"></td>
                                    </tr>
                                        @endforeach
                                        @endif
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee #</th>
                                        <th>Name</th>
                                        <th>Job Title</th>
                                        <th>Project</th>

                                        <th>ID No</th>
                                        <th>Passport No</th>
                                        <th class="hidesupervisor">Status 1</th>
                                        <th class="hidesupervisor">Status 2</th>
                                        <th class="hidesupervisor">Status 3</th>
                                        <th class="hidesupervisor">Status 4</th>
                                        <th class="hidesupervisor">Status 5</th>
                                        <th class="hidesupervisor">Status 6</th>
                                        <!-- days of the month -->
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <th>4</th>
                                        <th>5</th>

                                        <th>6</th>
                                        <th>7</th>
                                        <th>8</th>
                                        <th>9</th>
                                        <th>10</th>

                                        <th>11</th>
                                        <th>12</th>
                                        <th>13</th>
                                        <th>14</th>
                                        <th>15</th>

                                        <th>16</th>
                                        <th>17</th>
                                        <th>18</th>
                                        <th>19</th>
                                        <th>20</th>

                                        <th>21</th>
                                        <th>22</th>
                                        <th>23</th>
                                        <th>24</th>
                                        <th>25</th>

                                        <th>26</th>
                                        <th>27</th>
                                        <th>28</th>
                                        <th>29</th>
                                        <th>30</th>
                                        <th>31</th>
                                        <!-- end - days of the month -->

                                        <th>Added Date</th>
                                        <th>A. Date</th>
                                        <th>Total Days</th>
                                        <th>Over Time</th>
                                        <th>Notes</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                                @if(!in_array(Auth::user()->title, [1,2, 6]))
                            <div class="row mt-5">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-block btn-success"><i class="fas fa-save"></i>&emsp;Save</button>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                                    @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        </div>
    </div>
    <style>
        .dt-button-collection .dropdown-menu{
            display: block  !important;
        }
        #attendance_default select{
            width: 111px;
        }
        input.hide{
            display: none;
        }
        input.show{
            display: block;
        }
    </style>
    @if(Auth::user()->title == 3)
    <style>.hidesupervisor{display: none;}</style>
    @endif
    <script>
        function getSupervisor(){
            var projectid = $('select[name="projectid"] option:selected').val();

            $.ajax({
                type: "GET",
                url: "{{route('getcustomernamebyprojectid')}}",
                data: {id: projectid},
                cache: false,
                success: function (data) {
                    $('#supervisor').val(data)
                }
            });
        }
        function handleExtraTime(row, col){
            console.log(row, col);
            var name = 'attendacne'+row+'[]';
            //var show = $('select[name="'+name+'"]:nth-child('+col+') option:selected').val()
            //var show = $('.row'+row+' .custom-select:nth-child('+col+') option:selected').val();
            var show = $('.row'+row+' .custom-select option:selected:eq('+col+')').val();
            console.log(show, name);
            if(show == 5){
                $('.extratime-'+row+'-'+col).removeClass('hide').addClass('show');
            }else{
                $('.extratime-'+row+'-'+col).removeClass('show').addClass('hide');
            }
        }
        var gtype = '';
        var gid = '';
        var gnum = '';
        function attendanceModal(type, id, number=-1){
            gtype = type;
            gid = id;
            gnum = number;
            if( type == 'notes' ){
                $('#attendancemodel input').val($('#'+id + ' .notes').html());
                $('#attendancemodel').modal('show');
            }else{
                $('#attendancemodelDropdown').modal('show');
                var attendance = $('#'+id+' .'+number +' input').eq(0).val();
                var overtime = $('#'+id+' .'+number +' input').eq(1).val();
                console.log(attendance, overtime)
                $("#attendancemodelDropdown option[value="+attendance+"]").attr("selected", "selected");
                $('#attendancemodelDropdown input').val(overtime);
                if(attendance == 5){
                    $('#attendancemodelDropdown input').show()
                }else{
                    $('#attendancemodelDropdown input').hide()
                }
            }
        }
        function saveAttendaceModal(){
            console.log(gtype, gid)
            if( gtype == 'notes' ){
                $('#'+gid + ' .notes').html($('#attendancemodel input').val())
                $('#'+gid + ' .notesvalue').val($('#attendancemodel input').val())
                $('#attendancemodel').modal('hide');
            }else{
                var attendance = $('#attendancemodelDropdown select option:selected').val();
                var attendanceText = $('#attendancemodelDropdown select option:selected').text();
                var overtime = $('#attendancemodelDropdown input').val()
                if(attendance == 5){
                    attendanceText = attendanceText+' '+overtime;
                }
                $('#'+gid+' .'+gnum +' label').html(attendanceText);
                $('#'+gid+' .'+gnum +' input').eq(0).val(attendance);
                $('#'+gid+' .'+gnum +' input').eq(1).val(overtime);
                $('#attendancemodelDropdown').modal('hide')
            }
        }
        function changeAttendance(){
            var selectedOptionText = $('#attendancemodelDropdown select option:selected').text();
            var selectedOptionValue = $('#attendancemodelDropdown select option:selected').val();
            if(selectedOptionValue == 5){
                $('#attendancemodelDropdown input').show();
            }else{
                $('#attendancemodelDropdown input').hide();
            }
        }
    </script>
    @if(Auth()->user()->title !== 6)
    <div id="attendancemodel" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input value="" class="form-control" />
                </div>
                <input type="hidden" id="idfordelete" />
                <input type="hidden" id="action" />
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="saveAttendaceModal()">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div id="attendancemodelDropdown" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <select onchange="changeAttendance()" class="custom-select form-control">
                        <option value="0" selected="selected">Choose</option>
                        <option value="1">Present</option>
                        <option value="2">Absent</option>
                        <option value="3">Sick</option>
                        <option value="4">Off</option>
                        <option value="5">Overtime</option>
                        <option value="6">Holiday</option>
                    </select>
                    <input value="" class="form-control mt-3" />
                </div>
                <input type="hidden" id="idfordelete" />
                <input type="hidden" id="action" />
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="saveAttendaceModal()">Save</button>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if ($message = Session::get('success'))
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            $('document').ready(function (){
                showModal("{{ $message }}")
            })
        </script>
    @endif
@stop
