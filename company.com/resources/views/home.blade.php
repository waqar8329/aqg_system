
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><title>@yield('title','AQG | Abraj Qatar Group')</title>

    @includeIf('include.styles')
</head>

<body>
<div>
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper" style="display: contents;">

        <div class="page-breadcrumb" >

        </div>
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- *************************************************************** -->
            <!-- Start First Cards -->
            <!-- *************************************************************** -->


            <!-- *************************************************************** -->
            <!-- End First Cards -->
            <!-- *************************************************************** -->
            <!-- *************************************************************** -->
            <!-- Start Latest Games Section -->
            <!-- *************************************************************** -->
            <div class="row ">
                <!-- stats start -->
                <div class="col-12">
                    {{--<div class="col-3 text-center" style="max-width: 20%;">

                        <div class="customize-input">
                            <h4 class=" custom-select-set form-control bg-white border-0 custom-shadow custom-radius">
                                {{date('d/m/Y @ H:i')}}
                            </h4>

                        </div>
                    </div>--}}




                    <div class="jumbotron jumbotron-fluid" style="background-color: inherit;">
                        <div class="text-center">
                            <h1 class="display-3 text-dark">WELCOME TO ABRAJ QATAR GROUP</h1>
                        </div>
                    </div>





                </div>
                <!-- stats end -->
            </div>

            <br>
            <!-- Row -->
            <div class="row">
                <div class="col-12">
                    <!-- Row -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-2 col-md-6">
                            <!-- Card -->
                            <div class="card no-shadow  text-center">
                                <a href="{{route('login')}}"><img src="{{asset('assets/dist/imgs/abraj.jpg')}}" alt="image" class="card-img-top img-fluid rounded-circle img-shadow"
                                                              width="200"></a>
                                <div class="card-body">
                                    <a href="{{route('login')}}"><h3 class="card-title text-maroon">A.Q.G</h3></a>
                                    <!--<a href="javascript:void(0)" class="btn btn-rounded btn-maroon mt-2">Visit&emsp;<i class="fas fa-angle-right"></i></a>-->
                                </div>
                            </div>
                            <!-- Card -->
                        </div>
                        <!-- column -->
                        <!-- column -->
                        <div class="col-lg-2 col-md-6">
                            <!-- Card -->
                            <div class="card no-shadow  text-center">
                                <a href="chem_dry_login.html"><img src="{{asset('assets/dist/imgs/chem.jpg')}}" alt="image" class="card-img-top img-fluid rounded-circle img-shadow"
                                                                   width="200"></a>
                                <div class="card-body">
                                    <a href="chem_dry_login.html"><h3 class="card-title">CHEM-DRY QATAR</h3></a>
                                    <!--<a href="javascript:void(0)" class="btn btn-rounded btn-maroon mt-2">Visit&emsp;<i class="fas fa-angle-right"></i></a>-->
                                </div>
                            </div>
                            <!-- Card -->
                        </div>
                        <!-- column -->
                        <!-- column -->
                        <div class="col-lg-2 col-md-6">
                            <!-- Card -->
                            <div class="card no-shadow text-center">
                                <a href="royal_valet_login.html"><img src="{{asset('assets/dist/imgs/royal.jpg')}}" alt="image" class="card-img-top img-fluid rounded-circle img-shadow"
                                                                      width="200"></a>
                                <div class="card-body">
                                    <a href="royal_valet_login.html"><h3 class="card-title">ROYAL VALET</h3></a>
                                    <!--<a href="javascript:void(0)" class="btn btn-rounded btn-maroon mt-2">Visit&emsp;<i class="fas fa-angle-right"></i></a>-->
                                </div>
                            </div>
                            <!-- Card -->
                        </div>
                        <!-- column -->
                        <!-- column -->
                        <div class="col-lg-2 col-md-6 img-fluid">
                            <!-- Card -->
                            <div class="card no-shadow text-center">
                                <a href="spider_cleaning_login.html"><img src="{{asset('assets/dist/imgs/spider.jpg')}}" alt="image" class="card-img-top img-fluid rounded-circle img-shadow"
                                                                          width="200"></a>
                                <div class="card-body">
                                    <a href="spider_cleaning_login.html"><h3 class="card-title text-spider">SPIDER CLEANING</h3></a>
                                    <!--<a href="javascript:void(0)" class="btn btn-rounded btn-maroon mt-2">Visit&emsp;<i class="fas fa-angle-right"></i></a>-->
                                </div>
                            </div>
                            <!-- Card -->
                        </div>
                        <!-- column -->
                        <div class="col-lg-2">
                        </div>
                    </div>
                    <!-- Row -->
                </div>
            </div>
            <!-- End Row -->
            <!-- *************************************************************** -->
            <!-- End Latest Games Section -->
            <!-- *************************************************************** -->
            <!-- *************************************************************** -->
            <!-- Start Popular/Categories Games Section -->
            <!-- *************************************************************** -->

            <!-- *************************************************************** -->
            <!-- End Popular/Categories Games Section -->
            <!-- *************************************************************** -->
            <!-- *************************************************************** -->
            <!-- Start Similar Games Section -->
            <!-- *************************************************************** -->

            <!-- *************************************************************** -->
            <!-- End Similar Games Section -->
            <!-- *************************************************************** -->


        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
    @includeIf('include.footer')
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>

</body>
@includeIf('include.scripts')
</html>
