<aside class="left-sidebar" data-sidebarbg="skin6" style="display: none;">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar ps-container ps-theme-default ps-active-y" data-sidebarbg="skin6" data-ps-id="9d4ff019-01a1-c9ad-cfe5-1e25c024e230">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="in">
                @if(in_array(Auth::user()->title, [0,5]) && Auth::user()->title != 6)
                <li class="sidebar-item selected"> <a class="sidebar-link text-dark active" href="{{route('dashboard')}}" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home feather-icon"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg><span class="hide-menu">Home</span></a></li>
                @endif
                    @if(Auth::user()->title != 6 &&  Auth::user()->title != 3 )
                <li class="list-divider"></li>

                <!-- users drop down -->
                    @if(Auth::user()->title != 5)
                            @if(!in_array(Auth::user()->title, [1,2]))
                <li class="sidebar-item"> <a class="sidebar-link text-dark" href="{{route('getUsers')}}" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users feather-icon"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg><span class="hide-menu">Users
                                </span></a>
                </li>
                            @endif
                        @endif

                <!-- backup drop down -->


                <!-- marketing drop down -->
                <li class="sidebar-item"> <a class="sidebar-link has-arrow text-dark" href="javascript:void(0)" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe feather-icon"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg><span class="hide-menu">Marketing</span></a>
                    <ul aria-expanded="false" class="collapse first-level base-level-line">
                        @if(!in_array(Auth::user()->title, [1,2]))
                        <li class="sidebar-item"><a href="{{route('marketing.customers')}}" class="sidebar-link text-dark"><span class="hide-menu">Customers</span></a>
                        </li>
                        <li class="sidebar-item"><a href="{{route('marketing.category')}}" class="sidebar-link text-dark"><span class="hide-menu">Categories</span></a>
                        </li>
                        <li class="sidebar-item"><a href="{{route('marketing.projects')}}" class="sidebar-link text-dark"><span class="hide-menu">Projects</span></a>
                        </li>
                        <li class="sidebar-item"><a href="{{route('marketing.services')}}" class="sidebar-link text-dark"><span class="hide-menu">Services</span></a>
                        </li>
                        <li class="sidebar-item"><a href="{{route('marketing.locations')}}" class="sidebar-link text-dark"><span class="hide-menu">Locations</span></a>
                        </li>
                        {{--<li class="sidebar-item"><a href="{{route('marketing.jobtitles')}}" class="sidebar-link text-dark"><span class="hide-menu">Job Titles</span></a>
                        </li>--}}
                        <!--<li class="sidebar-item"><a href="supervisors.html" class="sidebar-link text-dark"><span
                                    class="hide-menu">Supervisors</span></a>-->



                                <li class="sidebar-item"><a href="{{route('marketing.paymentmethods')}}" class="sidebar-link"><span class="hide-menu">Payment Methods</span></a>
                                </li>

                                <li class="sidebar-item"><a href="{{route('marketing.addpayments')}}" class="sidebar-link"><span class="hide-menu">Add Payment</span></a>
                                </li>
                                @endif
                                <li class="sidebar-item"><a href="{{route('marketing.payments')}}" class="sidebar-link"><span class="hide-menu">View Payments</span></a>
                                </li>

                    </ul>
                </li>
                @endif
                <!-- humarn resources drop down -->
                @if(Auth::user()->title != 5 && Auth::user()->title != 3)
                <li class="sidebar-item"> <a class="sidebar-link has-arrow text-dark" href="javascript:void(0)" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-check feather-icon"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><polyline points="17 11 19 13 23 9"></polyline></svg><span class="hide-menu">Human Resources</span></a>
                    <ul id="humanresource" aria-expanded="false" class="collapse first-level base-level-line">
                        @if(!in_array(Auth::user()->title, [1,2]))
                        <li class="sidebar-item"><a href="{{route('hr.jobtitles')}}" class="sidebar-link text-dark"><span class="hide-menu">Job Titles</span></a>
                        </li>
                        <li class="sidebar-item"><a href="{{route('hr.projects')}}" class="sidebar-link text-dark"><span class="hide-menu">Projects</span></a>
                        </li>
                        <li class="sidebar-item"><a href="{{route('hr.status')}}" class="sidebar-link text-dark"><span class="hide-menu">Status</span></a>
                        </li>


                        <li class="sidebar-item"><a href="{{route('hr.addemploy')}}" class="sidebar-link"><span class="hide-menu">Add Employee</span></a>
                        </li>
                        @endif
                        <li class="sidebar-item"><a href="{{route('hr.employ')}}" class="sidebar-link"><span class="hide-menu">View Employees</span></a>
                        </li>
                            @if(!in_array(Auth::user()->title, [1,2]))
                        <li class="sidebar-item"><a href="{{route('hr.attendances')}}" class="sidebar-link"><span class="hide-menu">Attendances</span></a>
                        </li>
                            @endif

                    </ul>
                </li>
                @endif
                <!-- supervisor -->

                <!-- attendance -->
                    @if(Auth::user()->title != 6 && Auth::user()->title != 5 && Auth::user()->title != 0)<!--hr->6-->
                    @if(!in_array(Auth::user()->title, [1,2]))
                    <li id="attendance" class="sidebar-item"> <a class="sidebar-link text-dark" href="{{route('attendance')}}" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard feather-icon"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg><span class="hide-menu">Attendance Sheet
                                    </span></a>
                    </li>
                    @endif
                    @endif
                    {{--@if(Auth::user()->title == 6 || Auth::user()->title == 0)<!--hr->6-->
                    <li id="attendance" class="sidebar-item"> <a class="sidebar-link text-dark" href="{{route('hrattendance')}}" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard feather-icon"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg><span class="hide-menu">Attendance Sheet
                                </span></a>
                    </li>
                    @endif--}}
                <li class="list-divider"></li>
                <li class="nav-small-cap"><span class="hide-menu">Extra</span></li>
                @if(Auth::user()->title != 6 && Auth::user()->title != 3 && Auth::user()->title != 5)<!--hr->6-->
                    @if(!in_array(Auth::user()->title, [1,2]))
                <li class="sidebar-item"> <a class="sidebar-link text-dark" href="backup.html" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-download feather-icon"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="7 10 12 15 17 10"></polyline><line x1="12" y1="15" x2="12" y2="3"></line></svg><span class="hide-menu">Backup
                                </span></a>
                </li>
                    @endif
                    @endif
                <li onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" class="sidebar-item"> <a class="sidebar-link sidebar-link text-dark" href="{{route('logout')}}"aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out feather-icon"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg><span class="hide-menu">Logout</span></a></li>
            </ul>
            <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                @csrf
            </form>
        </nav>
        <!-- End Sidebar navigation -->
        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 577px; right: 3px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 568px;"></div></div></div>
    <!-- End Sidebar scroll-->
</aside>
